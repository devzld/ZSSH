package com.zy.zssh.m_news.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.dbBean.ChannelItem;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.activity.NewsDetailActivity;
import com.zy.zssh.m_news.adapter.news.NewsAdapter;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;
import com.zy.zssh.m_news.bean.news.CarouselImageBean;
import com.zy.zssh.m_news.bean.news.NewsBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanji on 16/3/29.
 * <p/>
 * describe:新闻fragmentTab页面
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_news_tab)
public class NewsTabFragment extends BaseFragment {

    private String TAG = "NewsTabFragment";

    private ChannelItem channelItem;

    @ViewInject(R.id.news_tab_recycler_view)
    private RecyclerView recyclerView;

    @ViewInject(R.id.news_tab_swipeRefreshLayout)
    private SwipyRefreshLayout swipeRefreshLayout;

    private NewsAdapter newsRecycleAdapter;

    private List<NewsBean> newsBeanList = new ArrayList<>();

    private List<CarouselImageBean> carouselImageBeanList = new ArrayList<>();

    private NewsBusiness business;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否含有上啦加载
     */
    private boolean isBottom = true;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;



    public NewsTabFragment(ChannelItem channelItem) {
        super();
        this.channelItem = channelItem;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        business = NewsBusiness.getInstance();

        if (newsBeanList.size() == 0) {

            swipeRefreshLayout.setRefreshing(true);
            initData();
        }

        if (isBottom) {

            swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        } else {

            swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        }


        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        newsRecycleAdapter = new NewsAdapter(getActivity(), newsBeanList, carouselImageBeanList, channelItem.getClassName().equals("推荐"));

//        newsRecycleAdapter = new NewsAdapter(getActivity(), newsBeanList, carouselImageBeanList, true);

        recyclerView.setAdapter(newsRecycleAdapter);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    newsBeanList.clear();
                    initData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }

            }
        });

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == newsRecycleAdapter.getItemCount()
                        && !isLoading && swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    swipeRefreshLayout.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        newsRecycleAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                Bundle bundle = new Bundle();

                //-1表示未设置id
                if (view.getId() == -1) {

                    bundle.putString("key", carouselImageBeanList.get(position).getNewsId());
                } else {

                    bundle.putString("key", newsBeanList.get(position).getNewsId());

                }

                openActivity(NewsDetailActivity.class, bundle);

//                openActivity(NDetailActivity.class,bundle);

            }
        });

    }

    /**
     * 初始化list数据
     */
    private void initData() {

        business.getNewsListData("1", channelItem.getClassId(), mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<NewsBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), NewsBean.class);

                    if (aa != null && aa.size() != 0) {

                        newsBeanList.clear();
                        newsBeanList.addAll(aa);
                        newsRecycleAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                        isBottom = false;
                    } else {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        isBottom = true;
                        pageNo++;
                    }

                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        if (carouselImageBeanList.size() == 0) {

            business.getCarouselImageData(getActivity(), new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);

                    if (isSuccess(response)) {

                        List<CarouselImageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), CarouselImageBean.class);

                        if (aa != null && aa.size() != 0) {

                            carouselImageBeanList.addAll(aa);

                            newsRecycleAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);
                }
            });
        }
    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getNewsListData(pageNo + "", channelItem.getClassId(), mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), NewsBean.class);

                    if (aa != null && aa.size() != 0) {

                        newsBeanList.addAll(aa);

                        newsRecycleAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                        isBottom = false;
                    } else {
                        pageNo++;
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        isBottom = true;
                    }
                }

                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
