package com.zy.zssh.m_news.activity.yellow;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMapOptions;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orhanobut.logger.Logger;
import com.shizhefei.view.largeimage.LongImageView;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.utils.CommonTool;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.yellow.YellowPageProductAdapter;
import com.zy.zssh.m_news.bean.yellow.YellowPageDetailBean;
import com.zy.zssh.m_news.bean.yellow.YellowPageProductDetailBean;
import com.zy.zssh.m_news.business.NewsBusiness;
import com.zy.zssh.m_news.view.MixtureTextView;

import org.xutils.common.Callback;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class YellowPageDetailActivity extends BaseActivity{

//    @ViewInject(R.id.yellow_page_detail_ico)
//    private ImageView yellow_page_detail_ico;
//
//    @ViewInject(R.id.yellow_page_detail_title)
//    private TextView yellow_page_detail_title;
//
//    @ViewInject(R.id.yellow_page_detail_describe)
//    private MixtureTextView yellow_page_detail_describe;

    @ViewInject(R.id.yellow_page_detail_recycleview)
    private RecyclerView yellow_page_detail_recycleview;

    @ViewInject(R.id.yellow_page_detail_maptitle)
    private TextView yellow_page_detail_maptitle;

    @ViewInject(R.id.yellow_page_detail_bmapView_linear)
    private LinearLayout yellow_page_detail_bmapView_linear;

    @ViewInject(R.id.yellow_page_detail_tell)
    private TextView yellow_page_detail_tell;

    @ViewInject(R.id.naviContainer)
    private LinearLayout naviContainer;

    @ViewInject(R.id.callPhoneContainer)
    private LinearLayout callPhoneContainer;

    @ViewInject(R.id.longImageView)
    private LongImageView longImageView;


    private MapView mapView;

    private BaiduMap baiduMap;

    private NewsBusiness business;

    private String entId = "";
    private String entName = "";

    private YellowPageDetailBean yellowPageDetailBean;

    private YellowPageProductAdapter yellowPageProductAdapter;

    private List<YellowPageProductDetailBean> yellowPageProductDetailBeanList = new ArrayList<>();



    private BaiduMap map;



    private static final int RESULT_PERMISSION_CALLPHONE = 0x1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_yellow_page_detail);

        x.view().inject(this);

        initView();
        initData();


    }

    @Override
    protected void initView() {


        callPhoneContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callPhone();


            }
        });


        naviContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(yellowPageDetailBean!=null){

                    Intent mIntent = null;
                    try {

                        String uri = "intent://map/marker?location=" + yellowPageDetailBean.getEntLat() +"," + yellowPageDetailBean.getEntLon() + "&title=" + yellowPageDetailBean.getEntName() +"&content=&src=yourCompanyName|yourAppName#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end";
//                        Logger.d("uri:" + uri);
                        mIntent = Intent.getIntent(uri);


                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    try {

                        startActivity(mIntent);

                    }catch (Exception exception){


                        Logger.e(exception.toString());
                        //将地址信息发过去
                        Intent intent = new Intent(YellowPageDetailActivity.this, RouteActivity.class);
                        intent.putExtra("latitude",yellowPageDetailBean.getEntLat());
                        intent.putExtra("longitude",yellowPageDetailBean.getEntLon());
                        startActivity(intent);


                    }


                }





            }
        });



        //初始化首页数据
        if (yellowPageDetailBean != null) {

//            ImageLoader.getInstance().displayImage(yellowPageDetailBean.getEntUrl(), yellow_page_detail_ico, MyApplication.imageOptionNormal);

            CommonTool.cacheNetFile(this, yellowPageDetailBean.getEntMain(), new Callback.CommonCallback<File>() {
                @Override
                public void onSuccess(File result) {


//                    Logger.d("缓存成功：" + result.getAbsolutePath());
//                    Logger.d("当前的线程：" + Thread.currentThread());
                    if(result.exists()){

                        longImageView.setImage(result.getPath());

                    }


                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {

                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {

                }
            });
//            yellow_page_detail_title.setText(yellowPageDetailBean.getEntName());
//            yellow_page_detail_describe.setText(yellowPageDetailBean.getEntMain());
            yellow_page_detail_maptitle.setText(yellowPageDetailBean.getEntName());
            yellow_page_detail_tell.setText(yellowPageDetailBean.getEntCon());

            setcenterAndDraw(new LatLng(Double.parseDouble(yellowPageDetailBean.getEntLat()), Double.parseDouble(yellowPageDetailBean.getEntLon())));
        }
    }

    private void callPhone() {


        if(yellowPageDetailBean!=null){

            String phone = yellowPageDetailBean.getEntCon();
            if(!TextUtils.isEmpty(phone)){


                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);


            }else{

                Tools.Toast(this,"电话号码为空");

            }


        }



    }



    /**
     * 本机是否安装百度地图
     *
     * @return
     */
    private boolean isInstall() {


        return new File("/data/data/" + "com.baidu.BaiduMap").exists();


    }

    @Override
    protected void initData() {

        entId = getIntent().getExtras().getString("entId");
        entName = getIntent().getExtras().getString("entName");

        setMiddleTitle(entName);

        business = NewsBusiness.getInstance();

        BaiduMapOptions options = new BaiduMapOptions();
        options.zoomControlsEnabled(false);
        options.scaleControlEnabled(false);
        options.compassEnabled(false);
        options.overlookingGesturesEnabled(false);
        options.rotateGesturesEnabled(false);
        options.scaleControlEnabled(false);
        options.scrollGesturesEnabled(false);
        options.zoomGesturesEnabled(false);


        //初始化地图
        mapView = new MapView(this, options);
        map = mapView.getMap();
        LinearLayout.LayoutParams params_map = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        yellow_page_detail_bmapView_linear.addView(mapView, params_map);


        baiduMap = mapView.getMap();

        //recycle初始化和数据设置
        yellow_page_detail_recycleview.setHasFixedSize(true);
        yellow_page_detail_recycleview.setItemAnimator(new DefaultItemAnimator());
        yellow_page_detail_recycleview.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.HORIZONTAL));
        yellowPageProductAdapter = new YellowPageProductAdapter(this, yellowPageProductDetailBeanList);
        yellow_page_detail_recycleview.setAdapter(yellowPageProductAdapter);

        business.getBusinessDetailData(entId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    yellowPageDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), YellowPageDetailBean.class);

                    initView();

                    initProductData();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

        yellowPageProductAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("list", (Serializable) yellowPageProductDetailBeanList);
                bundle.putInt("position", position);

                openActivity(YellowPageProductDetailActivity.class, bundle);
            }
        });
    }

    @Override
    protected void initClickListener(View v) {

    }

    /**
     * 初始化产品数据
     */
    private void initProductData() {

        business.getBusinessProductData(entId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {
                    yellowPageProductDetailBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "comprolist"), YellowPageProductDetailBean.class));

                    yellowPageProductAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

    }

    /**
     * 改变地图中心点的位置
     */
    private void setcenterAndDraw(LatLng point) {
        // TODO Auto-generated method stub

        MapStatusUpdate mapStatus = MapStatusUpdateFactory.newLatLngZoom(point, 15);
        baiduMap.setMapStatus(mapStatus);

        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.mipmap.icon_marka);
        //构建MarkerOption，用于在地图上添加Marker
        OverlayOptions option = new MarkerOptions().position(point).icon(bitmap);
        //在地图上添加Marker，并显示
        baiduMap.addOverlay(option);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }





}
