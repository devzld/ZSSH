package com.zy.zssh.m_news.adapter.yellow;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.m_news.bean.yellow.YellowPageProductDetailBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
public class YellowPageProductAdapter extends BaseRecycleAdapter {

    private List<YellowPageProductDetailBean> yellowPageProductDetailBeanList;

    public YellowPageProductAdapter(Context mContext, List<YellowPageProductDetailBean> yellowPageProductDetailBeanList) {
        super(mContext);

        this.yellowPageProductDetailBeanList = yellowPageProductDetailBeanList;

        if (this.yellowPageProductDetailBeanList == null) {
            this.yellowPageProductDetailBeanList = new ArrayList<>();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageView imageView = new ImageView(mContext);

        int size = Tools.getGridViewImageSize(mContext, 0, 0, 3);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, Tools.dip2px(MyApplication.getContext(), 70));
        params.setMargins(10, 10, 10, 10);
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        return new YellowPageProductViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(yellowPageProductDetailBeanList.get(position).getProUrl()
                /*"http://img3.imgtn.bdimg.com/it/u=3423398259,509180804&fm=15&gp=0.jpg"*/,
                ((YellowPageProductViewHolder) holder).imageView, MyApplication.imageOptionNormal);

    }

    @Override
    public int getItemCount() {
        return yellowPageProductDetailBeanList.size();
    }

    public class YellowPageProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;

        public YellowPageProductViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView;
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
