package com.zy.zssh.m_news.activity.wood;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.adapter.wood.WoodLoreAdapter;
import com.zy.zssh.m_news.bean.wood.WoodLoreBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

public class WoodLoreActivity extends BaseActivity {

    @ViewInject(R.id.wood_lore_recycleview)
    private RecyclerView wood_lore_recycleview;

    private List<WoodLoreBean> woodLoreBeanList = new ArrayList<>();

    private WoodLoreAdapter woodLoreAdapter;

    private NewsBusiness business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_wood_lore);

        x.view().inject(this);

        initView();
        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("木材知识");

        //recycle初始化和数据设置
        wood_lore_recycleview.setHasFixedSize(true);
        wood_lore_recycleview.setItemAnimator(new DefaultItemAnimator());
        wood_lore_recycleview.setLayoutManager(new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL));

        business = NewsBusiness.getInstance();

        woodLoreAdapter = new WoodLoreAdapter(this, woodLoreBeanList);

        wood_lore_recycleview.setAdapter(woodLoreAdapter);

        woodLoreAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();

                bundle.putString("moodId", woodLoreBeanList.get(position).getMoodId());
                bundle.putString("moodName", woodLoreBeanList.get(position).getMoodName());

                openActivity(WoodLoreDetailActivity.class, bundle);
            }
        });
    }

    @Override
    protected void initData() {

        business.getWoodLoreData(this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    woodLoreBeanList.clear();

                    woodLoreBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "mastlist"), WoodLoreBean.class));

                    woodLoreAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

    }

    @Override
    protected void initClickListener(View v) {

    }
}
