package com.zy.zssh.m_news.bean.news;

/**
 * Created by Administrator on 2016/6/24 0024.
 */
public class CarouselImageBean {

    /**
     * id : 1
     * newsId : 1
     * Url : http://app.shzywh.cn/images/news1.jpg
     */

    private String id;
    private String newsId;
    private String Url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }


    @Override
    public String toString() {
        return "CarouselImageBean{" +
                "id='" + id + '\'' +
                ", newsId='" + newsId + '\'' +
                ", Url='" + Url + '\'' +
                '}';
    }
}
