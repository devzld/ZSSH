package com.zy.zssh.m_news.business;

import android.content.Context;

import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hanj .
 */
public class NewsBusiness {
    private static NewsBusiness instance = new NewsBusiness();

    private NewsBusiness() {
    }

    public static synchronized NewsBusiness getInstance() {

        //

        return instance;
    }

    /**
     * 获取新闻列表数据
     *
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getNewsListData(String page, String classId,
                                Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "newlist" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("classId", classId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取新闻详情
     *
     * @param newsId   新闻id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getNewsDetailData(String newsId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "newdet" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("newsId", newsId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取新闻评论列表
     *
     * @param newsId   新闻id
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getNewsCommentData(String newsId, String page,
                                   Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "newscom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("newsId", newsId);
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);




    }

    /**
     * 添加评论
     *
     * @param newsId   新闻id
     * @param userId   发表评论用户id
     * @param comMain  评论内容
     * @param comId2   回复评论的评论id，一级评论为0
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void sendNewsCommentData(String newsId, String userId, String comMain, String comId2,
                                    Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "addnewscom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("newsId", newsId);
        params.put("userId", userId);
        params.put("comMain", comMain);
        params.put("comId2", comId2);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取地址列表数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getMapData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "map" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取所在地公司数据                获取更多公司的数据
     *
     * @param mapId    地址id 0为全国
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getBusinessData(String mapId, String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "comtLisrt" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("mapId", mapId);
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());
        params.put("key","");
        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取公司详情
     *
     * @param entId    公司的id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getBusinessDetailData(String entId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "company" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("entId", entId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取公司产品列表
     *
     * @param entId    公司id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getBusinessProductData(String entId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "compro" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("entId", entId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取大师列表数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGreatMasterData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "mastList" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取大师详情数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGreatMasterDetailData(String mastId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "master" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("mastId", mastId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取大师作品数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGreatMasterProductData(String mastId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "mastwork" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("mastId", mastId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取木材知识数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getWoodLoreData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "moodList" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取木材知识详情数据
     *
     * @param moodId   木材编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getWoodLoreDetailData(String moodId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "mood" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("moodId", moodId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取最新款式数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getNewestStyleData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "appnewspro" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());
        params.put("page","1");
        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);

    }


    /**
     * 加载更多最新款式
     *
     * @param context
     * @param page
     * @param callback
     */
    public void loadMoreStyleData(Context context , String page , VolleyCallback callback){

        String url = VolleyClient.SERVER_IP_ADDRESS + "appnewspro" + VolleyClient.SERVER_IP_FOOT;
        Map<String , String> paras = new HashMap<>();
        paras.put("userId",Config.Config(context).getUserId());
        paras.put("page",page);
        VolleyClient.getInstance(context).requsetOfPost(url,paras,callback);

    }






    /**
     * 获取所有频道数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getAllChannelData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "allnewsclass" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 上传频道数据
     *
     * @param classId  分类字符串格式1,2,3,4,5,6,7,8
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void updateChannelData(String classId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "addnewsclass" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());
        params.put("classId", classId);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取轮播图数据
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getCarouselImageData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "newslft" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取轮播图数据
     *
     * @param type     类型，新闻视频等
     * @param id       id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void setzanData(String type, String id, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "zan" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("id", id);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);





    }
















}


