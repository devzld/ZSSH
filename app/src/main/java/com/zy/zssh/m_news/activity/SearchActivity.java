package com.zy.zssh.m_news.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.activity.yellow.YellowPageDetailActivity;
import com.zy.zssh.m_news.adapter.yellow.YellowPageAdapter;
import com.zy.zssh.m_news.bean.yellow.YellowPageBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity implements TextWatcher{



    public static final String URL_SEARCH_COMPANY = "http://app.shzywh.cn/app/comtLisrt.aspx";

    @BindView(R.id.inputContent)
    EditText inputContent;
    @BindView(R.id.cancleButton)
    TextView cancleButton;
    @BindView(R.id.resultRecyclerView)
    RecyclerView resultRecyclerView;


    private List<YellowPageBean> yellowPageBeanList = new ArrayList<>();
    private YellowPageAdapter yellowPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        yellowPageAdapter = new YellowPageAdapter(this,yellowPageBeanList);
        yellowPageAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                //传递id和name数据到详情页面
                Bundle bundle = new Bundle();
                bundle.putString("entId", yellowPageBeanList.get(position).getEntId());
                bundle.putString("entName", yellowPageBeanList.get(position).getEntName());

                Intent intent = new Intent(SearchActivity.this,YellowPageDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
        resultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        resultRecyclerView.setAdapter(yellowPageAdapter);
        inputContent.addTextChangedListener(this);



    }

    @OnClick(R.id.cancleButton)
    public void onClick() {

        finish();


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {



        String content = s.toString().trim();

        HashMap<String,String> params = new HashMap<>();

        params.put("key",content);
        params.put("page",1+"");

        Logger.d(params.toString());

        VolleyClient.getInstance(this).requsetOfPost(URL_SEARCH_COMPANY, params, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                List<YellowPageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), YellowPageBean.class);
                yellowPageBeanList.clear();
                yellowPageBeanList.addAll(aa);
                yellowPageAdapter.notifyDataSetChanged();


            }


        });




    }
}
