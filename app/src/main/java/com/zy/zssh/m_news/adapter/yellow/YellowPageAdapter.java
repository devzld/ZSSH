package com.zy.zssh.m_news.adapter.yellow;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.bean.yellow.YellowPageBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/7 0007.
 */
public class YellowPageAdapter extends BaseRecycleAdapter {

    private List<YellowPageBean> yellowPageBeanList;

    public YellowPageAdapter(Context context, List<YellowPageBean> yellowPageBeanList) {
        super(context);

        this.yellowPageBeanList = yellowPageBeanList;

        if (this.yellowPageBeanList == null) {
            this.yellowPageBeanList = new ArrayList<>();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new YellowPageViewHolder(mLayoutInflater.inflate(R.layout.yellow_page_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(getItem(position).getEntUrl(), ((YellowPageViewHolder) holder).yellow_page_item_ico, MyApplication.imageOptionNormal15);

        ((YellowPageViewHolder) holder).yellow_page_item_title.setText(getItem(position).getEntName());

        ((YellowPageViewHolder) holder).yellow_page_item_renzheng.setSelected(getItem(position).getSate().equals("1"));

        ((YellowPageViewHolder) holder).yellow_page_item_renzheng_tv.setText(getItem(position).getSate().equals("1") ? "已认证" : "未认证");

        for (int i = 0; i < 5; i++) {
            ((YellowPageViewHolder) holder).images.get(i).setSelected(false);
        }

        try {
            for (int i = 0; i < Integer.parseInt(getItem(position).getEntSore()); i++) {
                ((YellowPageViewHolder) holder).images.get(i).setSelected(true);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        ((YellowPageViewHolder) holder).yellow_page_item_like.setSelected("1".equals(getItem(position).getZansate()));

        ((YellowPageViewHolder) holder).yellow_page_item_star_business.setSelected("1".equals(getItem(position).getColsate()));

    }

    private YellowPageBean getItem(int position) {

        return yellowPageBeanList.get(position);
    }

    @Override
    public int getItemCount() {
        return yellowPageBeanList.size();
    }

    public class YellowPageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.cv_item)
        private CardView cv_item;

        @ViewInject(R.id.yellow_page_item_ico)
        private ImageView yellow_page_item_ico;

        @ViewInject(R.id.yellow_page_item_title)
        private TextView yellow_page_item_title;

        @ViewInject(R.id.yellow_page_item_renzheng)
        private View yellow_page_item_renzheng;

        @ViewInject(R.id.yellow_page_item_renzheng_tv)
        private TextView yellow_page_item_renzheng_tv;

        @ViewInject(R.id.yellow_page_item_star1)
        private ImageView yellow_page_item_star1;

        @ViewInject(R.id.yellow_page_item_star2)
        private ImageView yellow_page_item_star2;

        @ViewInject(R.id.yellow_page_item_star3)
        private ImageView yellow_page_item_star3;

        @ViewInject(R.id.yellow_page_item_star4)
        private ImageView yellow_page_item_star4;

        @ViewInject(R.id.yellow_page_item_star5)
        private ImageView yellow_page_item_star5;

        @ViewInject(R.id.yellow_page_item_like)
        private ImageView yellow_page_item_like;

        @ViewInject(R.id.yellow_page_item_star_business)
        private ImageView yellow_page_item_star_business;

        private List<ImageView> images = new ArrayList<>();

        public YellowPageViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            images.add(yellow_page_item_star1);
            images.add(yellow_page_item_star2);
            images.add(yellow_page_item_star3);
            images.add(yellow_page_item_star4);
            images.add(yellow_page_item_star5);

            cv_item.setOnClickListener(this);
            yellow_page_item_like.setOnClickListener(this);
            yellow_page_item_star_business.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
