package com.zy.zssh.m_news.activity.yellow;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.DrivingRouteOverlay;

import org.xutils.x;

public class RouteActivity extends BaseActivity implements OnGetRoutePlanResultListener{



    /*
    @ViewInject(R.id.mapView)
    MapView mapView;*/

    private LocationClient client;
    private LocationClientOption option;
    private RoutePlanSearch routePlanSearch;
    private BaiduMap map;
    boolean useDefaultIcon = false;

    private PlanNode eNode;
    private MapView mv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        x.view().inject(this);
        initView();
        initData();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        routePlanSearch.destroy();
        mv.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mv.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mv.onResume();
    }

    @Override
    protected void initView() {

        mv = new MapView(this);
        setMainContent(mv);


    }

    @Override
    protected void initData() {

        Intent intent = getIntent();
        final String latitude = intent.getStringExtra("latitude");
        final String longitude = intent.getStringExtra("longitude");
        //计算结束坐标
        double eLatitude = Double.parseDouble(latitude);
        double eLongitude = Double.parseDouble(longitude);
        LatLng ell = new LatLng(eLatitude,eLongitude);
        eNode = PlanNode.withLocation(ell);

        map = mv.getMap();


        routePlanSearch = RoutePlanSearch.newInstance();
        routePlanSearch.setOnGetRoutePlanResultListener(this);

        option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        option.setIsNeedAddress(true);
        option.setOpenGps(true);
        option.setScanSpan(2000);
        client = new LocationClient(this,option);

        //设置定位的监听回调事件
        client.registerLocationListener(new BDLocationListener() {
            @Override
            public void onReceiveLocation(BDLocation bdLocation) {


                if(!TextUtils.isEmpty(bdLocation.getCity())){

                    //获取当前位置作为其实坐标
                    double sLatitude = bdLocation.getLatitude();
                    double sLongitude = bdLocation.getLongitude();
                    LatLng sll = new LatLng(sLatitude,sLongitude);
                    PlanNode sNode = PlanNode.withLocation(sll);

                    routePlanSearch.drivingSearch(new DrivingRoutePlanOption().from(sNode).to(eNode));

                    client.stop();

                }



            }
        });


        //开始定位
        client.start();


    }

    @Override
    protected void initClickListener(View v) {

    }

    @Override
    public void onGetWalkingRouteResult(WalkingRouteResult walkingRouteResult) {

    }

    @Override
    public void onGetTransitRouteResult(TransitRouteResult transitRouteResult) {

    }




    //路线图监听回调事件
    @Override
    public void onGetDrivingRouteResult(DrivingRouteResult drivingRouteResult) {

        //驾车
        if (drivingRouteResult == null || drivingRouteResult.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(RouteActivity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (drivingRouteResult.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            // result.getSuggestAddrInfo()
            return;
        }

        if (drivingRouteResult.error == SearchResult.ERRORNO.NO_ERROR) {


            if (drivingRouteResult.getRouteLines().size() > 0) {

                //如果有多条路线


                DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(map);
                map.setOnMarkerClickListener(overlay);
                overlay.setData(drivingRouteResult.getRouteLines().get(0));
                overlay.addToMap();
                overlay.zoomToSpan();


            }


        }




    }

    @Override
    public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {

    }


    // 定制RouteOverly
    private class MyDrivingRouteOverlay extends DrivingRouteOverlay {

        public MyDrivingRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(R.mipmap.icon_st);
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(R.mipmap.icon_en);
            }
            return null;
        }
    }



}
