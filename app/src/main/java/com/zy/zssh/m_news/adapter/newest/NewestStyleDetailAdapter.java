package com.zy.zssh.m_news.adapter.newest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zy.zssh.m_news.bean.newest.NewestStyleBean;
import com.zy.zssh.m_news.fragment.NewestStyleDetailFragment;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28 0028.
 */
public class NewestStyleDetailAdapter extends FragmentPagerAdapter{

    private List<NewestStyleBean> list;




    public NewestStyleDetailAdapter(FragmentManager fm,List<NewestStyleBean> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return new NewestStyleDetailFragment(list.get(position));
    }

    @Override
    public int getCount() {
        return list == null ? 0:list.size();
    }
}
