package com.zy.zssh.m_news.activity.wood;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.bean.wood.WoodLoreDetailBean;
import com.zy.zssh.m_news.business.NewsBusiness;
import com.zy.zssh.m_news.view.MixtureTextView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;
/**
 * desc:木材知识详情页面
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 9:17
**/
public class WoodLoreDetailActivity extends BaseActivity {

    @ViewInject(R.id.wood_lore_mixtureTextView)
    private MixtureTextView wood_lore_mixtureTextView;

    @ViewInject(R.id.wood_lore_ico)
    private ImageView wood_lore_ico;

//    @ViewInject(R.id.great_master_detail_recycleview)
//    private RecyclerView great_master_detail_recycleview;
//
//    private GreatMasterProductAdapter greatMasterProductAdapter;
//
//    private List<GreatMasterProductBean> greatMasterProductBeanList = new ArrayList<>();

    private NewsBusiness business;

    private WoodLoreDetailBean woodLoreDetailBean;

    private String moodId = "";

    private String moodName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_wood_lore_detail);

        x.view().inject(this);

        Bundle bundle = getIntent().getExtras();
        moodId = bundle.getString("moodId");
        moodName = bundle.getString("moodName");

        business = NewsBusiness.getInstance();

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle(moodName);

//        //recycle初始化和数据设置
//        great_master_detail_recycleview.setHasFixedSize(true);
//        great_master_detail_recycleview.setItemAnimator(new DefaultItemAnimator());
//        great_master_detail_recycleview.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.HORIZONTAL));
//
//        greatMasterProductAdapter = new GreatMasterProductAdapter(this, greatMasterProductBeanList);
//        great_master_detail_recycleview.setAdapter(greatMasterProductAdapter);
    }

    private void diaplayData() {
        if (woodLoreDetailBean != null) {

            ImageLoader.getInstance().displayImage(woodLoreDetailBean.getMoodUrl(), wood_lore_ico, MyApplication.imageOptionNormal);

            wood_lore_mixtureTextView.setText(woodLoreDetailBean.getMian());
        }
    }

    @Override
    protected void initData() {

        business.getWoodLoreDetailData(moodId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);
                if (isSuccess(response)) {

                    woodLoreDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), WoodLoreDetailBean.class);

                    diaplayData();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

//        business.getGreatMasterProductData(moodId, this, new VolleyCallback() {
//            @Override
//            public void requestSuccess(VolleyResponse response) {
//                super.requestSuccess(response);
//
//                if (isSuccess(response)) {
//
//                    greatMasterProductBeanList.addAll(FastJsonUtil.jsonString2Beans(
//                            FastJsonUtil.getNoteJson(response.getBody().toString(), "mastworklist"), GreatMasterProductBean.class));
//
//                    greatMasterProductAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void requestError(VolleyError error) {
//                super.requestError(error);
//            }
//        });
//
//
//        greatMasterProductAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("list", (Serializable) greatMasterProductBeanList);
//                bundle.putInt("position", position);
//
//                openActivity(GreatMasterProductDetailActivity.class, bundle);
//            }
//        });

    }

    @Override
    protected void initClickListener(View v) {

    }
}
