package com.zy.zssh.m_news.adapter.news;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.zy.zssh.common.dbBean.ChannelItem;
import com.zy.zssh.m_news.fragment.NewsTabFragment;

import java.util.List;

/**
 * Created by hanji on 16/3/30.
 * <p/>
 * describe:频道列表的ViewPage适配器
 */
public class NewsViewPageAdapter extends FragmentPagerAdapter {

    private List<ChannelItem> channelItemList;

    private String TAG = "NewsViewPageAdapter";

    public NewsViewPageAdapter(FragmentManager fm, List<ChannelItem> channelItemList) {
        super(fm);
        this.channelItemList = channelItemList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return channelItemList.get(position).getClassName();
    }

    @Override
    public int getCount() {
        return channelItemList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    //id存在的话，不会重复调用
    @Override
    public Fragment getItem(int position) {

        return new NewsTabFragment(channelItemList.get(position));
    }

    //设置每个页签唯一id防止不调用getitem
    @Override
    public long getItemId(int position) {
        // 获取当前数据的hashCode
        return channelItemList.get(position).getId();
    }
}