package com.zy.zssh.m_news.bean.news;

/**
 * Created by Administrator on 2016/6/3 0003.
 * 评论的bean类
 */
public class NewsCommentBean {

    /**
     * comId : 1
     * userId : 10086
     * loginName : 小张
     * ComMain : heihei
     * userUrl : http://app.shzywh.cn/app/images/head.png
     * Comzan : 3
     * addTime : 2016-5-28 19:36:22
     * userID2 : 0
     * LoginName2 :
     * userUrL2 :
     * comMain2 :
     */

    private String comId;
    private String userId;
    private String loginName;
    private String ComMain;
    private String userUrl;
    private String Comzan;
    private String addTime;
    private String userID2;
    private String LoginName2;
    private String userUrL2;
    private String comMain2;
    private String zansate;

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getComMain() {
        return ComMain;
    }

    public void setComMain(String ComMain) {
        this.ComMain = ComMain;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public String getComzan() {
        return Comzan;
    }

    public void setComzan(String Comzan) {
        this.Comzan = Comzan;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUserID2() {
        return userID2;
    }

    public void setUserID2(String userID2) {
        this.userID2 = userID2;
    }

    public String getLoginName2() {
        return LoginName2;
    }

    public void setLoginName2(String loginName2) {
        LoginName2 = loginName2;
    }

    public String getUserUrL2() {
        return userUrL2;
    }

    public void setUserUrL2(String userUrL2) {
        this.userUrL2 = userUrL2;
    }

    public String getComMain2() {
        return comMain2;
    }

    public void setComMain2(String comMain2) {
        this.comMain2 = comMain2;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }
}
