package com.zy.zssh.m_news.adapter.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.utils.CommonTool;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;
import com.zy.zssh.m_news.bean.news.NewsDetailBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/3 0003.
 * 新闻评论列表的adapter
 */
public class NewsCommentAdapter extends NewsCommentBaseRecycleAdapter {

    private List<NewsCommentBean> newsCommentBeanList;

    public NewsCommentAdapter(Context mContext, List<NewsCommentBean> newsCommentBeanList, NewsDetailBean newsDetailBean) {
        super(mContext, newsDetailBean);

        this.newsCommentBeanList = newsCommentBeanList;

        if (newsCommentBeanList == null) {
            this.newsCommentBeanList = new ArrayList<>();
        }
    }

    /**
     * 新闻详情数据绑定
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //新闻详情
        if (holder instanceof NewsCommentHeadViewHolder && newsDetailBean != null) {

            ((NewsCommentHeadViewHolder) holder).news_detail_title.setText(newsDetailBean.getNewsTitle());

            ((NewsCommentHeadViewHolder) holder).news_detail_author.setText(newsDetailBean.getNewsWriter());

            ((NewsCommentHeadViewHolder) holder).news_detail_time.setText(newsDetailBean.getAddTime());


//            Logger.d(newsDetailBean.toString());


            //设置缓存，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据
//            WebSettings settings = ((NewsCommentHeadViewHolder) holder).news_detail_webview.getSettings();
//            settings.setCacheMode(WebSettings.LOAD_DEFAULT | WebSettings.LOAD_CACHE_ELSE_NETWORK);

//            ((NewsCommentHeadViewHolder) holder).news_detail_webview.loadDataWithBaseURL("", newsDetailBean.getNewsMain(), "text/html", "utf-8", null);
//            ((NewsCommentHeadViewHolder) holder).news_detail_webview.loadUrl(newsDetailBean.getNewsMain());


        } else if (holder instanceof NewsCommentViewHolder) {

            //评论的viewholder


            ImageLoader.getInstance().displayImage(getItem(position).getUserUrl(),
                    ((NewsCommentViewHolder) holder).news_comment_user_ico, MyApplication.imageOptionCircle);

            ((NewsCommentViewHolder) holder).news_comment_user_name.setText(getItem(position).getLoginName());

            ((NewsCommentViewHolder) holder).news_comment_user_time.setText(getItem(position).getAddTime());

            ((NewsCommentViewHolder) holder).news_comment_content.setText(getItem(position).getComMain());
            ((NewsCommentViewHolder) holder).news_comment_like.setSelected(getItem(position).getZansate().equals("1"));

            if ("0".equals(getItem(position).getUserID2())) {
                ((NewsCommentViewHolder) holder).news_comment_reply.setVisibility(View.GONE);
            } else {

                try {
                    ((NewsCommentViewHolder) holder).news_comment_reply.setVisibility(View.VISIBLE);

                    ImageLoader.getInstance().displayImage(getItem(position).getUserUrL2(),
                            ((NewsCommentViewHolder) holder).news_comment_user_ico1, MyApplication.imageOptionCircle);

                    ((NewsCommentViewHolder) holder).news_comment_user_name1.setText(getItem(position).getLoginName2());

                    ((NewsCommentViewHolder) holder).news_comment_user_time1.setText(getItem(position).getAddTime());

                    ((NewsCommentViewHolder) holder).news_comment_content1.setText(getItem(position).getComMain2());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }else if(holder instanceof MyWebViewHolder){


            ((MyWebViewHolder)holder).news_detail_webview.loadUrl(newsDetailBean.getNewsMain());

        }

    }

    public NewsCommentBean getItem(int position) {
        return newsCommentBeanList.get(newsDetailBean != null ? position - 2 : position);
    }

    @Override
    public int getItemCount() {
        return newsDetailBean != null ? newsCommentBeanList.size() + 2 : newsCommentBeanList.size();
    }

    //头
    @Override
    public RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent) {
        return new NewsCommentHeadViewHolder(mLayoutInflater.inflate(R.layout.fg_news_comment_head_item, parent, false));
    }

    //评论
    @Override
    public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
        return new NewsCommentViewHolder(mLayoutInflater.inflate(R.layout.fg_news_comment_item, parent, false));
    }


    //网页
    @Override
    public RecyclerView.ViewHolder onCreateWebViewHolder(ViewGroup parent) {
        WebView webWiew = new WebView(mContext);

        int margin = CommonTool.dp2Pix(mContext,7);
//        int padding = CommonTool.dp2Pix(mContext,7);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        params.setMargins(margin,0,margin,0);

        webWiew.setLayoutParams(params);

//        webWiew.setPadding(padding,0,padding,0);

        return new MyWebViewHolder(webWiew);

    }



    public class NewsCommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.news_comment_user_ico)
        private ImageView news_comment_user_ico;

        @ViewInject(R.id.news_comment_user_name)
        private TextView news_comment_user_name;

        @ViewInject(R.id.news_comment_user_time)
        private TextView news_comment_user_time;

        @ViewInject(R.id.news_comment_content)
        private TextView news_comment_content;

        @ViewInject(R.id.news_comment_reply)
        private View news_comment_reply;

        @ViewInject(R.id.news_comment_user_ico1)
        private ImageView news_comment_user_ico1;

        @ViewInject(R.id.news_comment_user_name1)
        private TextView news_comment_user_name1;

        @ViewInject(R.id.news_comment_user_time1)
        private TextView news_comment_user_time1;

        @ViewInject(R.id.news_comment_content1)
        private TextView news_comment_content1;

        @ViewInject(R.id.news_comment_like)
        private ImageView news_comment_like;
//
//        @ViewInject(R.id.cv_item)
//        private CardView cv_item;

        public NewsCommentViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

//            cv_item.setOnClickListener(this);
            news_comment_like.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {

                switch (v.getId()) {
                    case R.id.cv_item:

                        itemClickListener.onItemClick(v, newsDetailBean != null ? getPosition() - 2 : getPosition());
                        break;

                    case R.id.news_comment_like:

                        itemClickListener.onItemClick(v, newsDetailBean != null ? getPosition() - 2 : getPosition());

                        break;
                }


            }
        }
    }


    /**
     * 新闻详情的viewholder
     *
     */
    public class NewsCommentHeadViewHolder extends RecyclerView.ViewHolder {

        @ViewInject(R.id.news_detail_title)
        private TextView news_detail_title;

        @ViewInject(R.id.news_detail_author)
        private TextView news_detail_author;

        @ViewInject(R.id.news_detail_time)
        private TextView news_detail_time;

//        @ViewInject(R.id.news_detail_webview)
//        private WebView news_detail_webview;

        public NewsCommentHeadViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

        }
    }


    public class MyWebViewHolder extends RecyclerView.ViewHolder{


        private WebView news_detail_webview;

        public MyWebViewHolder(View itemView) {
            super(itemView);
            news_detail_webview = (WebView) itemView;
        }
    }

}
