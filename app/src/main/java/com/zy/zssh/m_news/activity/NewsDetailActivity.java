package com.zy.zssh.m_news.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.orhanobut.logger.Logger;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.activity.LoginActivity;
import com.zy.zssh.m_news.adapter.news.NewsCommentAdapter;
import com.zy.zssh.m_news.adapter.news.NewsCommentBaseRecycleAdapter;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;
import com.zy.zssh.m_news.bean.news.NewsDetailBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * desc:新闻详情页
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 11:20
**/
public class NewsDetailActivity extends BaseActivity {

    @ViewInject(R.id.news_detail_comment)
    private RecyclerView news_detail_comment;

    @ViewInject(R.id.news_tab_swipeRefreshLayout)
    private SwipyRefreshLayout swipeRefreshLayout;

    @ViewInject(R.id.news_detail_comment_edit)
    private EditText news_detail_comment_edit;

    @ViewInject(R.id.news_detail_comment_send)
    private View news_detail_comment_send;

    @ViewInject(R.id.news_detail_comment_num)
    private ImageView news_detail_comment_num;

    private NewsCommentAdapter newsCommentAdapter;

    private String newsId = "";

    private NewsBusiness business;

    private NewsDetailBean newsDetailBean;

    private int comPage = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    private boolean isSend = false;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;
    private int firstPosition = 0;

    /**
     * 新闻评论数据
     */
    private List<NewsCommentBean> newsCommentBeanList = new ArrayList<>();

    final SHARE_MEDIA[] displaylist = new SHARE_MEDIA[]
            {
                    SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE,SHARE_MEDIA.SINA,
                    SHARE_MEDIA.QQ
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_news_detail);

        x.view().inject(this);

        initData();

        initView();
    }

    @Override
    protected void initView() {

        news_detail_comment_edit.setTag("0");
        setRightImageView(R.mipmap.share);

    }

    @Override
    public void rightImgClick() {
        super.rightImgClick();


        if(newsDetailBean!=null){


            new ShareAction(this)
                    .setDisplayList(displaylist)
                    .withText(newsDetailBean.getNewsTitle())
                    .withTargetUrl(newsDetailBean.getNewsMain())
                    .setCallback(new UMShareListener() {
                        @Override
                        public void onResult(SHARE_MEDIA share_media) {

                            Toast.makeText(NewsDetailActivity.this,share_media + " 分享成功啦", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(SHARE_MEDIA share_media, Throwable throwable) {


                            Toast.makeText(NewsDetailActivity.this,share_media + " 分享失败啦", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onCancel(SHARE_MEDIA share_media) {

                            Toast.makeText(NewsDetailActivity.this,share_media + " 分享取消了", Toast.LENGTH_SHORT).show();

                        }
                    })
                    .open();

        }


    }

    @Override
    protected void initData() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            newsId = bundle.getString("key", "");
        }

        business = NewsBusiness.getInstance();

        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        news_detail_comment.setHasFixedSize(true);
        news_detail_comment.setItemAnimator(new DefaultItemAnimator());

        news_detail_comment.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        newsCommentAdapter = new NewsCommentAdapter(NewsDetailActivity.this, newsCommentBeanList, newsDetailBean);

        news_detail_comment.setAdapter(newsCommentAdapter);

        setListener();

        getNewsData();






    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get( this ).onActivityResult( requestCode, resultCode, data);
    }

    /**
     * 设置监听
     */
    private void setListener() {


        //刷新监听事件
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {


//                Logger.d("swipeRefreshLayout");

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    getNewsData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreCommentData();
                }

            }
        });


        //内容滑动事件
        news_detail_comment.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

//                Logger.d("onScrollStateChanged");


//                Logger.d("***********");
//                Logger.d(lastPosition + 1 == newsCommentAdapter.getItemCount());
//                Logger.d(newState == RecyclerView.SCROLL_STATE_IDLE);
//                Logger.d(isLoading);
//                Logger.d(swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH);
//                Logger.d("***********");
                //recyclerview停止滑动  并且滑动到底部时加载更多内容
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == newsCommentAdapter.getItemCount()
                        && !isLoading && swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    //开始刷新
                    swipeRefreshLayout.setRefreshing(true);
                    loadMoreCommentData();

                }else{

                    //停止刷新
                    swipeRefreshLayout.setRefreshing(false);

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                lastPosition = manager.findLastVisibleItemPosition();




            }
        });


        //评论的点击事件
        newsCommentAdapter.setOnItemClickListener(new NewsCommentBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {

                switch (view.getId()) {
                    case R.id.cv_item:

                        news_detail_comment_edit.setText("@" + newsCommentBeanList.get(position).getLoginName() + ":");
                        news_detail_comment_edit.setTag(newsCommentBeanList.get(position).getComId());
                        news_detail_comment_edit.requestFocus();

                        news_detail_comment_edit.setSelection(news_detail_comment_edit.length());

                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                        break;

                    case R.id.news_comment_like:

                        view.setSelected(!view.isSelected());

                        business.setzanData(view.isSelected() ? "newscom" : "clearnewscom", newsCommentBeanList.get(position).getComId(),
                                NewsDetailActivity.this, new VolleyCallback() {
                                    @Override
                                    public void requestSuccess(VolleyResponse response) {
                                        super.requestSuccess(response);

                                        if (isSuccess(response)) {

                                            if (view.isSelected()) {
                                                Toast.makeText(NewsDetailActivity.this, "点赞", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(NewsDetailActivity.this, "取消赞", Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    }

                                    @Override
                                    public void requestError(VolleyError error) {
                                        super.requestError(error);
                                    }
                                });

                        break;
                }

            }
        });


        //评论的编辑区
        news_detail_comment_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                System.out.println("CharSequence--" + s + "--start--" + start + "--count--" + count + "--after--" + after);

                //输入内容为空时隐藏发送按钮显示评论数目按钮
                if (start == 0 && after == 0) {

                    news_detail_comment_send.setVisibility(View.GONE);
                    news_detail_comment_num.setVisibility(View.VISIBLE);

                    news_detail_comment_edit.setTag("0");
                } else {

                    news_detail_comment_send.setVisibility(View.VISIBLE);
                    news_detail_comment_num.setVisibility(View.GONE);
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        news_detail_comment_send.setOnClickListener(this);
        news_detail_comment_num.setOnClickListener(this);
    }


    /**
     * 获取新闻
     *
     */
    private void getNewsData() {
        if (!newsId.equals("")) {

            showLoadingView();

            business.getNewsDetailData(newsId, this, new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);

                    if (isSuccess(response)) {

                        comPage = 1;

                        newsDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), NewsDetailBean.class);

//                        Logger.d("新闻详情：" + newsDetailBean.toString());


                        newsCommentAdapter.setNewsDetailBean(newsDetailBean);

                        newsCommentAdapter.notifyDataSetChanged();

                        news_detail_comment_num.setSelected(newsDetailBean.getZansate().equals("1"));

                        getNewsCommentData();
                    }

                    endLoadingView();
                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);

                    endLoadingView();
                }
            });

        } else {
            Toast.makeText(NewsDetailActivity.this, "很抱歉，加载出错", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {
            case R.id.news_detail_comment_send:

                if (Config.Config(NewsDetailActivity.this).getUserId().equals("")) {

                    Toast.makeText(NewsDetailActivity.this, "登录后才能评论", Toast.LENGTH_SHORT).show();
                    openActivity(LoginActivity.class);
                } else {

                    sendComment();
                }

                break;

            case R.id.news_detail_comment_num:

                if (Config.Config(NewsDetailActivity.this).getUserId().equals("")) {

                    Toast.makeText(NewsDetailActivity.this, "登录后才能点赞", Toast.LENGTH_SHORT).show();
                    openActivity(LoginActivity.class);
                } else {

                    news_detail_comment_num.setSelected(!news_detail_comment_num.isSelected());
                    news_detail_comment_num.setEnabled(false);

                    business.setzanData(news_detail_comment_num.isSelected() ? "news" : "clearnews", newsId, NewsDetailActivity.this, new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {

                                if (news_detail_comment_num.isSelected()) {
                                    Toast.makeText(NewsDetailActivity.this, "点赞了", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(NewsDetailActivity.this, "取消赞", Toast.LENGTH_SHORT).show();
                                }

                            }
                            news_detail_comment_num.setEnabled(true);

                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                            news_detail_comment_num.setEnabled(true);
                        }
                    });
                }

                break;
        }

    }

    /**
     * 发送评论
     */
    private void sendComment() {

        //newsId=新闻编号&userId=评论者用户编号&comMain=评论内容&comId2=被评论的评论ID
        business.sendNewsCommentData(newsId, Config.Config(this).getUserId(),
                news_detail_comment_edit.getText().toString(), news_detail_comment_edit.getTag().toString(), this,
                new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);

                        if (isSuccess(response)) {
                            Toast.makeText(NewsDetailActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            news_detail_comment_edit.setText("");
                            Tools.hideKeyboard(NewsDetailActivity.this);

//                            isSend = true;
                            getNewsCommentData();


                        } else {
                            Toast.makeText(NewsDetailActivity.this, "发布失败", Toast.LENGTH_SHORT).show();
                            Tools.hideKeyboard(NewsDetailActivity.this);
                        }

                        news_detail_comment_edit.setTag("0");
                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                        news_detail_comment_edit.setTag("0");
                    }
                });
    }

    /**
     * 获取新闻评论数据
     */
    private void getNewsCommentData() {

        business.getNewsCommentData(newsId, comPage + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);


                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newscom"), NewsCommentBean.class);

                    if (aa != null) {

                        if (newsCommentBeanList.size() != 0) {

                            newsCommentBeanList.clear();
                        }


                        newsCommentBeanList.addAll(aa);

                        newsCommentAdapter.notifyDataSetChanged();

//                        if(isSend){
//
//                            news_detail_comment.smoothScrollToPosition(newsCommentAdapter.getItemCount()-1);
//                            isSend = false;
//                        }



                    }


                    //评论为空或者评论的条数小于十条就禁用加载更多操作
                    if (aa !=null && aa.size() ==10) {
                        //不足一页

//                        Logger.d("刚好十页，上下都可以加载数据");
                        //刚好十页，上下都可以加载数据
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        comPage++;

                    } else {

//                        Logger.d("不足一页");
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);

                    }

                } else {

                    Toast.makeText(NewsDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 加载更多新闻评论数据
     */
    private void loadMoreCommentData() {

        business.getNewsCommentData(newsId, comPage + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newscom"), NewsCommentBean.class);

                    if (aa != null) {

                        newsCommentBeanList.addAll(aa);

                        newsCommentAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 10) {

                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);

                    } else {

                        comPage++;
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }

                } else {

                    Toast.makeText(NewsDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();

                }

                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

//                Logger.e("加载更多内容失败");
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }
}
