package com.zy.zssh.m_news.activity.newest;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.m_news.adapter.newest.NewestStyleDetailAdapter;
import com.zy.zssh.m_news.bean.newest.NewestStyleBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;

/**
 * desc:最新款式二级页面
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 9:19
**/
public class NewestStyleDetailActivity extends BaseActivity {

//    private String proUrl = "";
//    private String proDet = "";
//
//    @ViewInject(R.id.product_ico)
//    private ImageView product_ico;
//
//    @ViewInject(R.id.product_describe)
//    private TextView product_describe;


    @ViewInject(R.id.viewPager)
    private ViewPager viewPager;


    private ArrayList<NewestStyleBean> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.activity_new_style_detail);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setHidePublicTitle(true);

        Bundle bundle = getIntent().getExtras();

//        proDet = bundle.getString("proDet");
//        proUrl = bundle.getString("proUrl");

        int position = bundle.getInt("position");
        list = (ArrayList<NewestStyleBean>) bundle.getSerializable("list");
        viewPager.setAdapter(new NewestStyleDetailAdapter(getSupportFragmentManager(),list));
        viewPager.setCurrentItem(position);



    }

    @Override
    protected void initData() {

//        ImageLoader.getInstance().displayImage(proUrl, product_ico, MyApplication.imageOptionNormal);
//
//        product_describe.setText(proDet);

    }

    @Override
    protected void initClickListener(View v) {

    }
}
