package com.zy.zssh.m_news.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.m_news.bean.yellow.YellowPageProductDetailBean;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_product_detail)
public class YellowProductDetailFragment extends Fragment {

    private boolean injected = false;

    private YellowPageProductDetailBean yellowPageProductDetailBean;

    @ViewInject(R.id.product_ico)
    private ImageView product_ico;

    @ViewInject(R.id.product_describe)
    private TextView product_describe;

    public YellowProductDetailFragment(YellowPageProductDetailBean yellowPageProductDetailBean) {
        this.yellowPageProductDetailBean = yellowPageProductDetailBean;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        injected = true;
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!injected) {
            x.view().inject(this, this.getView());
        }

        ImageLoader.getInstance().displayImage(yellowPageProductDetailBean.getProUrl()
                /*"http://img3.imgtn.bdimg.com/it/u=3423398259,509180804&fm=15&gp=0.jpg"*/
                , product_ico, MyApplication.imageOptionNormal);

        product_describe.setText(yellowPageProductDetailBean.getProName());

    }
}
