package com.zy.zssh.m_news.bean.great;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class GreatMasterDetailBean {

    /**
     * mastName : 小张
     * mastUrl : http://app.shzywh.cn/app/images/head.png
     * mian  : 正文
     */

    private String mastName;
    private String mastUrl;
    private String mian;

    public String getMastName() {
        return mastName;
    }

    public void setMastName(String mastName) {
        this.mastName = mastName;
    }

    public String getMastUrl() {
        return mastUrl;
    }

    public void setMastUrl(String mastUrl) {
        this.mastUrl = mastUrl;
    }

    public String getMian() {
        return mian;
    }

    public void setMian(String mian) {
        this.mian = mian;
    }
}
