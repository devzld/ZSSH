package com.zy.zssh.m_news.activity.great;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.great.GreatMasterProductAdapter;
import com.zy.zssh.m_news.bean.great.GreatMasterDetailBean;
import com.zy.zssh.m_news.bean.great.GreatMasterProductBean;
import com.zy.zssh.m_news.business.NewsBusiness;
import com.zy.zssh.m_news.view.MixtureTextView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GreatMasterDetailActivity extends BaseActivity {

    @ViewInject(R.id.great_master_mixtureTextView)
    private MixtureTextView great_master_mixtureTextView;

    @ViewInject(R.id.great_master_ico)
    private ImageView great_master_ico;

    @ViewInject(R.id.great_master_detail_recycleview)
    private RecyclerView great_master_detail_recycleview;

    private GreatMasterProductAdapter greatMasterProductAdapter;

    private List<GreatMasterProductBean> greatMasterProductBeanList = new ArrayList<>();

    private NewsBusiness business;

    private GreatMasterDetailBean greatMasterDetailBean;

    private String mastId = "";

    private String mastName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_great_master_detail);

        x.view().inject(this);

        Bundle bundle = getIntent().getExtras();
        mastId = bundle.getString("mastId");
        mastName = bundle.getString("mastName");

        business = NewsBusiness.getInstance();

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle(mastName);

        //recycle初始化和数据设置
        great_master_detail_recycleview.setHasFixedSize(true);
        great_master_detail_recycleview.setItemAnimator(new DefaultItemAnimator());
        great_master_detail_recycleview.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.HORIZONTAL));

        greatMasterProductAdapter = new GreatMasterProductAdapter(this, greatMasterProductBeanList);
        great_master_detail_recycleview.setAdapter(greatMasterProductAdapter);
    }

    private void diaplayData() {
        if (greatMasterDetailBean != null) {

            ImageLoader.getInstance().displayImage(greatMasterDetailBean.getMastUrl(), great_master_ico, MyApplication.imageOptionNormal);

            great_master_mixtureTextView.setText(greatMasterDetailBean.getMian());
        }
    }

    @Override
    protected void initData() {

        business.getGreatMasterDetailData(mastId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);
                if (isSuccess(response)) {

                    greatMasterDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), GreatMasterDetailBean.class);

                    diaplayData();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

        business.getGreatMasterProductData(mastId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    greatMasterProductBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "mastworklist"), GreatMasterProductBean.class));

                    greatMasterProductAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });


        greatMasterProductAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("list", (Serializable) greatMasterProductBeanList);
                bundle.putInt("position", position);

                openActivity(GreatMasterProductDetailActivity.class, bundle);
            }
        });

    }

    @Override
    protected void initClickListener(View v) {

    }
}
