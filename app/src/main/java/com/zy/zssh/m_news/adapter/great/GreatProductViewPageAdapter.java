package com.zy.zssh.m_news.adapter.great;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zy.zssh.m_news.bean.great.GreatMasterProductBean;
import com.zy.zssh.m_news.fragment.GreatProductDetailFragment;

import java.util.List;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
public class GreatProductViewPageAdapter extends FragmentPagerAdapter {

    private List<GreatMasterProductBean> greatMasterProductBeanList;

    public GreatProductViewPageAdapter(FragmentManager fm, List<GreatMasterProductBean> greatMasterProductBeanList) {
        super(fm);
        this.greatMasterProductBeanList = greatMasterProductBeanList;
    }


    @Override
    public Fragment getItem(int position) {
        return new GreatProductDetailFragment(greatMasterProductBeanList.get(position));
    }

    @Override
    public int getCount() {
        return greatMasterProductBeanList.size();
    }
}
