package com.zy.zssh.m_news.bean.yellow;

/**
 * Created by Administrator on 2016/6/6 0006.
 */
public class CityBean {

    /**
     * mapId : 1
     * mapTile : 河北省
     */

    private String mapId;
    private String mapTile;

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getMapTile() {
        return mapTile;
    }

    public void setMapTile(String mapTile) {
        this.mapTile = mapTile;
    }
}
