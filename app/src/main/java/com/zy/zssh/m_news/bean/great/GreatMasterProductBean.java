package com.zy.zssh.m_news.bean.great;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class GreatMasterProductBean implements Serializable {

    /**
     * workId : 1
     * mastName : 大鹏展翅
     * workUrl : http://app.shzywh.cn/app/images/head.png
     */

    private String workId;
    private String mastName;
    private String workUrl;

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getMastName() {
        return mastName;
    }

    public void setMastName(String mastName) {
        this.mastName = mastName;
    }

    public String getWorkUrl() {
        return workUrl;
    }

    public void setWorkUrl(String workUrl) {
        this.workUrl = workUrl;
    }
}
