package com.zy.zssh.m_news.activity.yellow;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.zy.zssh.R;
import com.zy.zssh.m_news.adapter.yellow.YellowProductViewPageAdapter;
import com.zy.zssh.m_news.bean.yellow.YellowPageProductDetailBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.List;

public class YellowPageProductDetailActivity extends AppCompatActivity {

    private List<YellowPageProductDetailBean> yellowPageProductDetailBeanList;
    private int position;

    @ViewInject(R.id.view_pager)
    private ViewPager view_pager;

    private YellowProductViewPageAdapter yellowProductViewPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yellow_page_product_detail);

        x.view().inject(this);

        initData();
    }

    private void initData() {
        yellowPageProductDetailBeanList = (List<YellowPageProductDetailBean>) getIntent().getExtras().getSerializable("list");

        position = getIntent().getExtras().getInt("position");

        yellowProductViewPageAdapter = new YellowProductViewPageAdapter(getSupportFragmentManager(), yellowPageProductDetailBeanList);

        view_pager.setAdapter(yellowProductViewPageAdapter);

        view_pager.setCurrentItem(position);
    }
}
