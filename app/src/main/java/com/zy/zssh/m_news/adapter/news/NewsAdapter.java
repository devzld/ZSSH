package com.zy.zssh.m_news.adapter.news;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.m_news.activity.great.GreatMasterActivity;
import com.zy.zssh.m_news.activity.newest.NewestStyleActivity;
import com.zy.zssh.m_news.activity.wood.WoodLoreActivity;
import com.zy.zssh.m_news.activity.yellow.YellowPageActivity;
import com.zy.zssh.m_news.bean.news.CarouselImageBean;
import com.zy.zssh.m_news.bean.news.NewsBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * desc:新闻页的实际fragment对应的adapter
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 9:33
**/
public class NewsAdapter extends NewsBaseRecycleAdapter {

    private List<NewsBean> newsBeanList;

    private List<CarouselImageBean> carouselImageBeanList;

    private ArrayList<ImageView> imageViews = new ArrayList<>();

    public NewsAdapter(Context context, List<NewsBean> newsBeanList, List<CarouselImageBean> carouselImageBeanList, boolean isHeadCount) {

        super(context);

        this.isHeadCount = isHeadCount;
        this.newsBeanList = newsBeanList;
        this.carouselImageBeanList = carouselImageBeanList;

        if (newsBeanList == null) {
            this.newsBeanList = new ArrayList<>();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //头部事件的处理
        if (holder instanceof NewsHeadViewHolder) {

            final RecyclerView.ViewHolder viewHolder = holder;

            if (carouselImageBeanList.size() != 0) {

                if (imageViews.size() == 0 && ((NewsHeadViewHolder) holder).pointViews.getChildCount() == 0) {

                    for (int i = 0; i < carouselImageBeanList.size(); i++) {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

//                        Logger.d("大图轮播：" + carouselImageBeanList.get(i).toString());


                        ImageLoader.getInstance().displayImage(carouselImageBeanList.get(i).getUrl(), imageView, MyApplication.imageOptionNormal);

                        imageViews.add(imageView);

                        ImageView pointView = new ImageView(mContext);

                        pointView.setBackgroundResource(R.drawable.point_select);

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                10, 10);

                        params.rightMargin = 20;

                        pointView.setLayoutParams(params);

                        if (i == 0) {

                            pointView.setEnabled(true);
                        } else {
                            pointView.setEnabled(false);
                        }

                        ((NewsHeadViewHolder) holder).pointViews.addView(pointView);

                    }
                }

                ((NewsHeadViewHolder) holder).viewpage.setAdapter(new myPageAdapter());

                ((NewsHeadViewHolder) holder).viewpage.setCurrentItem(Integer.MAX_VALUE / 2
                        - (Integer.MAX_VALUE / 2 % imageViews.size()));

                ((NewsHeadViewHolder) holder).viewpage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageSelected(int arg0) {
                        // TODO Auto-generated method stub

                        arg0 = arg0 % imageViews.size();

                        ((NewsHeadViewHolder) viewHolder).adtextview.setText("广告条效果" + (arg0 + 1));

                        ((NewsHeadViewHolder) viewHolder).pointViews.getChildAt(arg0).setEnabled(true);

                        ((NewsHeadViewHolder) viewHolder).pointViews.getChildAt(((NewsHeadViewHolder) viewHolder).lastpoint).setEnabled(false);

                        ((NewsHeadViewHolder) viewHolder).lastpoint = arg0;
                    }

                    @Override
                    public void onPageScrolled(int arg0, float arg1, int arg2) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onPageScrollStateChanged(int arg0) {
                        // TODO Auto-generated method stub

                        ((NewsHeadViewHolder) viewHolder).isthreadui = false;
                    }
                });

                new Timer().schedule(new TimerTask() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        ((NewsHeadViewHolder) viewHolder).viewpage.post(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                if (((NewsHeadViewHolder) viewHolder).isthreadui) {
                                    ((NewsHeadViewHolder) viewHolder).viewpage.setCurrentItem(((NewsHeadViewHolder) viewHolder).viewpage.getCurrentItem() + 1);
                                } else {
                                    ((NewsHeadViewHolder) viewHolder).isthreadui = true;
                                }
                            }
                        });
                    }
                }, ((NewsHeadViewHolder) holder).timer1, ((NewsHeadViewHolder) holder).timer2);
            }
        }
        //正常事件的处理       普通新闻数据的绑定
        else if (holder instanceof NewsContentViewHolder) {
            position = isHeadCount ? position - 1 : position;

            ImageLoader.getInstance().displayImage(getItem(position).getNewsUrl1(), ((NewsContentViewHolder) holder).tab_item_image, MyApplication.imageOptionNormal);

            NewsBean bean = getItem(position);

//            Logger.d("普通新闻：" + bean.toString());

            ((NewsContentViewHolder) holder).tab_item_title.setText(getItem(position).getNewsTitle());
            ((NewsContentViewHolder) holder).tab_item_describe.setText(getItem(position).getNewsMainjj());
            ((NewsContentViewHolder) holder).tab_item_author.setText(getItem(position).getNewsWriter());
            ((NewsContentViewHolder) holder).tab_item_time.setText(getItem(position).getAddTime());
            ((NewsContentViewHolder) holder).tab_item_view_count.setText(getItem(position).getNewsHost());
            ((NewsContentViewHolder) holder).tab_item_comment_count.setText(getItem(position).getSunCom());

        }
        //头条事件的处理
        else if (holder instanceof NewsContentHotViewHolder) {

            position = isHeadCount ? position - 1 : position;


//            Logger.d("热门新闻：" + getItem(position).toString());
            ImageLoader.getInstance().displayImage(getItem(position).getNewsUrl1(), ((NewsContentHotViewHolder) holder).tab_item_image1, MyApplication.imageOptionNormal);
            ImageLoader.getInstance().displayImage(getItem(position).getNewsUrl2(), ((NewsContentHotViewHolder) holder).tab_item_image2, MyApplication.imageOptionNormal);
            ImageLoader.getInstance().displayImage(getItem(position).getNewsUrl3(), ((NewsContentHotViewHolder) holder).tab_item_image3, MyApplication.imageOptionNormal);

            ((NewsContentHotViewHolder) holder).tab_item_title.setText(getItem(position).getNewsTitle());
            ((NewsContentHotViewHolder) holder).tab_item_author.setText(getItem(position).getNewsWriter());
            ((NewsContentHotViewHolder) holder).tab_item_time.setText(getItem(position).getAddTime());
            ((NewsContentHotViewHolder) holder).tab_item_view_count.setText(getItem(position).getNewsHost());
            ((NewsContentHotViewHolder) holder).tab_item_comment_count.setText(getItem(position).getSunCom());

            /**
             * 长图新闻的数据绑定
             *
             */
        }else if(holder instanceof NewsOnPicViewHolder){


            //获取当前位置
            position = isHeadCount ? position -1 : position;
            ImageLoader.getInstance().displayImage(getItem(position).getNewsUrl1(), ((NewsOnPicViewHolder) holder).tab_item_image1, MyApplication.imageOptionNormal);

            ((NewsOnPicViewHolder) holder).tab_item_title.setText(getItem(position).getNewsTitle());
            ((NewsOnPicViewHolder) holder).tab_item_author.setText(getItem(position).getNewsWriter());
            ((NewsOnPicViewHolder) holder).tab_item_time.setText(getItem(position).getAddTime());
            ((NewsOnPicViewHolder) holder).tab_item_view_count.setText(getItem(position).getNewsHost());
            ((NewsOnPicViewHolder) holder).tab_item_comment_count.setText(getItem(position).getSunCom());




        }

    }

    /**
     * 获取列表项
     *
     * @param position
     * @return
     */
    private NewsBean getItem(int position) {

        return newsBeanList.get(position);

    }


    /**
     * 获得类型
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {




        //如果有头部并且当位置为0时返回头部类型的
        if (isHeadCount && position == 0) {

            return ITEM_TYPE.HEAD.ordinal();

            //如果有头部则返回的position减一
        } else if (getItem(isHeadCount ? position - 1 : position).getType().equals("1")) {

            //热点新闻
            return ITEM_TYPE.HOT.ordinal();

        } else if(getItem(isHeadCount ? position - 1 : position).getType().equals("3")){

            //一张长图的新闻
            return ITEM_TYPE.ONEPIC.ordinal();

        }else{

            //其它的都返回不同新闻
            return ITEM_TYPE.CONTENT.ordinal();
        }
    }

    //头信息
    @Override
    public RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent) {
        return new NewsHeadViewHolder(mLayoutInflater.inflate(R.layout.fg_news_tab_head, parent, false));
    }



    //普通新闻
    @Override
    public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
        return new NewsContentViewHolder(mLayoutInflater.inflate(R.layout.fg_news_tab_item, parent, false));
    }



    //热门新闻
    @Override
    public RecyclerView.ViewHolder onCreateContentHotViewHolder(ViewGroup parent) {

        return new NewsContentHotViewHolder(mLayoutInflater.inflate(R.layout.fg_news_tab_item_hot, parent, false));
    }

    //创建一张长图的ViewHolder
    @Override
    public RecyclerView.ViewHolder onCreateContentOnPicViewHolder(ViewGroup parent) {
        return new NewsOnPicViewHolder(mLayoutInflater.inflate(R.layout.fg_news_tab_item_one_pic,parent,false));
    }


    /**
     * 获取实际的list列表大小
     *
     * @return
     */
    @Override
    public int getContentItemCount() {
        return newsBeanList == null ? 0 : newsBeanList.size();
    }

    @Override
    public boolean getType(int position) {

        return "1".equals(newsBeanList.get(position).getType());
    }


    private class myPageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            // TODO Auto-generated method stub

            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            // TODO Auto-generated method stub
            container.addView(imageViews.get(position % imageViews.size()));
            imageViews.get(position % imageViews.size()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(v, position % imageViews.size());
                    }
                }
            });

            return imageViews.get(position % imageViews.size());
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // TODO Auto-generated method stub

            container.removeView((View) object);
            object = null;
        }

    }


    /**
     * 头部的viewholder   大图轮播+四个圆形图片
     */
    public class NewsHeadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.parent)
        private View parent;

        @ViewInject(R.id.viewpage)
        private ViewPager viewpage;

        @ViewInject(R.id.adtextview)
        private TextView adtextview;

        @ViewInject(R.id.pointViews)
        private LinearLayout pointViews;

        @ViewInject(R.id.news_yellow_pages)
        private LinearLayout news_yellow_pages;

        @ViewInject(R.id.news_great_master)
        private LinearLayout news_great_master;

        @ViewInject(R.id.news_wood_lore)
        private LinearLayout news_wood_lore;

        @ViewInject(R.id.news_newest_style)
        private LinearLayout news_newest_style;

        private int lastpoint;
        private boolean isthreadui = true;

        /**
         * 开始广告条自动滚动的时间
         */
        private int timer1 = 3000;

        /**
         * 广告轮播间隔时间
         */
        private int timer2 = 2000;

        public NewsHeadViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            news_yellow_pages.setOnClickListener(this);
            news_great_master.setOnClickListener(this);
            news_wood_lore.setOnClickListener(this);
            news_newest_style.setOnClickListener(this);

            parent.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.news_yellow_pages:

                    mContext.startActivity(new Intent(mContext, YellowPageActivity.class));
                    break;

                case R.id.news_great_master:

                    mContext.startActivity(new Intent(mContext, GreatMasterActivity.class));
                    break;

                case R.id.news_wood_lore:

                    mContext.startActivity(new Intent(mContext, WoodLoreActivity.class));
                    break;

                case R.id.news_newest_style:

                    mContext.startActivity(new Intent(mContext, NewestStyleActivity.class));
                    break;
            }
        }
    }




    /**
     * 热门的viewholder
     */
    public class NewsContentHotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @ViewInject(R.id.tab_item_title)
        private TextView tab_item_title;

        @ViewInject(R.id.tab_item_image1)
        private ImageView tab_item_image1;

        @ViewInject(R.id.tab_item_image2)
        private ImageView tab_item_image2;

        @ViewInject(R.id.tab_item_image3)
        private ImageView tab_item_image3;

        @ViewInject(R.id.tab_item_author)
        private TextView tab_item_author;

        @ViewInject(R.id.tab_item_time)
        private TextView tab_item_time;

        @ViewInject(R.id.tab_item_view_count)
        private TextView tab_item_view_count;

        @ViewInject(R.id.tab_item_comment_count)
        private TextView tab_item_comment_count;

        @ViewInject(R.id.cv_item)
        private FrameLayout cv_item;

        public NewsContentHotViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            cv_item.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null)
                itemClickListener.onItemClick(v, isHeadCount ? getPosition() - 1 : getPosition());
        }

        @Override
        public boolean onLongClick(View v) {

            return itemLongClickListener != null && itemLongClickListener.onItemLongClick(v, isHeadCount ? getPosition() - 1 : getPosition());
        }
    }

    /**
     * 正常的viewholder
     */
    public class NewsContentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @ViewInject(R.id.tab_item_image)
        private ImageView tab_item_image;

        @ViewInject(R.id.tab_item_title)
        private TextView tab_item_title;

        @ViewInject(R.id.tab_item_describe)
        private TextView tab_item_describe;

        @ViewInject(R.id.tab_item_author)
        private TextView tab_item_author;

        @ViewInject(R.id.tab_item_time)
        private TextView tab_item_time;

        @ViewInject(R.id.tab_item_view_count)
        private TextView tab_item_view_count;

        @ViewInject(R.id.tab_item_comment_count)
        private TextView tab_item_comment_count;

        @ViewInject(R.id.cv_item)
        private View cv_item;

        public NewsContentViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            cv_item.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null)
                itemClickListener.onItemClick(v, isHeadCount ? getPosition() - 1 : getPosition());
        }

        @Override
        public boolean onLongClick(View v) {

            return itemLongClickListener != null && itemLongClickListener.onItemLongClick(v, isHeadCount ? getPosition() - 1 : getPosition());
        }
    }


    /**
     * 一张长图的ViewHolder
     *
     */
    public class NewsOnPicViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{


        @ViewInject(R.id.tab_item_title)
        private TextView tab_item_title;

        @ViewInject(R.id.tab_item_image1)
        private ImageView tab_item_image1;

        @ViewInject(R.id.tab_item_author)
        private TextView tab_item_author;

        @ViewInject(R.id.tab_item_time)
        private TextView tab_item_time;

        @ViewInject(R.id.tab_item_view_count)
        private TextView tab_item_view_count;

        @ViewInject(R.id.tab_item_comment_count)
        private TextView tab_item_comment_count;

        @ViewInject(R.id.cv_item)
        private FrameLayout cv_item;



        public NewsOnPicViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            cv_item.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null)
                //传入到当前item的位置和view
                itemClickListener.onItemClick(v, isHeadCount ? getPosition() - 1 : getPosition());

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }


}
