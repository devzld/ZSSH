package com.zy.zssh.m_news.adapter.yellow;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zy.zssh.m_news.bean.yellow.YellowPageProductDetailBean;
import com.zy.zssh.m_news.fragment.YellowProductDetailFragment;

import java.util.List;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
public class YellowProductViewPageAdapter extends FragmentPagerAdapter {

    private List<YellowPageProductDetailBean> yellowPageProductDetailBeanList;

    public YellowProductViewPageAdapter(FragmentManager fm, List<YellowPageProductDetailBean> yellowPageProductDetailBeanList) {
        super(fm);
        this.yellowPageProductDetailBeanList = yellowPageProductDetailBeanList;
    }


    @Override
    public Fragment getItem(int position) {
        return new YellowProductDetailFragment(yellowPageProductDetailBeanList.get(position));
    }

    @Override
    public int getCount() {
        return yellowPageProductDetailBeanList.size();
    }
}
