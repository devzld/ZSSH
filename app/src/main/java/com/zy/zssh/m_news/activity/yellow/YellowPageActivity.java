package com.zy.zssh.m_news.activity.yellow;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.orhanobut.logger.Logger;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.view.DetialDialog;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.activity.LoginActivity;
import com.zy.zssh.m_news.activity.SearchActivity;
import com.zy.zssh.m_news.adapter.yellow.PopCityAdapter;
import com.zy.zssh.m_news.adapter.yellow.YellowPageAdapter;
import com.zy.zssh.m_news.bean.yellow.CityBean;
import com.zy.zssh.m_news.bean.yellow.YellowPageBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
/**
 * desc:企业大黄页列表
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/23 0023 11:10
**/
public class YellowPageActivity extends BaseActivity {

    @ViewInject(R.id.news_tab_swipeRefreshLayout)
    private SwipyRefreshLayout swipeRefreshLayout;

    @ViewInject(R.id.yellow_page_recycleview)
    private RecyclerView yellow_page_recycleview;

    @ViewInject(R.id.searchContainer)
    private LinearLayout searchContainer;

    private NewsBusiness business;

    private PopupWindow popupWindow;

    //城市列表
    private List<CityBean> cityBeanList = new ArrayList<>();
    private PopCityAdapter popCityAdapter;

    //企业列表
    private List<YellowPageBean> yellowPageBeanList = new ArrayList<>();
    private YellowPageAdapter yellowPageAdapter;

    private String mapID = "0";

    private int pageNum = 1;


    private static final String IS_FIRST_OPEN = "is_first_open";



    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_yellow_page);

        x.view().inject(this);

//       if(!SharedListString.getSpString(this,IS_FIRST_OPEN).equals("no")){
//
//
//
//
//
//           //判断用户是否时第一次使用大黄页
//            DetialDialog dialog = new DetialDialog(this);
//            dialog.show(getSupportFragmentManager(),"detail");
//
//            SharedListString.saveSpString(this,IS_FIRST_OPEN,"no");
//
//       }

        DetialDialog dialog = new DetialDialog(this);
        dialog.show(getSupportFragmentManager(),"detail");


        initView();
        initData();
    }

    @Override
    protected void initView() {



        searchContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(YellowPageActivity.this, SearchActivity.class));
            }
        });

        setMiddleTitle("企业大黄页");
        setRightImageView(R.mipmap.caidan);

        yellow_page_recycleview.setHasFixedSize(true);
        yellow_page_recycleview.setItemAnimator(new DefaultItemAnimator());

        yellow_page_recycleview.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));
    }

    @Override
    protected void initData() {

        business = NewsBusiness.getInstance();

        popCityAdapter = new PopCityAdapter(this, cityBeanList);

        yellowPageAdapter = new YellowPageAdapter(this, yellowPageBeanList);
        yellow_page_recycleview.setAdapter(yellowPageAdapter);

        initCityData();
        showLoadingView();
        initBusinessData();

        setListener();
    }

    /**
     * 设置监听事件
     */
    private void setListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                //根据方向来设置相应的操作
                Logger.d("direction:" + direction);


                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    initBusinessData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreBusinessData();
                }

            }
        });

        yellow_page_recycleview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                /**
                 * 1.当前停止滑动
                 * 2.最后一个item可见
                 * 3.没有加载
                 * 4.滑动方向为双向
                 */
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == yellowPageAdapter.getItemCount()
                        && !isLoading && swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    Logger.d("--------------------------------------------------------------------");
                    Logger.d("newState:" + newState);
                    Logger.d("lastPosition:" + lastPosition);
                    Logger.d("yellowPageAdapter.getItemCount():" + yellowPageAdapter.getItemCount());
                    Logger.d("isLoading:" + isLoading);
                    Logger.d("--------------------------------------------------------------------");
                    isLoading = true;
                    swipeRefreshLayout.setRefreshing(true);
                    loadMoreBusinessData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        yellowPageAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {

                switch (view.getId()) {
                    case R.id.cv_item:

                        //传递id和name数据到详情页面
                        Bundle bundle = new Bundle();
                        bundle.putString("entId", yellowPageBeanList.get(position).getEntId());
                        bundle.putString("entName", yellowPageBeanList.get(position).getEntName());

                        openActivity(YellowPageDetailActivity.class, bundle);
                        break;

                    case R.id.yellow_page_item_like:

                        if (Config.Config(YellowPageActivity.this).getUserId().equals("")) {
                            Toast.makeText(YellowPageActivity.this, "请先登录", Toast.LENGTH_SHORT).show();
                            openActivity(LoginActivity.class);
                        } else {

                            view.setSelected(!view.isSelected());
                            view.setEnabled(false);

                            business.setzanData(view.isSelected() ? "ent" : "clearent", yellowPageBeanList.get(position).getEntId(), YellowPageActivity.this,
                                    new VolleyCallback() {
                                        @Override
                                        public void requestSuccess(VolleyResponse response) {
                                            super.requestSuccess(response);

                                            if (isSuccess(response)) {

                                                if (view.isSelected()) {
                                                    Toast.makeText(YellowPageActivity.this, "点赞", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(YellowPageActivity.this, "取消赞", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            view.setEnabled(true);
                                        }

                                        @Override
                                        public void requestError(VolleyError error) {
                                            super.requestError(error);
                                            view.setEnabled(true);
                                        }
                                    });
                        }
                        break;

                    case R.id.yellow_page_item_star_business:

                        if (Config.Config(YellowPageActivity.this).getUserId().equals("")) {
                            Toast.makeText(YellowPageActivity.this, "请先登录", Toast.LENGTH_SHORT).show();
                            openActivity(LoginActivity.class);
                        } else {

                            view.setSelected(!view.isSelected());
                            view.setEnabled(false);

                            business.setzanData(view.isSelected() ? "enrtcol" : "clearenrtcol", yellowPageBeanList.get(position).getEntId(), YellowPageActivity.this,
                                    new VolleyCallback() {
                                        @Override
                                        public void requestSuccess(VolleyResponse response) {
                                            super.requestSuccess(response);

                                            if (isSuccess(response)) {

                                                if (view.isSelected()) {
                                                    Toast.makeText(YellowPageActivity.this, "收藏", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(YellowPageActivity.this, "取消收藏", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            view.setEnabled(true);
                                        }

                                        @Override
                                        public void requestError(VolleyError error) {
                                            super.requestError(error);
                                            view.setEnabled(true);
                                        }
                                    });
                        }
                        break;
                }
            }
        });
    }

    /**
     * 初始化城市数据
     */
    private void initCityData() {
        business.getMapData(this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                List<CityBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "maplist"), CityBean.class);

                if (aa != null && aa.size() != 0) {
                    cityBeanList.clear();
                    cityBeanList.addAll(aa);
                    popCityAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });
    }

    /**
     * 获取当前城市的企业列表
     */
    private void initBusinessData() {

        pageNum = 1;

        //mapId=公司所在省会&page=页数
        business.getBusinessData(mapID, pageNum + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);


                if (isSuccess(response)) {

                    List<YellowPageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), YellowPageBean.class);

                    yellowPageBeanList.clear();
                    if (aa != null && aa.size() != 0) {
                        yellowPageBeanList.addAll(aa);
                    }
                    yellowPageAdapter.notifyDataSetChanged();

                    if (yellowPageBeanList.size() < 20) {

                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

                        pageNum++;
                    }
                }

                endLoadingView();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

                swipeRefreshLayout.setRefreshing(false);
                endLoadingView();
            }
        });
    }

    /**
     * 加载更多企业数据
     */
    private void loadMoreBusinessData() {

        //mapId=公司所在省会&page=页数
        business.getBusinessData(mapID, pageNum + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<YellowPageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), YellowPageBean.class);

                    if (aa != null && aa.size() != 0) {
                        yellowPageBeanList.addAll(aa);
                        yellowPageAdapter.notifyDataSetChanged();
                    }

                    if (aa != null && aa.size() == 20) {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNum++;
                    } else {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }

                endLoadingView();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                swipeRefreshLayout.setRefreshing(false);
                endLoadingView();
            }
        });
    }


    @Override
    protected void initClickListener(View v) {

    }

    /**
     * 右侧图片点击响应
     */
    @Override
    public void rightImgClick() {
        super.rightImgClick();

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
            popupWindow = null;
        } else {
            getPopupWindow();
            popupWindow.showAsDropDown(getRightImageView(),0,0);
        }

    }

    /**
     * 获取PopupWindow实例
     */
    private void getPopupWindow() {
        if (null != popupWindow) {
            popupWindow.dismiss();
            return;
        } else {
            initPopuptWindow();
        }
    }

    /**
     * 创建PopupWindow
     */
    protected void initPopuptWindow() {
        // TODO Auto-generated method stub
        // 获取自定义布局文件activity_popupwindow_left.xml的视图
        View popupWindow_view = getLayoutInflater().inflate(R.layout.yellow_page_city_pop, null);
        // 创建PopupWindow实例,200,LayoutParams.MATCH_PARENT分别是宽度和高度
//        popupWindow = new PopupWindow(popupWindow_view, AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT, true);

        popupWindow = new PopupWindow(popupWindow_view, Tools.dip2px(this,150),Tools.dip2px(this,200));

        // 背景不能为空，为空下面代码无效
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //设置焦点
        popupWindow.setFocusable(true);
        //设置点击其他地方 就消失
        popupWindow.setOutsideTouchable(true);

        ListView listView = (ListView) popupWindow_view.findViewById(R.id.yellow_page_city_item_listview);

        listView.setAdapter(popCityAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mapID = cityBeanList.get(position).getMapId();

                pageNum = 1;
                showLoadingView();
                initBusinessData();

                popupWindow.dismiss();
            }
        });
    }
}
