package com.zy.zssh.m_news.bean.news;

/**
 * Created by Administrator on 2016/6/2 0002.
 * 新闻详情的bean类
 */
public class NewsDetailBean {

    /**
     * newsTitle : 测试新闻1
     * newsMain : 测试新
     * <p/>
     * <img src='http://app.shzywh.cn/app/images/head.png' width='100%'>
     * <p/>
     * 闻内容1
     * SunCom : 1
     * newsHost : 4
     * addTime : 2016-5-27 8:44:09
     * newsWriter : 小张
     */

    private String newsTitle;
    private String newsMain;
    private String SunCom;
    private String newsHost;
    private String addTime;
    private String newsWriter;

    private String zansate;

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsMain() {
        return newsMain;
    }

    public void setNewsMain(String newsMain) {
        this.newsMain = newsMain;
    }

    public String getSunCom() {
        return SunCom;
    }

    public void setSunCom(String SunCom) {
        this.SunCom = SunCom;
    }

    public String getNewsHost() {
        return newsHost;
    }

    public void setNewsHost(String newsHost) {
        this.newsHost = newsHost;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getNewsWriter() {
        return newsWriter;
    }

    public void setNewsWriter(String newsWriter) {
        this.newsWriter = newsWriter;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }


    @Override
    public String toString() {
        return "NewsDetailBean{" +
                "newsTitle='" + newsTitle + '\'' +
                ", newsMain='" + newsMain + '\'' +
                ", SunCom='" + SunCom + '\'' +
                ", newsHost='" + newsHost + '\'' +
                ", addTime='" + addTime + '\'' +
                ", newsWriter='" + newsWriter + '\'' +
                ", zansate='" + zansate + '\'' +
                '}';
    }
}
