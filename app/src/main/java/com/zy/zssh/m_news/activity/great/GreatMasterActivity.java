package com.zy.zssh.m_news.activity.great;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.adapter.great.GreatMasterAdapter;
import com.zy.zssh.m_news.bean.great.GreatMasterBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

public class GreatMasterActivity extends BaseActivity {

    @ViewInject(R.id.great_master_recycleview)
    private RecyclerView great_master_recycleview;

    private List<GreatMasterBean> greatMasterBeanList = new ArrayList<>();

    private GreatMasterAdapter greatMasterAdapter;

    private NewsBusiness business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_great_master);

        x.view().inject(this);

        initView();
        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("大师风采");

        //recycle初始化和数据设置
        great_master_recycleview.setHasFixedSize(true);
        great_master_recycleview.setItemAnimator(new DefaultItemAnimator());
        great_master_recycleview.setLayoutManager(new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL));

        business = NewsBusiness.getInstance();

        greatMasterAdapter = new GreatMasterAdapter(this, greatMasterBeanList);

        great_master_recycleview.setAdapter(greatMasterAdapter);

        greatMasterAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();

                bundle.putString("mastId", greatMasterBeanList.get(position).getMastId());
                bundle.putString("mastName", greatMasterBeanList.get(position).getMastName());

                openActivity(GreatMasterDetailActivity.class, bundle);
            }
        });
    }

    @Override
    protected void initData() {

        business.getGreatMasterData(this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);
                if (isSuccess(response)) {

                    greatMasterBeanList.clear();

                    greatMasterBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "mastlist"), GreatMasterBean.class));

                    greatMasterAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });

    }

    @Override
    protected void initClickListener(View v) {

    }
}
