package com.zy.zssh.m_news.adapter.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.bean.news.NewsDetailBean;

/**
 * Created by devin on 16/4/27.
 */
public abstract class NewsCommentBaseRecycleAdapter extends BaseRecycleAdapter {

    protected enum ITEM_TYPE {
        HEAD, CONTENT,WEBVIEW
    }

    protected NewsDetailBean newsDetailBean;

    public NewsCommentBaseRecycleAdapter(Context mContext, NewsDetailBean newsDetailBean) {
        super(mContext);
        this.newsDetailBean = newsDetailBean;
    }

    public void setNewsDetailBean(NewsDetailBean newsDetailBean) {
        this.newsDetailBean = newsDetailBean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE.HEAD.ordinal() && newsDetailBean != null) {

            return onCreateHeadViewHolder(parent);
        } else if (viewType == ITEM_TYPE.CONTENT.ordinal()) {

            return onCreateContentViewHolder(parent);
        }else if(viewType == ITEM_TYPE.WEBVIEW.ordinal()){

            return onCreateWebViewHolder(parent);


        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {


        //
        if (position == 0 && newsDetailBean != null) {

            return ITEM_TYPE.HEAD.ordinal();
        } else if(position == 1){

            return ITEM_TYPE.WEBVIEW.ordinal();

        }else{
            return ITEM_TYPE.CONTENT.ordinal();

        }
    }

    public abstract RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateWebViewHolder(ViewGroup parent);


}
