package com.zy.zssh.m_news.adapter.wood;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.bean.wood.WoodLoreBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class WoodLoreAdapter extends BaseRecycleAdapter {

    private List<WoodLoreBean> woodLoreBeanList;

    public WoodLoreAdapter(Context mContext, List<WoodLoreBean> woodLoreBeanList) {
        super(mContext);

        if (woodLoreBeanList == null) {
            woodLoreBeanList = new ArrayList<>();
        }

        this.woodLoreBeanList = woodLoreBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WoodLoreViewHolder(mLayoutInflater.inflate(R.layout.greate_master_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(woodLoreBeanList.get(position).getMoodUrl(),
                ((WoodLoreViewHolder) holder).great_master_ico, MyApplication.imageOptionNormal5);

        ((WoodLoreViewHolder) holder).great_master_name.setText(woodLoreBeanList.get(position).getMoodName());
    }

    @Override
    public int getItemCount() {
        return woodLoreBeanList.size();
    }

    public class WoodLoreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.great_master_ico)
        private ImageView great_master_ico;

        @ViewInject(R.id.great_master_name)
        private TextView great_master_name;

        @ViewInject(R.id.cv_item)
        private CardView cv_item;

        public WoodLoreViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
