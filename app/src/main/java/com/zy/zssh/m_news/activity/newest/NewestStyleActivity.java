package com.zy.zssh.m_news.activity.newest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.orhanobut.logger.Logger;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.newest.NewestStyleAdapter;
import com.zy.zssh.m_news.bean.newest.NewestStyleBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * desc:最新款式页面
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/28 0028 9:21
**/
public class NewestStyleActivity extends BaseActivity {

    @ViewInject(R.id.loadMoreStyle)
    SwipyRefreshLayout loadMoreStyle;
    @ViewInject(R.id.great_master_recycleview)
    private RecyclerView great_master_recycleview;


    private List<NewestStyleBean> newestStyleBeanList = new ArrayList<>();

    private NewestStyleAdapter newestStyleAdapter;

    private NewsBusiness business;

    private int currentPage = 1;

    private boolean isLoading = false;

    private int lastPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_great_master);
        x.view().inject(this);

        initView();
        initData();

    }

    @Override
    protected void initView() {

        setMiddleTitle("最新款式");

        //recycle初始化和数据设置
        great_master_recycleview.setHasFixedSize(true);
        great_master_recycleview.setItemAnimator(new DefaultItemAnimator());
        great_master_recycleview.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL));

        business = NewsBusiness.getInstance();

        newestStyleAdapter = new NewestStyleAdapter(this, newestStyleBeanList);

        great_master_recycleview.setAdapter(newestStyleAdapter);

        newestStyleAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();

//                bundle.putString("proUrl", newestStyleBeanList.get(position).getProUrl());
//                bundle.putString("proDet", newestStyleBeanList.get(position).getProDet());

                bundle.putSerializable("list", (Serializable) newestStyleBeanList);
                bundle.putInt("position",position);
                //将当前页面的集合传给下一个页面

//                openActivity(NewestStyleDetailActivity.class, bundle);
                Intent intent = new Intent(NewestStyleActivity.this,NewestStyleDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);


            }

        });




        //配置swipeRefreshLayout
        loadMoreStyle.setDirection(SwipyRefreshLayoutDirection.BOTH);
        loadMoreStyle.setColorSchemeColors(getResources().getColor(R.color.refresh_color));
        loadMoreStyle.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {


                if(direction == SwipyRefreshLayoutDirection.TOP){


                    initData();

                }else if(direction == SwipyRefreshLayoutDirection.BOTTOM){

                    loadMoreData();


                }

            }
        });


        //配置recyleview
        great_master_recycleview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);



                //如果当前没有正在加载,并且在最后一页
                if(!isLoading && newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition+1 == newestStyleAdapter.getItemCount()
                        && loadMoreStyle.getDirection() == SwipyRefreshLayoutDirection.BOTH){



//                    Logger.d("当前可见位置：" + lastPosition);
                    loadMoreData();

                    //判断最后一个可见的item距离底部的距离


                }





            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int[] lastItems = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPositions(null);

                int max = 0;

                for(int i:lastItems){

                    if(i>max){

                        max = i;

                    }

                }

                lastPosition = max;

                //Logger.d("最后的可见位置：" + lastPosition);


            }
        });






    }

    private void loadMoreData() {


        String isVip = Config.Config(this).getUserSate();
        if(TextUtils.isEmpty(isVip)||isVip.equals("0")){


            Tools.Toast(this,"非会员，不能加载更多");
            loadMoreStyle.setRefreshing(false);

        }else{


            business.loadMoreStyleData(this, currentPage + "", new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);


                    //Logger.d(response.getBody().toString());

                    if(isSuccess(response)){

                        newestStyleBeanList.addAll(FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(),"comprolist"),NewestStyleBean.class));
                        newestStyleAdapter.notifyDataSetChanged();

                    }else{


                        Tools.Toast(NewestStyleActivity.this,"服务器异常");

                    }

                    loadMoreStyle.setRefreshing(false);
                    isLoading = false;


                }



                @Override
                public void requestError(VolleyError error) {

                    super.requestError(error);

                    isLoading = false;
                    loadMoreStyle.setRefreshing(false);
                }



            });


        }




    }


    @Override
    protected void initData() {

        showLoadingView();

        business.getNewestStyleData(this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                //Logger.d(response.getBody().toString());

                if (isSuccess(response)) {

                    newestStyleBeanList.clear();

                    newestStyleBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "comprolist"), NewestStyleBean.class));

                    newestStyleAdapter.notifyDataSetChanged();


                }else{

                    Tools.Toast(NewestStyleActivity.this,"服务器异常");


                }

                loadMoreStyle.setRefreshing(false);
                endLoadingView();


            }

            @Override
            public void requestError(VolleyError error) {

                super.requestError(error);
                Logger.e(error.toString());
                loadMoreStyle.setRefreshing(false);
                endLoadingView();

            }

        });

    }

    @Override
    protected void initClickListener(View v) {

    }
}
