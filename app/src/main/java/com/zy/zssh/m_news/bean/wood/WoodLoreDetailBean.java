package com.zy.zssh.m_news.bean.wood;

/**
 * Created by Administrator on 2016/6/14 0014.
 */
public class WoodLoreDetailBean {


    /**
     * moodName : 大红酸枝
     * moodUrl : http://app.shzywh.cn/app/images/head.png
     * mian  : 大红酸枝main
     */

    private String moodName;
    private String moodUrl;
    private String mian;

    public String getMoodName() {
        return moodName;
    }

    public void setMoodName(String moodName) {
        this.moodName = moodName;
    }

    public String getMoodUrl() {
        return moodUrl;
    }

    public void setMoodUrl(String moodUrl) {
        this.moodUrl = moodUrl;
    }

    public String getMian() {
        return mian;
    }

    public void setMian(String mian) {
        this.mian = mian;
    }
}
