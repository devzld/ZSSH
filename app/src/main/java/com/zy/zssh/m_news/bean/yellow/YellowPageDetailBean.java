package com.zy.zssh.m_news.bean.yellow;

/**
 * Created by Administrator on 2016/6/11 0011.
 */
public class YellowPageDetailBean {


    /**
     * entName : 明工坊红木家具
     * entUrl : http://app.shzywh.cn/app/images/head1.png
     * entCon : 联系
     * entMain : 正文
     * entLon : 0
     * entLat : 0
     * sate : 0
     * zansate : 0
     * colsate : 0
     */

    private String entName;
    private String entUrl;
    private String entCon;
    private String entMain;
    private String entLon;
    private String entLat;
    private String sate;
    private String zansate;
    private String colsate;

    public String getEntName() {
        return entName;
    }

    public void setEntName(String entName) {
        this.entName = entName;
    }

    public String getEntUrl() {
        return entUrl;
    }

    public void setEntUrl(String entUrl) {
        this.entUrl = entUrl;
    }

    public String getEntCon() {
        return entCon;
    }

    public void setEntCon(String entCon) {
        this.entCon = entCon;
    }

    public String getEntMain() {
        return entMain;
    }

    public void setEntMain(String entMain) {
        this.entMain = entMain;
    }

    public String getEntLon() {
        return entLon;
    }

    public void setEntLon(String entLon) {
        this.entLon = entLon;
    }

    public String getEntLat() {
        return entLat;
    }

    public void setEntLat(String entLat) {
        this.entLat = entLat;
    }

    public String getSate() {
        return sate;
    }

    public void setSate(String sate) {
        this.sate = sate;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }

    public String getColsate() {
        return colsate;
    }

    public void setColsate(String colsate) {
        this.colsate = colsate;
    }
}
