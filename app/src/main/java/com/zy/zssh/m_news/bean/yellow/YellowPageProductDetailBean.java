package com.zy.zssh.m_news.bean.yellow;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
public class YellowPageProductDetailBean implements Serializable {

    /**
     * proId : 1
     * proName : 产品1
     * proDet : 介绍1
     * proUrl : http:app.shzywh.com/app/images/head1.png
     * proMet : 大红酸枝
     * proGuige  : 规格
     */

    private String proId;
    private String proName;
    private String proDet;
    private String proUrl;
    private String proMet;
    private String proGuige;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProDet() {
        return proDet;
    }

    public void setProDet(String proDet) {
        this.proDet = proDet;
    }

    public String getProUrl() {
        return proUrl;
    }

    public void setProUrl(String proUrl) {
        this.proUrl = proUrl;
    }

    public String getProMet() {
        return proMet;
    }

    public void setProMet(String proMet) {
        this.proMet = proMet;
    }

    public String getProGuige() {
        return proGuige;
    }

    public void setProGuige(String proGuige) {
        this.proGuige = proGuige;
    }
}
