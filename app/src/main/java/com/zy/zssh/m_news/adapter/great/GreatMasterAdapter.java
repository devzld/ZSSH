package com.zy.zssh.m_news.adapter.great;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.bean.great.GreatMasterBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class GreatMasterAdapter extends BaseRecycleAdapter {

    private List<GreatMasterBean> greatMasterBeanList;

    public GreatMasterAdapter(Context mContext, List<GreatMasterBean> greatMasterBeanList) {
        super(mContext);

        if (greatMasterBeanList == null) {
            greatMasterBeanList = new ArrayList<>();
        }

        this.greatMasterBeanList = greatMasterBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GreatMasterViewHolder(mLayoutInflater.inflate(R.layout.greate_master_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(greatMasterBeanList.get(position).getMastUrl(),
                ((GreatMasterViewHolder) holder).great_master_ico, MyApplication.imageOptionNormal5);

        ((GreatMasterViewHolder) holder).great_master_name.setText(greatMasterBeanList.get(position).getMastName());
    }

    @Override
    public int getItemCount() {
        return greatMasterBeanList.size();
    }

    public class GreatMasterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.great_master_ico)
        private ImageView great_master_ico;

        @ViewInject(R.id.great_master_name)
        private TextView great_master_name;

        @ViewInject(R.id.cv_item)
        private FrameLayout cv_item;

        public GreatMasterViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
