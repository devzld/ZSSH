package com.zy.zssh.m_news.activity.great;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.zy.zssh.R;
import com.zy.zssh.m_news.adapter.great.GreatProductViewPageAdapter;
import com.zy.zssh.m_news.bean.great.GreatMasterProductBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.List;

public class GreatMasterProductDetailActivity extends AppCompatActivity {

    private List<GreatMasterProductBean> greatMasterProductBeanList;
    private int position;

    @ViewInject(R.id.view_pager)
    private ViewPager view_pager;

    private GreatProductViewPageAdapter greatProductViewPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_great_master_product_detail);
        x.view().inject(this);

        initData();
    }

    private void initData() {
        greatMasterProductBeanList = (List<GreatMasterProductBean>) getIntent().getExtras().getSerializable("list");

        position = getIntent().getExtras().getInt("position");

        greatProductViewPageAdapter = new GreatProductViewPageAdapter(getSupportFragmentManager(), greatMasterProductBeanList);

        view_pager.setAdapter(greatProductViewPageAdapter);

        view_pager.setCurrentItem(position);
    }

}
