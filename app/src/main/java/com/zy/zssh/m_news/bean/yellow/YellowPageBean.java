package com.zy.zssh.m_news.bean.yellow;

/**
 * Created by Administrator on 2016/6/7 0007.
 */
public class YellowPageBean {

    /**
     * entId	公司编号	公司编号
     * entName	公司名称	公司名称
     * entUrl	公司缩略图	公司缩略图
     * entZan	点赞	被点赞的次数
     * entSore	评价星级	数字形式如：4为4颗心
     * Sate	认证状态	1为认证0为未认证
     * colsate 是否收藏   0否1是
     * zansate 是否点赞   0否1是
     */

    private String entId;
    private String entName;
    private String entUrl;
    private String entZan;
    private String entSore;
    private String Sate;
    private String colsate;
    private String zansate;

    public String getEntId() {
        return entId;
    }

    public void setEntId(String entId) {
        this.entId = entId;
    }

    public String getEntName() {
        return entName;
    }

    public void setEntName(String entName) {
        this.entName = entName;
    }

    public String getEntUrl() {
        return entUrl;
    }

    public void setEntUrl(String entUrl) {
        this.entUrl = entUrl;
    }

    public String getEntZan() {
        return entZan;
    }

    public void setEntZan(String entZan) {
        this.entZan = entZan;
    }

    public String getEntSore() {
        return entSore;
    }

    public void setEntSore(String entSore) {
        this.entSore = entSore;
    }

    public String getSate() {
        return Sate;
    }

    public void setSate(String Sate) {
        this.Sate = Sate;
    }

    public String getColsate() {
        return colsate;
    }

    public void setColsate(String colsate) {
        this.colsate = colsate;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }
}
