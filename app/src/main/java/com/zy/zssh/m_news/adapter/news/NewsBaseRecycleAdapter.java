package com.zy.zssh.m_news.adapter.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.zy.zssh.common.adapter.BaseRecycleAdapter;

/**
 * Created by devin on 16/4/27.
 */
public abstract class NewsBaseRecycleAdapter extends BaseRecycleAdapter {

    protected enum ITEM_TYPE {
        HEAD, CONTENT, HOT,ONEPIC
    }


    //是否有头部信息，构造函数进行初始化
    protected boolean isHeadCount = false;

    public NewsBaseRecycleAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE.HEAD.ordinal()) {

            return onCreateHeadViewHolder(parent);

        } else if (viewType == ITEM_TYPE.CONTENT.ordinal()) {

            return onCreateContentViewHolder(parent);

        } else if (viewType == ITEM_TYPE.HOT.ordinal()) {

            return onCreateContentHotViewHolder(parent);

            //创建一张新闻的viewholder
        } else if(viewType == ITEM_TYPE.ONEPIC.ordinal()){

            return onCreateContentOnPicViewHolder(parent);

        }

        return null;
    }

    //讲返回的内省交给子类进行处理
//    //返回item的type
//    @Override
//    public int getItemViewType(int position) {
//
//        //如果有头部并且当位置为0时返回头部类型的
//        if (isHeadCount && position == 0) {
//
//            return ITEM_TYPE.HEAD.ordinal();
//
//            //如果有头部则返回的position减一
//        } else if (getType(isHeadCount ? position - 1 : position)) {
//
//            return ITEM_TYPE.HOT.ordinal();
//
//        } else {
//
//            return ITEM_TYPE.CONTENT.ordinal();
//        }
//    }

    @Override
    public int getItemCount() {
        return (isHeadCount ? 1 : 0) + getContentItemCount();
    }

    public abstract RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateContentHotViewHolder(ViewGroup parent);

    //只显示一张图片的板式
    public abstract RecyclerView.ViewHolder onCreateContentOnPicViewHolder(ViewGroup parent);




    public abstract int getContentItemCount();

    /**
     * 获取item的类型
     * true为hot false为普通的
     *
     * @return
     */
    public abstract boolean getType(int position);

}
