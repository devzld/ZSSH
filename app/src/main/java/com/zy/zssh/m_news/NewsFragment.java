package com.zy.zssh.m_news;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.dao.DbUtilsDao;
import com.zy.zssh.common.dbBean.ChannelItem;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.activity.ChannelManagerActivity;
import com.zy.zssh.m_news.adapter.news.NewsViewPageAdapter;
import com.zy.zssh.m_news.business.NewsBusiness;

import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * desc:新闻页对应的fragment
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 9:22
**/
@ContentView(R.layout.fg_news)
public class NewsFragment extends BaseFragment {

    private String TAG = "NewsFragment";

    @ViewInject(R.id.tablayout)
    private TabLayout tabs;

    @ViewInject(R.id.view_pager)
    private ViewPager pager;

    private NewsViewPageAdapter newsViewPageAdapter;

    private List<ChannelItem> channelItemList = new ArrayList<>();

    private NewsBusiness business;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        business = NewsBusiness.getInstance();

//
//        try {
//            channelItemList = DbUtilsDao.getDbManager().selector(ChannelItem.class).where("sate", "in", new String[]{"1"}).findAll();
//        } catch (DbException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();

        init();
    }

    private void init() {
        if (Config.ConfigSyn(getActivity()).getIsFirst()) {

            //Logger.d("第一次使用");

            business.getAllChannelData(getActivity(), new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);

                    if (isSuccess(response)) {

                        List<ChannelItem> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "newslist"), ChannelItem.class);

                        if (aa != null && aa.size() != 0) {

                            try {
                                DbUtilsDao.getDbManager().dropTable(ChannelItem.class);
                                DbUtilsDao.getDbManager().save(aa);
                            } catch (DbException e) {
                                e.printStackTrace();
                            }

                            try {
                                channelItemList = DbUtilsDao.getDbManager().selector(ChannelItem.class).where("sate", "in", new String[]{"1"}).findAll();
                            } catch (DbException e) {
                                e.printStackTrace();
                            }


                            Config.ConfigSyn(getActivity()).setIsFrist(false);

                            newsViewPageAdapter = new NewsViewPageAdapter(getActivity().getSupportFragmentManager(), channelItemList);

                            pager.setAdapter(newsViewPageAdapter);

                            tabs.setupWithViewPager(pager);
                            tabs.setTabTextColors(getResources().getColor(R.color.select_text), Color.WHITE);
                        }

                    }

                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);
                }
            });
        } else {


            try {
                channelItemList = DbUtilsDao.getDbManager().selector(ChannelItem.class).where("sate", "in", new String[]{"1"}).findAll();
            } catch (DbException e) {
                e.printStackTrace();
            }

            newsViewPageAdapter = new NewsViewPageAdapter(getActivity().getSupportFragmentManager(), channelItemList);

            pager.setAdapter(newsViewPageAdapter);

            tabs.setupWithViewPager(pager);
            tabs.setTabTextColors(getResources().getColor(R.color.select_text), Color.WHITE);
        }
    }

    /**
     * @param view 点击add添加频道执行函数
     */
    @Event(value = R.id.add_channel)
    private void addCannel(View view) {

        startActivityForResult(new Intent(getActivity(), ChannelManagerActivity.class), 43);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 43) {
            //用于保存修改前的位置
            int a = 0;
            String str;

            if (channelItemList.size() == 0) {
                str = "";
            } else {
                str = channelItemList.get(pager.getCurrentItem()).getClassName();
            }

            try {
                channelItemList.clear();
                channelItemList.addAll(DbUtilsDao.getDbManager().selector(ChannelItem.class).where("sate", "in", new String[]{"1"}).findAll());
            } catch (DbException e) {
                e.printStackTrace();
            }

            newsViewPageAdapter.notifyDataSetChanged();
            //防止所在位置被删除，附近两个视图不重新创建
            pager.setAdapter(newsViewPageAdapter);

            for (int i = 0; i < channelItemList.size(); i++) {
                if (channelItemList.get(i).getClassName().equals(str)) {
                    a = i;
                    break;
                }
            }
            //保持添加前后的位置不变
            if (a != (channelItemList.size() - 1)) {
                pager.setCurrentItem(a);
            } else {
                pager.setCurrentItem(0);
            }

            StringBuilder channel = new StringBuilder();

            for (int i = 0; i < channelItemList.size(); i++) {

                channel.append(channelItemList.get(i).getClassId() + ",");
            }

            if (!Config.Config(getActivity()).getUserId().equals("")) {

                business.updateChannelData(channel.toString().substring(0, channel.length() - 1), getActivity(), new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);

                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                    }
                });
            }

        }
    }
}
