package com.zy.zssh.m_news.adapter.great;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.m_news.bean.great.GreatMasterProductBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/12 0012.
 */
public class GreatMasterProductAdapter extends BaseRecycleAdapter {

    private List<GreatMasterProductBean> greatMasterProductBeanList;

    public GreatMasterProductAdapter(Context mContext, List<GreatMasterProductBean> greatMasterProductBeanList) {
        super(mContext);

        this.greatMasterProductBeanList = greatMasterProductBeanList;

        if (this.greatMasterProductBeanList == null) {
            this.greatMasterProductBeanList = new ArrayList<>();
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageView imageView = new ImageView(mContext);

        int size = Tools.getGridViewImageSize(mContext, 0, 0, 3);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, Tools.dip2px(MyApplication.getContext(), 70));
        params.setMargins(10, 10, 10, 10);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(params);

        return new YellowPageProductViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(greatMasterProductBeanList.get(position).getWorkUrl(),
                ((YellowPageProductViewHolder) holder).imageView, MyApplication.imageOptionNormal);

    }

    @Override
    public int getItemCount() {
        return greatMasterProductBeanList.size();
    }

    public class YellowPageProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;

        public YellowPageProductViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView;
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
