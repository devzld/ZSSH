package com.zy.zssh.m_news.bean.wood;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class WoodLoreBean {

    /**
     * moodId : 1
     * moodName : 大红酸枝
     * moodUrl : http://app.shzywh.cn/app/images/head.png
     */

    private String moodId;
    private String moodName;
    private String moodUrl;

    public String getMoodId() {
        return moodId;
    }

    public void setMoodId(String moodId) {
        this.moodId = moodId;
    }

    public String getMoodName() {
        return moodName;
    }

    public void setMoodName(String moodName) {
        this.moodName = moodName;
    }

    public String getMoodUrl() {
        return moodUrl;
    }

    public void setMoodUrl(String moodUrl) {
        this.moodUrl = moodUrl;
    }
}
