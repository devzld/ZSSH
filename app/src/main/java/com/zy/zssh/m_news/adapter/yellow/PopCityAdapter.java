package com.zy.zssh.m_news.adapter.yellow;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.m_news.bean.yellow.CityBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/6 0006.
 */
public class PopCityAdapter extends BaseAdapter {

    private Context context;
    private List<CityBean> cityBeanList;
    private LayoutInflater mLayoutInflater;

    public PopCityAdapter(Context context, List<CityBean> cityBeanList) {
        this.context = context;
        this.cityBeanList = cityBeanList;

        if (this.cityBeanList == null)
            this.cityBeanList = new ArrayList<>();

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cityBeanList.size();
    }

    @Override
    public CityBean getItem(int position) {
        return cityBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        TextView textView = (TextView) mLayoutInflater.inflate(R.layout.yellow_page_city_pop_item, null).findViewById(R.id.yellow_page_city_pop_item_tv);

        TextView textView = new TextView(context);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        int padding = Tools.dip2px(context,10);
        textView.setPadding(padding,padding,padding,padding);
        textView.setTextSize(Tools.dip2px(context,12));
        textView.setGravity(Gravity.CENTER);
        textView.setText(getItem(position).getMapTile());

        return textView;
    }
}
