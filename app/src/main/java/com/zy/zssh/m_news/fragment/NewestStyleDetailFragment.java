package com.zy.zssh.m_news.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.m_news.bean.newest.NewestStyleBean;

/**
 * Created by Administrator on 2016/7/28 0028.
 */
@SuppressLint("ValidFragment")
public class NewestStyleDetailFragment extends Fragment{


    private NewestStyleBean styleBean;

    public NewestStyleDetailFragment(NewestStyleBean styleBean) {
        this.styleBean = styleBean;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fg_product_detail,null);
        if(styleBean!=null){


            ImageView product_ico = (ImageView) view.findViewById(R.id.product_ico);
            TextView product_describe = (TextView) view.findViewById(R.id.product_describe);
            ImageLoader.getInstance().displayImage(styleBean.getProUrl(),product_ico, MyApplication.imageOptionNormal);
            product_describe.setText(styleBean.getProName());

        }
        return view;
    }






}
