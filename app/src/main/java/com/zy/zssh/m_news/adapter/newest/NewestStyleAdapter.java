package com.zy.zssh.m_news.adapter.newest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_news.bean.newest.NewestStyleBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class NewestStyleAdapter extends BaseRecycleAdapter {

    private List<NewestStyleBean> newestStyleBeanList;

    public NewestStyleAdapter(Context mContext, List<NewestStyleBean> newestStyleBeanList) {
        super(mContext);

        if (newestStyleBeanList == null) {
            newestStyleBeanList = new ArrayList<>();
        }

        this.newestStyleBeanList = newestStyleBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GreatMasterViewHolder(mLayoutInflater.inflate(R.layout.greate_master_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(newestStyleBeanList.get(position).getProUrl(),
                ((GreatMasterViewHolder) holder).great_master_ico, MyApplication.imageOptionNormal);

        ((GreatMasterViewHolder) holder).great_master_name.setText(newestStyleBeanList.get(position).getProName());
    }

    @Override
    public int getItemCount() {
        return newestStyleBeanList.size();
    }

    public class GreatMasterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.great_master_ico)
        private ImageView great_master_ico;

        @ViewInject(R.id.great_master_name)
        private TextView great_master_name;

        @ViewInject(R.id.cv_item)
        private LinearLayout cv_item;

        public GreatMasterViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
