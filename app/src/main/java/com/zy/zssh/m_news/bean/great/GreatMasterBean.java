package com.zy.zssh.m_news.bean.great;

/**
 * 项目名称：ZSSH_Android
 * 类描述：
 * 创建人：HJ
 * 创建时间：2016/6/12 20:09
 */
public class GreatMasterBean {

    /**
     * mastId : 1
     * mastName : 小张
     * mastUrl : http://app.shzywh.cn/app/images/head.png
     */

    private String mastId;
    private String mastName;
    private String mastUrl;

    public String getMastId() {
        return mastId;
    }

    public void setMastId(String mastId) {
        this.mastId = mastId;
    }

    public String getMastName() {
        return mastName;
    }

    public void setMastName(String mastName) {
        this.mastName = mastName;
    }

    public String getMastUrl() {
        return mastUrl;
    }

    public void setMastUrl(String mastUrl) {
        this.mastUrl = mastUrl;
    }
}
