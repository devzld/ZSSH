package com.zy.zssh.m_news.bean.news;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/30 0030.
 * 新闻列表的bean类
 */
public class NewsBean implements Serializable {

    /**
     * "newsId": "5",
     * "newsTitle": "测试新闻5",
     * "newsUrl1": "http://app.shzywh.cn/images/news1.jpg",
     * "SunCom": "0",
     * "newsHost": "0",
     * "addTime": "2016-5-31 16:18:56",
     * "newsWriter": "小张",
     * "newszan": "0",
     * "newsUrl2": "http://app.shzywh.cn/images/news2.jpg",
     * "newsUrl3": "http://app.shzywh.cn/images/news3.jpg",
     * "type ": "0",
     * "newsMainjj": "jian_jie"
     */


    /**
     * 新闻编号
     */
    private String newsId;
    /**
     * 新闻标题
     */
    private String newsTitle;
    /**
     * 新闻缩略图1
     */
    private String newsUrl1;

    /**
     * 新闻缩略图2
     */
    private String newsUrl2;

    /**
     * 新闻缩略图3
     */
    private String newsUrl3;

    /**
     * 新闻的概要描述
     */
    private String newsMainjj;

    /**
     * 新闻评论数默认0
     */
    private String SunCom;
    /**
     * 新闻热度默认0
     */
    private String newsHost;
    /**
     * 添加新闻的时间
     */
    private String addTime;
    /**
     * 作者
     */
    private String newsWriter;
    /**
     * 点赞数量
     */
    private String newszan;

    /**
     * 1是hot   0是普通
     */
    private String type;

    public NewsBean() {
    }

    public NewsBean(String newsId, String newsTitle, String newsUrl1, String newsUrl2, String newsUrl3,
                    String newsMainjj, String sunCom, String newsHost, String addTime, String newsWriter, String newszan, String type) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.newsUrl1 = newsUrl1;
        this.newsUrl2 = newsUrl2;
        this.newsUrl3 = newsUrl3;
        this.newsMainjj = newsMainjj;
        this.SunCom = sunCom;
        this.newsHost = newsHost;
        this.addTime = addTime;
        this.newsWriter = newsWriter;
        this.newszan = newszan;
        this.type = type;
    }

    /**
     * 1是hot   0是普通
     */
    public String getType() {
        return type;
    }

    /**
     * 1是hot   0是普通
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsUrl1() {
        return newsUrl1;
    }

    public void setNewsUrl1(String newsUrl1) {
        this.newsUrl1 = newsUrl1;
    }

    public String getNewsUrl2() {
        return newsUrl2;
    }

    public void setNewsUrl2(String newsUrl2) {
        this.newsUrl2 = newsUrl2;
    }

    public String getNewsUrl3() {
        return newsUrl3;
    }

    public void setNewsUrl3(String newsUrl3) {
        this.newsUrl3 = newsUrl3;
    }

    public String getNewsMainjj() {
        return newsMainjj;
    }

    public void setNewsMainjj(String newsMainjj) {
        this.newsMainjj = newsMainjj;
    }

    public String getSunCom() {
        return SunCom;
    }

    public void setSunCom(String SunCom) {
        this.SunCom = SunCom;
    }

    public String getNewsHost() {
        return newsHost;
    }

    public void setNewsHost(String newsHost) {
        this.newsHost = newsHost;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getNewsWriter() {
        return newsWriter;
    }

    public void setNewsWriter(String newsWriter) {
        this.newsWriter = newsWriter;
    }

    public String getNewszan() {
        return newszan;
    }

    public void setNewszan(String newszan) {
        this.newszan = newszan;
    }


    @Override
    public String toString() {
        return "NewsBean{" +
                "newsId='" + newsId + '\'' +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsUrl1='" + newsUrl1 + '\'' +
                ", newsUrl2='" + newsUrl2 + '\'' +
                ", newsUrl3='" + newsUrl3 + '\'' +
                ", newsMainjj='" + newsMainjj + '\'' +
                ", SunCom='" + SunCom + '\'' +
                ", newsHost='" + newsHost + '\'' +
                ", addTime='" + addTime + '\'' +
                ", newsWriter='" + newsWriter + '\'' +
                ", newszan='" + newszan + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
