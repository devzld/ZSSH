package com.zy.zssh.m_gambit.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/29 0029.
 */
public class GambitListBean implements Serializable {

    /**
     * huaId : 1
     * title : hahaceshihuati
     * huaUrl : http://app.shzywh.cn/app/images/news1.jpg
     * suncom : 1
     * writer : 小张
     * addTime : 2016-6-1 8:24:26
     * userId : 10086
     * sunzan : 4
     * zansate : 1
     * main : haha正文内容
     * headurL : http://app.shzywh.cn/app/head/10101.png
     */

    private String huaId;
    private String title;
    private String huaUrl;
    private String suncom;
    private String writer;
    private String addTime;
    private String userId;
    private String sunzan;
    private String zansate;
    private String main;
    private String headurL;

    private String sate;

    public String getHuaId() {
        return huaId;
    }

    public void setHuaId(String huaId) {
        this.huaId = huaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHuaUrl() {
        return huaUrl;
    }

    public void setHuaUrl(String huaUrl) {
        this.huaUrl = huaUrl;
    }

    public String getSuncom() {
        return suncom;
    }

    public void setSuncom(String suncom) {
        this.suncom = suncom;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSunzan() {
        return sunzan;
    }

    public void setSunzan(String sunzan) {
        this.sunzan = sunzan;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getHeadurL() {
        return headurL;
    }

    public void setHeadurL(String headurL) {
        this.headurL = headurL;
    }

    public String getSate() {
        return sate;
    }

    public void setSate(String sate) {
        this.sate = sate;
    }
}
