package com.zy.zssh.m_gambit.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_gambit.adapter.MyMessageGambitAdapter;
import com.zy.zssh.m_gambit.bean.MyMessageGambitBean;
import com.zy.zssh.m_gambit.business.GambitBusiness;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/30 0030.
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_my_release_gambit)
public class MyMessageGambitFragment extends BaseFragment {

    Map<String, String> map;

    @ViewInject(R.id.root_view)
    private View root_view;

    @ViewInject(R.id.fg_my_release_gambit_swipeRefresh)
    private SwipyRefreshLayout fg_my_release_gambit_swipeRefresh;

    @ViewInject(R.id.fg_my_release_gambit_recycleView)
    private RecyclerView fg_my_release_gambit_recycleView;

    @ViewInject(R.id.my_release_gambit_item_com_layout)
    private View my_release_gambit_item_com_layout;

    @ViewInject(R.id.my_release_gambit_com_edit)
    private EditText my_release_gambit_com_edit;

    private MyMessageGambitAdapter myMessageGambitAdapter;

    private GambitBusiness business;

    private List<MyMessageGambitBean> myMessageGambitBeanList = new ArrayList<>();

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    private int position;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    public MyMessageGambitFragment(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();

        setListener();
    }

    private void init() {
        business = GambitBusiness.getInstance();

        if (myMessageGambitBeanList.size() == 0) {

            fg_my_release_gambit_swipeRefresh.setRefreshing(true);
            initData();
        }

        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);

        fg_my_release_gambit_recycleView.setHasFixedSize(true);
        fg_my_release_gambit_recycleView.setItemAnimator(new DefaultItemAnimator());

        fg_my_release_gambit_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        myMessageGambitAdapter = new MyMessageGambitAdapter(getActivity(), myMessageGambitBeanList);

        fg_my_release_gambit_recycleView.setAdapter(myMessageGambitAdapter);

        fg_my_release_gambit_swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.refresh_color));
    }

    private void setListener() {
        fg_my_release_gambit_swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    initData();
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }
            }
        });

        fg_my_release_gambit_recycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == myMessageGambitAdapter.getItemCount()
                        && !isLoading && fg_my_release_gambit_swipeRefresh.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    fg_my_release_gambit_swipeRefresh.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        myMessageGambitAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {

                if ("0".equals(myMessageGambitBeanList.get(position).getType())) {

                    my_release_gambit_item_com_layout.setVisibility(View.VISIBLE);

                    my_release_gambit_com_edit.requestFocus();
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                    MyMessageGambitFragment.this.position = position;

                } else {
                    my_release_gambit_item_com_layout.setVisibility(View.GONE);
                }
            }
        });

        my_release_gambit_com_edit.setOnKeyListener(backlistener);
        root_view.setOnKeyListener(backlistener);
        fg_my_release_gambit_recycleView.setOnKeyListener(backlistener);

    }

    /**
     * 发送评论
     *
     * @param view
     */
    @Event(R.id.my_release_gambit_item_com_send)
    private void sendCom(View view) {

        if (!"".equals(my_release_gambit_com_edit.getText().toString())) {

            business.sendUserMessage(myMessageGambitBeanList.get(position).getYuan(), myMessageGambitBeanList.get(position).getFeiId(),
                    my_release_gambit_com_edit.getText().toString(), myMessageGambitBeanList.get(position).getComId(), getActivity(), new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {
                                Tools.hideKeyboard(getActivity());
                                my_release_gambit_item_com_layout.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "发送成功", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "回复失败，请稍后重试！", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                            Toast.makeText(getActivity(), "回复失败，请稍后重试！", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {

            Toast.makeText(getActivity(), "回复内容不能为空", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 初始化list数据
     */
    private void initData() {

        business.getGambitMessageData("1", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<MyMessageGambitBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "videocom"), MyMessageGambitBean.class);

                    myMessageGambitBeanList.clear();

                    if (aa != null && aa.size() != 0) {

                        myMessageGambitBeanList.addAll(aa);
                    }
                    myMessageGambitAdapter.notifyDataSetChanged();

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }
        });

    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getGambitMessageData(pageNo + "", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<MyMessageGambitBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "videocom"), MyMessageGambitBean.class);

                    if (aa != null && aa.size() != 0) {

                        myMessageGambitBeanList.addAll(aa);

                        myMessageGambitAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }
        });
    }

    private View.OnKeyListener backlistener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (i == KeyEvent.KEYCODE_BACK) {  //表示按返回键 时的操作

                    if (my_release_gambit_item_com_layout.getVisibility() == View.VISIBLE) {
                        my_release_gambit_item_com_layout.setVisibility(View.GONE);

                        return true;
                    }
                }
            }
            return false;
        }
    };

}
