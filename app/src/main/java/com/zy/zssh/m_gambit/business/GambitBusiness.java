package com.zy.zssh.m_gambit.business;

import android.content.Context;

import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hanj .
 */
public class GambitBusiness {
    private static GambitBusiness instance = new GambitBusiness();

    private GambitBusiness() {

    }

    public static synchronized GambitBusiness getInstance() {

        return instance;
    }

    /**
     * 获取话题列表数据
     *
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGambitListData(String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "hualist" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 设置赞数据
     *
     * @param type     类型，新闻视频等
     * @param id       id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void setzanData(String type, String id, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "zan" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("id", id);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取话题评论数据
     *
     * @param huaId    话题id
     * @param page     page
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGambitComData(String huaId, String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "huacom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("huaId", huaId);
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 添加评论
     *
     * @param huaId    话题id
     * @param userId   发表评论用户id
     * @param comMain  评论内容
     * @param comId2   回复评论的评论id，一级评论为0
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void sendGambitCommentData(String huaId, String userId, String comMain, String comId2,
                                      Context context, VolleyCallback callback) {
        //

        String url = VolleyClient.SERVER_IP_ADDRESS + "addhuacom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("huaId", huaId);
        params.put("userId", userId);
        params.put("comMain", comMain);
        params.put("comId2", comId2);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取我的话题列表数据
     *
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getMyGambitListData(String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "myhualist" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 发布话题
     *
     * @param title
     * @param mian
     * @param huaurl
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void sendGambitData(String title, String mian, String huaurl, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "addhua" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        params.put("mian", mian);
        params.put("huaurl", huaurl);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取话题中的消息
     *
     * @param page     页面
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getGambitMessageData(String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "huadong1" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("userId2", /*Config.Config(context).getUserId()*/"10086");

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 回复用户动态
     *
     * @param yuan     来源
     * @param leiId    原来动态的编号
     * @param mian     评论内容
     * @param comId    原评论编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void sendUserMessage(String yuan, String leiId, String mian, String comId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "hfdong" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("yuan", yuan);
        params.put("leiId", leiId);
        params.put("mian", mian);
        params.put("comId", comId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

}


