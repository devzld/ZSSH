package com.zy.zssh.m_gambit.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_gambit.adapter.GambitCommentAdapter;
import com.zy.zssh.m_gambit.bean.GambitListBean;
import com.zy.zssh.m_gambit.business.GambitBusiness;
import com.zy.zssh.m_me.activity.LoginActivity;
import com.zy.zssh.m_news.adapter.news.NewsCommentBaseRecycleAdapter;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
/**
 * desc:话题详情
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/8/2 0002 16:45
**/
public class GambitDetailActivity extends BaseActivity {

    private GambitListBean gambitListBean;

    @ViewInject(R.id.gambit_detail_swipeRefreshLayout)
    private SwipyRefreshLayout gambit_detail_swipeRefreshLayout;

    @ViewInject(R.id.gambit_detail_recycleview)
    private RecyclerView gambit_detail_recycleview;

    @ViewInject(R.id.news_detail_comment_edit)
    private EditText news_detail_comment_edit;

    @ViewInject(R.id.news_detail_comment_send)
    private ImageView news_detail_comment_send;

    private GambitCommentAdapter gambitCommentAdapter;

    private GambitBusiness business;

    private int comPage = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    /**
     * 新闻评论数据
     */
    private List<NewsCommentBean> newsCommentBeanList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_gambit_detail);

        x.view().inject(this);

        initData();

        initView();
    }

    @Override
    protected void initData() {

        Bundle bundle = getIntent().getExtras();

        gambitListBean = (GambitListBean) bundle.getSerializable("gambitBean");

        business = GambitBusiness.getInstance();

        gambit_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        gambit_detail_recycleview.setHasFixedSize(true);
        gambit_detail_recycleview.setItemAnimator(new DefaultItemAnimator());

        gambit_detail_recycleview.setLayoutManager(new LinearLayoutManager(this));

        gambit_detail_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        gambitCommentAdapter = new GambitCommentAdapter(this, newsCommentBeanList, gambitListBean);

        gambit_detail_recycleview.setAdapter(gambitCommentAdapter);

        getNewsCommentData();

        setListener();
    }

    /**
     * 设置监听
     */
    private void setListener() {
        gambit_detail_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    getNewsCommentData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreCommentData();
                }
            }
        });

        gambit_detail_recycleview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                /**
                 * 当前停止滑动
                 * 在最后一页
                 * 当前没有在加载
                 * 当前开启上拉，下拉刷新操作
                 */
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == gambitCommentAdapter.getItemCount()
                        && !isLoading && gambit_detail_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    gambit_detail_swipeRefreshLayout.setRefreshing(true);
                    loadMoreCommentData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        gambitCommentAdapter.setOnItemClickListener(new NewsCommentBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {

                switch (view.getId()) {

                    case R.id.cv_item:

                        news_detail_comment_edit.setText("@" + newsCommentBeanList.get(position).getLoginName() + ":");
                        news_detail_comment_edit.setTag(newsCommentBeanList.get(position).getComId());
                        news_detail_comment_edit.requestFocus();

                        news_detail_comment_edit.setSelection(news_detail_comment_edit.length());

                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                        break;

                    case R.id.news_comment_like:

                        view.setSelected(!view.isSelected());

                        business.setzanData(view.isSelected() ? "huacom" : "clearhuacom", newsCommentBeanList.get(position).getComId(),
                                GambitDetailActivity.this, new VolleyCallback() {
                                    @Override
                                    public void requestSuccess(VolleyResponse response) {
                                        super.requestSuccess(response);

                                        if (isSuccess(response)) {

                                            if (view.isSelected()) {
                                                Toast.makeText(GambitDetailActivity.this, "点赞", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(GambitDetailActivity.this, "取消赞", Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    }

                                    @Override
                                    public void requestError(VolleyError error) {
                                        super.requestError(error);
                                    }
                                });

                        break;
                }

            }
        });

        news_detail_comment_send.setOnClickListener(this);

    }

    @Override
    protected void initView() {

        news_detail_comment_edit.setTag("0");
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {
            case R.id.news_detail_comment_send:

                if (Config.Config(GambitDetailActivity.this).getUserId().equals("")) {

                    Toast.makeText(GambitDetailActivity.this, "登录后才能评论", Toast.LENGTH_SHORT).show();
                    openActivity(LoginActivity.class);
                } else {

                    sendComment();
                }

                break;
        }


    }

    /**
     * 添加评论
     */
    private void sendComment() {

        //newsId=新闻编号&userId=评论者用户编号&comMain=评论内容&comId2=被评论的评论ID
        business.sendGambitCommentData(gambitListBean.getHuaId(), Config.Config(this).getUserId(),
                news_detail_comment_edit.getText().toString(), news_detail_comment_edit.getTag().toString(), this,
                new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);

                        if (isSuccess(response)) {
                            Toast.makeText(GambitDetailActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            news_detail_comment_edit.setText("");
                            Tools.hideKeyboard(GambitDetailActivity.this);
                        } else {
                            Toast.makeText(GambitDetailActivity.this, "发布失败", Toast.LENGTH_SHORT).show();
                            Tools.hideKeyboard(GambitDetailActivity.this);
                        }

                        news_detail_comment_edit.setTag("0");
                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                        news_detail_comment_edit.setTag("0");
                    }
                });
    }


    /**
     * 获取新闻评论数据
     */
    private void getNewsCommentData() {

        comPage = 1;

        business.getGambitComData(gambitListBean.getHuaId(), comPage + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newscom"), NewsCommentBean.class);

                    if (aa != null) {

                        if (newsCommentBeanList.size() != 0) {

                            newsCommentBeanList.clear();
                        }

                        newsCommentBeanList.addAll(aa);

                        gambitCommentAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 10) {
                        gambit_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        gambit_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        comPage++;
                    }

                } else {
                    Toast.makeText(GambitDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }

                gambit_detail_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                gambit_detail_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 加载更多新闻评论数据
     */
    private void loadMoreCommentData() {

        business.getGambitComData(gambitListBean.getHuaId(), comPage + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newscom"), NewsCommentBean.class);

                    if (aa != null) {

                        newsCommentBeanList.addAll(aa);

                        gambitCommentAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 10) {
                        gambit_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        comPage++;
                        gambit_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }

                } else {
                    Toast.makeText(GambitDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }

                isLoading = false;
                gambit_detail_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

                isLoading = false;
                gambit_detail_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
