package com.zy.zssh.m_gambit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_gambit.bean.GambitListBean;
import com.zy.zssh.m_news.bean.news.NewsDetailBean;

/**
 * Created by devin on 16/4/27.
 */
public abstract class GambitCommentBaseRecycleAdapter extends BaseRecycleAdapter {

    protected enum ITEM_TYPE {
        HEAD, CONTENT
    }

    protected GambitListBean gambitListBean;

    public GambitCommentBaseRecycleAdapter(Context mContext, GambitListBean gambitListBean) {
        super(mContext);
        this.gambitListBean = gambitListBean;
    }

    public void setNewsDetailBean(GambitListBean gambitListBean) {
        this.gambitListBean = gambitListBean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE.HEAD.ordinal() && gambitListBean != null) {

            return onCreateHeadViewHolder(parent);
        } else if (viewType == ITEM_TYPE.CONTENT.ordinal()) {

            return onCreateContentViewHolder(parent);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 && gambitListBean != null) {

            return ITEM_TYPE.HEAD.ordinal();
        } else {

            return ITEM_TYPE.CONTENT.ordinal();
        }
    }

    public abstract RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent);
}
