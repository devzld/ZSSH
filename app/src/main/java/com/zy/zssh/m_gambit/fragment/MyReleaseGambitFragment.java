package com.zy.zssh.m_gambit.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_gambit.activity.GambitDetailActivity;
import com.zy.zssh.m_gambit.adapter.MyGambitListAdapter;
import com.zy.zssh.m_gambit.bean.GambitListBean;
import com.zy.zssh.m_gambit.business.GambitBusiness;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/30 0030.
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_my_release_gambit)
public class MyReleaseGambitFragment extends BaseFragment {

    Map<String, String> map;

    @ViewInject(R.id.fg_my_release_gambit_swipeRefresh)
    private SwipyRefreshLayout fg_my_release_gambit_swipeRefresh;

    @ViewInject(R.id.fg_my_release_gambit_recycleView)
    private RecyclerView fg_my_release_gambit_recycleView;

    private MyGambitListAdapter gambitListAdapter;

    private GambitBusiness business;

    private List<GambitListBean> gambitListBeanList = new ArrayList<>();

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    public MyReleaseGambitFragment(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();

        setListener();
    }

    private void init() {
        business = GambitBusiness.getInstance();

        if (gambitListBeanList.size() == 0) {

            fg_my_release_gambit_swipeRefresh.setRefreshing(true);
            initData();
        }

        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);

        fg_my_release_gambit_recycleView.setHasFixedSize(true);
        fg_my_release_gambit_recycleView.setItemAnimator(new DefaultItemAnimator());

        fg_my_release_gambit_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        gambitListAdapter = new MyGambitListAdapter(getActivity(), gambitListBeanList);

        fg_my_release_gambit_recycleView.setAdapter(gambitListAdapter);

        fg_my_release_gambit_swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.refresh_color));
    }

    private void setListener() {
        fg_my_release_gambit_swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    initData();
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }
            }
        });

        fg_my_release_gambit_recycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == gambitListAdapter.getItemCount()
                        && !isLoading && fg_my_release_gambit_swipeRefresh.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    fg_my_release_gambit_swipeRefresh.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        gambitListAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {

                switch (view.getId()) {
                    case R.id.cv_item:

                        Bundle bundle = new Bundle();
                        bundle.putSerializable("gambitBean", gambitListBeanList.get(position));
                        openActivity(GambitDetailActivity.class, bundle);

                        break;

                    case R.id.gambit_item_like_ico:

                        view.setSelected(!view.isSelected());

                        business.setzanData(view.isSelected() ? "hua" : "clearhua", gambitListBeanList.get(position).getHuaId(), getActivity(),
                                new VolleyCallback() {
                                    @Override
                                    public void requestSuccess(VolleyResponse response) {
                                        super.requestSuccess(response);

                                        if (isSuccess(response)) {
                                            gambitListBeanList.get(position).setZansate("1");
                                            gambitListAdapter.notifyDataSetChanged();
                                            Toast.makeText(getActivity(), "点赞", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getActivity(), "赞被劫持走了", Toast.LENGTH_SHORT).show();
                                            view.setSelected(!view.isSelected());
                                        }
                                    }

                                    @Override
                                    public void requestError(VolleyError error) {
                                        super.requestError(error);

                                        Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_SHORT).show();
                                        view.setSelected(!view.isSelected());
                                    }
                                });

                        break;
                }

            }
        });
    }

    /**
     * 初始化list数据
     */
    private void initData() {

        business.getMyGambitListData("1", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<GambitListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "hualist"), GambitListBean.class);

                    gambitListBeanList.clear();

                    if (aa != null && aa.size() != 0) {

                        gambitListBeanList.addAll(aa);
                    }
                    gambitListAdapter.notifyDataSetChanged();

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }
        });

    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getMyGambitListData(pageNo + "", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<GambitListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "hualist"), GambitListBean.class);

                    if (aa != null && aa.size() != 0) {

                        gambitListBeanList.addAll(aa);

                        gambitListAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        fg_my_release_gambit_swipeRefresh.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                fg_my_release_gambit_swipeRefresh.setRefreshing(false);
            }
        });
    }
}
