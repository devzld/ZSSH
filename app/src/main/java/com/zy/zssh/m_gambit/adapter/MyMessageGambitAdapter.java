package com.zy.zssh.m_gambit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_gambit.bean.MyMessageGambitBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/30 0030.
 */
public class MyMessageGambitAdapter extends BaseRecycleAdapter {

    private List<MyMessageGambitBean> myMessageGambitBeanList = new ArrayList<>();

    public MyMessageGambitAdapter(Context mContext, List<MyMessageGambitBean> myMessageGambitBeanList) {
        super(mContext);
        this.myMessageGambitBeanList = myMessageGambitBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyMessageGambitViewHolder(mLayoutInflater.inflate(R.layout.my_message_gambit_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ImageLoader.getInstance().displayImage(getItem(position).getUrl(),
                ((MyMessageGambitViewHolder) holder).my_message_gambit_item_ico, MyApplication.imageOptionCircle);

        ((MyMessageGambitViewHolder) holder).my_message_gambit_item_name.setText(getItem(position).getLoginName());

        ((MyMessageGambitViewHolder) holder).my_message_gambit_item_time.setText(getItem(position).getAddtime());

        if (getItem(position).getType().equals("1")) {
            //点赞
            ((MyMessageGambitViewHolder) holder).my_message_gambit_item_content1.setText("攒了" + (getItem(position).getComId().equals("0") ? "该话题" : "该回复"));
        } else {
            ((MyMessageGambitViewHolder) holder).my_message_gambit_item_content1.setText("回复了：" + getItem(position).getMian());
        }

        if (getItem(position).getComId().equals("0")) {
            ((MyMessageGambitViewHolder) holder).my_message_gambit_item_content2.setText(getItem(position).getTitle());
        } else {
            ((MyMessageGambitViewHolder) holder).my_message_gambit_item_content2.setText(getItem(position).getMian2());
        }

    }

    private MyMessageGambitBean getItem(int position) {
        return myMessageGambitBeanList.get(position);
    }

    @Override
    public int getItemCount() {
        return myMessageGambitBeanList.size();
    }

    public class MyMessageGambitViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.cv_item)
        private View cv_item;

        @ViewInject(R.id.my_message_gambit_item_ico)
        private ImageView my_message_gambit_item_ico;

        @ViewInject(R.id.my_message_gambit_item_name)
        private TextView my_message_gambit_item_name;

        @ViewInject(R.id.my_message_gambit_item_time)
        private TextView my_message_gambit_item_time;

        @ViewInject(R.id.my_message_gambit_item_content1)
        private TextView my_message_gambit_item_content1;

        @ViewInject(R.id.my_message_gambit_item_content2)
        private TextView my_message_gambit_item_content2;


        public MyMessageGambitViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getPosition());
            }

        }
    }

}
