package com.zy.zssh.m_gambit.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.m_gambit.adapter.PersonGambitPageAdapter;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonGambitActivity extends BaseActivity {


    @ViewInject(R.id.person_gambit_tablayout)
    private SlidingTabLayout person_gambit_tablayout;

    @ViewInject(R.id.person_gambit_view_pager)
    private ViewPager person_gambit_view_pager;

    private PersonGambitPageAdapter personGambitPageAdapter;

    private List<Map<String, String>> personList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_person_gambit);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setHidePublicTitle(true);
    }

    @Override
    protected void initData() {

        Map<String, String> map;
        map = new HashMap<>();
        map.put("id", "1");
        map.put("name", "我发布的");
        personList.add(map);

        map = new HashMap<>();
        map.put("id", "2");
        map.put("name", "消息");
        personList.add(map);

        personGambitPageAdapter = new PersonGambitPageAdapter(getSupportFragmentManager(), personList);

        person_gambit_view_pager.setAdapter(personGambitPageAdapter);

        person_gambit_tablayout.setViewPager(person_gambit_view_pager);
    }

    @Event(R.id.person_gambit_back)
    private void back(View view) {
        finish();
    }


    @Override
    protected void initClickListener(View v) {

    }
}
