package com.zy.zssh.m_gambit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.m_gambit.bean.GambitListBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.List;

/**
 * Created by Administrator on 2016/6/29 0029.
 */
public class MyGambitListAdapter extends BaseRecycleAdapter {

    private List<GambitListBean> gambitListBeanList;

    public MyGambitListAdapter(Context mContext, List<GambitListBean> gambitListBeanList) {
        super(mContext);
        this.gambitListBeanList = gambitListBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GambitListViewHolder(mLayoutInflater.inflate(R.layout.my_gambit_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(Config.Config(mContext).getUserUrl(),
                ((GambitListViewHolder) holder).gambit_item_ico, MyApplication.imageOptionCircle);

        ((GambitListViewHolder) holder).gambit_item_name.setText(getItem(position).getWriter());

        ((GambitListViewHolder) holder).gambit_item_time.setText(getItem(position).getAddTime());

        ((GambitListViewHolder) holder).gambit_item_title.setText(getItem(position).getTitle());
        ((GambitListViewHolder) holder).gambit_item_sate.setText(getItem(position).getSate().equals("0") ? "未审核" : getItem(position).getSate().equals("1") ? "审核通过" : "审核失败");


        if (getItem(position).getUserId().equals("")) {
            ((GambitListViewHolder) holder).gambit_item_image.setVisibility(View.GONE);
        } else {
            ((GambitListViewHolder) holder).gambit_item_image.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(getItem(position).getHuaUrl(),
                    ((GambitListViewHolder) holder).gambit_item_image, MyApplication.imageOptionNormal);
        }

        ((GambitListViewHolder) holder).gambit_item_content.setText(getItem(position).getMain());

        ((GambitListViewHolder) holder).gambit_item_comNum.setText(getItem(position).getSuncom());

        ((GambitListViewHolder) holder).gambit_item_like_ico.setSelected(getItem(position).getZansate().equals("1"));

        ((GambitListViewHolder) holder).gambit_item_like_num.setText(getItem(position).getSunzan());

    }

    private GambitListBean getItem(int position) {
        return gambitListBeanList.get(position);
    }

    @Override
    public int getItemCount() {
        return gambitListBeanList.size();
    }

    public class GambitListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.cv_item)
        private View cv_item;

        @ViewInject(R.id.gambit_item_ico)
        private ImageView gambit_item_ico;

        @ViewInject(R.id.gambit_item_name)
        private TextView gambit_item_name;

        @ViewInject(R.id.gambit_item_time)
        private TextView gambit_item_time;

        @ViewInject(R.id.gambit_item_title)
        private TextView gambit_item_title;

        @ViewInject(R.id.gambit_item_image)
        private ImageView gambit_item_image;

        @ViewInject(R.id.gambit_item_content)
        private TextView gambit_item_content;

        @ViewInject(R.id.gambit_item_comNum)
        private TextView gambit_item_comNum;

        @ViewInject(R.id.gambit_item_like_ico)
        private ImageView gambit_item_like_ico;

        @ViewInject(R.id.gambit_item_like_num)
        private TextView gambit_item_like_num;

        @ViewInject(R.id.gambit_item_sate)
        private TextView gambit_item_sate;

        public GambitListViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            gambit_item_like_ico.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getPosition());
            }

        }
    }
}
