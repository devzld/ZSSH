package com.zy.zssh.m_gambit.bean;

/**
 * Created by Administrator on 2016/6/30 0030.
 */
public class MyMessageGambitBean {

    /**
     * userId : 10087
     * loginName : 顾剑锋
     * sate1 : 回复
     * mian : 哈哈
     * mian2 :
     * sate : 0
     * type : 1
     * yuan : 3
     * feiId : 1
     * comId : 0
     * addtime : 2016-6-22 15:54:51
     * url : http://app.shzywh.cn/app/images/head.png
     * title : hahaceshihuati
     */

    private String userId;
    private String loginName;
    private String sate1;
    private String mian;
    private String mian2;
    private String sate;
    private String type;
    private String yuan;
    private String feiId;
    private String comId;
    private String addtime;
    private String url;
    private String title;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getSate1() {
        return sate1;
    }

    public void setSate1(String sate1) {
        this.sate1 = sate1;
    }

    public String getMian() {
        return mian;
    }

    public void setMian(String mian) {
        this.mian = mian;
    }

    public String getMian2() {
        return mian2;
    }

    public void setMian2(String mian2) {
        this.mian2 = mian2;
    }

    public String getSate() {
        return sate;
    }

    public void setSate(String sate) {
        this.sate = sate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYuan() {
        return yuan;
    }

    public void setYuan(String yuan) {
        this.yuan = yuan;
    }

    public String getFeiId() {
        return feiId;
    }

    public void setFeiId(String feiId) {
        this.feiId = feiId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
