package com.zy.zssh.m_gambit.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.CommonTool;
import com.zy.zssh.common.utils.PictureUtil;
import com.zy.zssh.common.view.SelectPicPopupWindow;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_gambit.business.GambitBusiness;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * desc:发布话题
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/8/3 0003 9:08
**/
public class ReleaseGambitActivity extends BaseActivity {

    @ViewInject(R.id.release_gambit_title)
    private EditText release_gambit_title;

    @ViewInject(R.id.release_gambit_content)
    private EditText release_gambit_content;

    @ViewInject(R.id.release_gambit_add_img)
    private ImageView release_gambit_add_img;

    private GambitBusiness business;

    private String imgBase64 = "";

    // 头像上传
    public static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 裁剪

    private SelectPicPopupWindow menuWindow;

    private File tempFile;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_release_gambit);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("发布话题");

        setRightImageView(R.mipmap.fenxngx);

        menuWindow = new SelectPicPopupWindow(this, this);
        menuWindow.setOnDismissListener(new PersonInformaton());

    }

    @Override
    protected void initData() {

        business = GambitBusiness.getInstance();
    }

    /**
     * 发送监听
     */
    @Override
    public void rightImgClick() {
        super.rightImgClick();

        if (release_gambit_title.getText().toString().equals("")) {
            Toast.makeText(ReleaseGambitActivity.this, "标题不能为空", Toast.LENGTH_SHORT).show();
        } else if (release_gambit_content.getText().toString().equals("")) {
            Toast.makeText(ReleaseGambitActivity.this, "内容不能为空", Toast.LENGTH_SHORT).show();
        } else {

            business.sendGambitData(release_gambit_title.getText().toString(), release_gambit_content.getText().toString(), imgBase64, this,
                    new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {
                                Toast.makeText(ReleaseGambitActivity.this, "发布成功", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(ReleaseGambitActivity.this, "发布失败", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                            Toast.makeText(ReleaseGambitActivity.this, "发布失败", Toast.LENGTH_SHORT).show();
                        }
                    });

        }

    }

    @Event(R.id.release_gambit_add_img)
    private void addImg(View view) {

        menuWindow.setBackgroundDrawable(new BitmapDrawable());
        menuWindow.setText("拍照", "从相册中选取");
        menuWindow.update();
        menuWindow.showAtLocation(this.findViewById(R.id.root_view),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        backgroundAlpha(0.5f);

    }

    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; // 0.0-1.0
        getWindow().setAttributes(lp);
    }


    class PersonInformaton implements PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            backgroundAlpha(1f);
        }
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {
            case R.id.rv_photo_paizhao:
                camera();
                if (menuWindow != null) {
                    menuWindow.dismiss();
                }
                break;
            case R.id.rv_photo_xcxz:
                gallery();
                if (menuWindow != null) {
                    menuWindow.dismiss();
                }
                break;
        }
    }


    /**
     * 从相机获取
     */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }


    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * 从相册获取
	 */
    public void gallery() {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }

    /**
     * 剪切图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */

    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    public void saveFile(Bitmap bm, String fileName) throws IOException {
        File dirFile = new File(CommonTool.PROJECT_IMAGE_PATH);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        File myCaptureFile = new File(CommonTool.PROJECT_IMAGE_PATH + fileName);

        if (myCaptureFile.exists()) {
            myCaptureFile.delete();
        }

        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
        bos.flush();
        bos.close();
    }


    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PHOTO_REQUEST_GALLERY) {
            if (data != null) {
                // 得到图片的全路径
                Uri uri = data.getData();
                crop(uri);
            }

        } else if (requestCode == PHOTO_REQUEST_CAMERA && resultCode != 0) {
            if (hasSdcard()) {
                tempFile = new File(Environment.getExternalStorageDirectory(),
                        PHOTO_FILE_NAME);
                crop(Uri.fromFile(tempFile));
            } else {
                toastMsg("未找到存储卡，无法存储照片！");
            }

        } else if (requestCode == PHOTO_REQUEST_CUT && resultCode != 0) {
            try {
                WindowManager.LayoutParams lp = this.getWindow()
                        .getAttributes();
                lp.alpha = 1f;
                this.getWindow().setAttributes(lp);

                bitmap = data.getParcelableExtra("data");
                saveFile(bitmap, "user_ico.jpg");

                ImageLoader.getInstance().displayImage("file://" + CommonTool.PROJECT_IMAGE_PATH + "user_ico.jpg", release_gambit_add_img, MyApplication.imageOptionNormalLocal);

                imgBase64 = PictureUtil.bitmapToString(PictureUtil.getBitmap(CommonTool.PROJECT_IMAGE_PATH + "user_ico.jpg"));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
