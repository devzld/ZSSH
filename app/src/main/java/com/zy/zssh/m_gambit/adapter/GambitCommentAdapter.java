package com.zy.zssh.m_gambit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.m_gambit.bean.GambitListBean;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/3 0003.
 * 新闻评论列表的adapter
 */
public class GambitCommentAdapter extends GambitCommentBaseRecycleAdapter {

    private List<NewsCommentBean> newsCommentBeanList;

    public GambitCommentAdapter(Context mContext, List<NewsCommentBean> newsCommentBeanList, GambitListBean gambitListBean) {
        super(mContext, gambitListBean);

        this.newsCommentBeanList = newsCommentBeanList;

        if (newsCommentBeanList == null) {
            this.newsCommentBeanList = new ArrayList<>();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //头部事件的处理
        if (holder instanceof GambitListViewHolder && gambitListBean != null) {

            ImageLoader.getInstance().displayImage(gambitListBean.getHeadurL(),
                    ((GambitListViewHolder) holder).gambit_item_ico, MyApplication.imageOptionCircle);

            ((GambitListViewHolder) holder).gambit_item_name.setText(gambitListBean.getWriter());

            ((GambitListViewHolder) holder).gambit_item_time.setText(gambitListBean.getAddTime());

            ((GambitListViewHolder) holder).gambit_item_title.setText(gambitListBean.getTitle());

            if (gambitListBean.getHuaUrl().equals("")) {
                ((GambitListViewHolder) holder).gambit_item_image.setVisibility(View.GONE);
            } else {
                ((GambitListViewHolder) holder).gambit_item_image.setVisibility(View.VISIBLE);
                ImageLoader.getInstance().displayImage(gambitListBean.getHuaUrl(),
                        ((GambitListViewHolder) holder).gambit_item_image, MyApplication.imageOptionNormal);
            }

            ((GambitListViewHolder) holder).gambit_item_content.setText(gambitListBean.getMain());

        } else if (holder instanceof NewsCommentViewHolder) {

            ImageLoader.getInstance().displayImage(getItem(position).getUserUrl(),
                    ((NewsCommentViewHolder) holder).news_comment_user_ico, MyApplication.imageOptionCircle);

            ((NewsCommentViewHolder) holder).news_comment_user_name.setText(getItem(position).getLoginName());

            ((NewsCommentViewHolder) holder).news_comment_user_time.setText(getItem(position).getAddTime());

            ((NewsCommentViewHolder) holder).news_comment_content.setText(getItem(position).getComMain());
            ((NewsCommentViewHolder) holder).news_comment_like.setSelected(getItem(position).getZansate().equals("1"));

            if ("0".equals(getItem(position).getUserID2())) {
                ((NewsCommentViewHolder) holder).news_comment_reply.setVisibility(View.GONE);
            } else {

                try {
                    ((NewsCommentViewHolder) holder).news_comment_reply.setVisibility(View.VISIBLE);

                    ImageLoader.getInstance().displayImage(getItem(position).getUserUrL2(),
                            ((NewsCommentViewHolder) holder).news_comment_user_ico1, MyApplication.imageOptionCircle);

                    ((NewsCommentViewHolder) holder).news_comment_user_name1.setText(getItem(position).getLoginName2());

                    ((NewsCommentViewHolder) holder).news_comment_user_time1.setText(getItem(position).getAddTime());

                    ((NewsCommentViewHolder) holder).news_comment_content1.setText(getItem(position).getComMain2());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public NewsCommentBean getItem(int position) {
        return newsCommentBeanList.get(gambitListBean != null ? position - 1 : position);
    }

    @Override
    public int getItemCount() {
        return gambitListBean != null ? newsCommentBeanList.size() + 1 : newsCommentBeanList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent) {
        return new GambitListViewHolder(mLayoutInflater.inflate(R.layout.gambit_detail_item_head, parent, false));
    }

    @Override
    public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
        return new NewsCommentViewHolder(mLayoutInflater.inflate(R.layout.fg_news_comment_item, parent, false));
    }

    public class NewsCommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.news_comment_user_ico)
        private ImageView news_comment_user_ico;

        @ViewInject(R.id.news_comment_user_name)
        private TextView news_comment_user_name;

        @ViewInject(R.id.news_comment_user_time)
        private TextView news_comment_user_time;

        @ViewInject(R.id.news_comment_content)
        private TextView news_comment_content;

        @ViewInject(R.id.news_comment_reply)
        private View news_comment_reply;

        @ViewInject(R.id.news_comment_user_ico1)
        private ImageView news_comment_user_ico1;

        @ViewInject(R.id.news_comment_user_name1)
        private TextView news_comment_user_name1;

        @ViewInject(R.id.news_comment_user_time1)
        private TextView news_comment_user_time1;

        @ViewInject(R.id.news_comment_content1)
        private TextView news_comment_content1;

        @ViewInject(R.id.news_comment_like)
        private ImageView news_comment_like;

        @ViewInject(R.id.cv_item)
        private LinearLayout cv_item;

        public NewsCommentViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            news_comment_like.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, gambitListBean != null ? getPosition() - 1 : getPosition());
            }
        }
    }

    public class GambitListViewHolder extends RecyclerView.ViewHolder {

        @ViewInject(R.id.gambit_item_ico)
        private ImageView gambit_item_ico;

        @ViewInject(R.id.gambit_item_name)
        private TextView gambit_item_name;

        @ViewInject(R.id.gambit_item_time)
        private TextView gambit_item_time;

        @ViewInject(R.id.gambit_item_title)
        private TextView gambit_item_title;

        @ViewInject(R.id.gambit_item_image)
        private ImageView gambit_item_image;

        @ViewInject(R.id.gambit_item_content)
        private TextView gambit_item_content;

        public GambitListViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

        }
    }
}
