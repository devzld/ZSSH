package com.zy.zssh.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by devin on 16/4/27.
 */
public abstract class BaseRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected LayoutInflater mLayoutInflater;
    protected Context mContext;

    protected MyItemClickListener itemClickListener;
    protected MyItemLongClickListener itemLongClickListener;

    public BaseRecycleAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(MyItemClickListener itemClickListener) {

        this.itemClickListener = itemClickListener;
    }

    public interface MyItemLongClickListener {
        boolean onItemLongClick(View view, int position);
    }

    public void setOnItemLongClickListener(MyItemLongClickListener itemLongClickListener) {

        this.itemLongClickListener = itemLongClickListener;
    }

}
