package com.zy.zssh.common.volleyutil;

public class VolleyRequest {
//    /**
//     * 服务名称
//     */
//    private String serviceName;
//    /**
//     * 令牌
//     */
//    private String token;
//    /**
//     * 消息体
//     */
//    private Object bodyObject;

//    public VolleyRequest() {
//    }
//
//    public VolleyRequest(String serviceName, String token, String userId,
//                         Object bodyObject) {
//        this.serviceName = serviceName;
//        this.token = token;
//        this.bodyObject = bodyObject;
//    }

    private String strs[];
    private String method = "";

    /**
     * 参数传入成对的键值,最后一位为方法地址方法名
     *
     * @param strs
     */
    public VolleyRequest(String[] strs, String method) {
        this.strs = strs;
        this.method = method;
    }

    public String getUri() {

        StringBuilder stringBuilder = new StringBuilder(VolleyClient.SERVER_IP_ADDRESS);
        stringBuilder.append(method);
        stringBuilder.append(VolleyClient.SERVER_IP_FOOT);

        for (int i = 0; i < strs.length; i++) {

            if (i % 2 == 0) {

                if (i == 0) {
                    stringBuilder.append("?" + strs[i]);
                } else {
                    stringBuilder.append("&" + strs[i]);
                }

            } else {
                stringBuilder.append("=" + strs[i]);
            }
        }

        return stringBuilder.toString();
    }

//    /**
//     * 封装请求消息
//     * <p/>
//     * 返回类型:JSONObject
//     *
//     * @return
//     * @throws JSONException author:hanj
//     */
//    public JSONObject toJSONObject() throws JSONException {
//        Map<String, Object> requestMap = new HashMap<String, Object>();
//        requestMap.put("serviceName", this.getServiceName());
//        if (!"".equals(this.token)) {
//            requestMap.put("token", this.getToken());
//        }
//        JSONObject requestJSON = new JSONObject(requestMap);
//
//        requestJSON.put("body", this.getBodyObject());
//        return requestJSON;
//    }
//
//    public String getServiceName() {
//        return serviceName;
//    }
//
//    public void setServiceName(String serviceName) {
//        this.serviceName = serviceName;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    public Object getBodyObject() {
//        return bodyObject;
//    }
//
//    public void setBodyObject(Object bodyObject) {
//        this.bodyObject = bodyObject;
//    }

}
