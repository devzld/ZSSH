package com.zy.zssh.common.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import org.xutils.common.Callback;
import org.xutils.common.util.MD5;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 下午5:11
 * To change this template use File | Settings | File Templates.
 */
public class CommonTool {
    public static final String PROJECT_NAME = "ZSSH";
    public static final String SHJI_PATH = android.os.Environment.getDataDirectory().getAbsolutePath() + "/" + PROJECT_NAME;
    public static final String DATABASE_PATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + PROJECT_NAME;//路径
    public static final String DATA_FILE_PATH = existSDcard() ? DATABASE_PATH : SHJI_PATH;
    /**
     * ****存放图片的文件夹*****************
     */
    public static final String PROJECT_IMAGE_PATH = DATA_FILE_PATH + "/Image/";//路径

    /**
     * ***********************
     */
    //判断是否存在网络
    public static boolean isNotNetworkAvailable(Context context) {
        return !isNetworkAvailable(context);
    }

    //判断是否存在网络
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if ((info[i].getState() == NetworkInfo.State.CONNECTED) || (info[i].getState() == NetworkInfo.State.CONNECTING)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //判断SD卡是否存在
    public static boolean existSDcard() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * dp转像素
     *
     */

    public static int dp2Pix(Context context , int dp){


        return (int) (dp *context.getResources().getDisplayMetrics().density);


    }


    /**
     * 缓存网络图片
     *
     * @param context
     * @param url
     * @param callback
     */
    public static void cacheNetFile(Context context, String url, Callback.CommonCallback<File> callback){


        String cachePath = getCachePath(context);
        if(!TextUtils.isEmpty(cachePath)){

            //拼接路径
            String path = cachePath + "/" + MD5.md5(url) + ".jpg";

            File file = new File(path);
            if(file.exists()){


                callback.onSuccess(file);


            }else{


                RequestParams requestParams = new RequestParams(url);
                requestParams.setSaveFilePath(path);
                x.http().get(requestParams, callback);

            }
//               RequestParams requestParams = new RequestParams(url);
//                requestParams.setSaveFilePath(path);
//                x.http().get(requestParams, callback);



        }



    }


    public static String getCachePath(Context context){

        File file = context.getCacheDir();
        if(file.exists()){

            return file.getAbsolutePath();
        }else{

            return null;

        }

    }


    //








}

