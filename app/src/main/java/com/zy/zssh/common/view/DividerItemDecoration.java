package com.zy.zssh.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by mac on 16/7/19.
 * recyclerview的分割线
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration{


    private int mOrientation = LinearLayoutManager.VERTICAL;
    private Paint mPaint;
    private int mItemSize = 8;


    public DividerItemDecoration(Context context , int mOrientation,int colorRes){


        this.mOrientation = mOrientation;
        if(mOrientation!=LinearLayoutManager.VERTICAL && mOrientation!= LinearLayoutManager.HORIZONTAL){

            throw new IllegalArgumentException("布局参数错误");

        }



        this.mItemSize = (int) TypedValue.applyDimension(mItemSize,TypedValue.COMPLEX_UNIT_DIP,context.getResources().getDisplayMetrics());


        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(context.getResources().getColor(colorRes));
        mPaint.setStyle(Paint.Style.FILL);



    }


    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);

        if(mOrientation == LinearLayoutManager.VERTICAL){

            drawVertical(c,parent);


        }else{


            drawHorizontal(c,parent);



        }


    }

    private void drawHorizontal(Canvas c, RecyclerView parent) {


        int top = parent.getTop();
        int bottom = parent.getMeasuredHeight() - parent.getPaddingBottom();
        int childSize = parent.getChildCount();
        for(int i=0 ; i< childSize ; i++){


            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int left = child.getRight() + params.rightMargin;
            int right = left + mItemSize;

            c.drawRect(left,top,right,bottom,mPaint);


        }

    }

    /**
     * 画横向的分割线
     *
     * @param c
     * @param parent
     */
    private void drawVertical(Canvas c, RecyclerView parent) {

        int left = parent.getPaddingLeft();
        int right = parent.getMeasuredWidth() - parent.getPaddingRight();
        int childSize = parent.getChildCount();
        for(int i=0 ; i<childSize ; i++){


            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mItemSize;

            c.drawRect(left,top,right,bottom,mPaint);

        }


    }


}
