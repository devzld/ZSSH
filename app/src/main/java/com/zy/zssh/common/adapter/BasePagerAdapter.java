package com.zy.zssh.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * desc:
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/8 0008
 **/
public class BasePagerAdapter extends FragmentPagerAdapter{


    private List<Fragment> al;


    public BasePagerAdapter(FragmentManager fm,List<Fragment> list) {
        super(fm);
        al = list;
    }

    @Override
    public Fragment getItem(int position) {
        return al.get(position);
    }

    @Override
    public int getCount() {
        return al == null?0:al.size();
    }


    public void addData(List<Fragment> list){

        al.addAll(list);
        notifyDataSetChanged();

    }


    public void loadData(List<Fragment> list){

        al.clear();
        al.addAll(list);
        notifyDataSetChanged();


    }



}
