package com.zy.zssh.common.volleyutil;

import com.android.volley.VolleyError;

/**
 * 监听请求的接口
 * <p/>
 * 2015年10月16日
 * <p/>
 * VolleyRequestListener.java
 *
 * @author hanj
 */
public interface VolleyRequestListener {

    /**
     * 请求成功
     * 返回类型:void
     *
     * @param response author:hanj
     */
    public abstract void requestSuccess(VolleyResponse response);


    /**
     * 请求失败
     * 返回类型:void
     *
     * @param error author:hanj
     */
    public void requestError(VolleyError error);


}
