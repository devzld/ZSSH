package com.zy.zssh.common.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.zy.zssh.R;

import java.util.Timer;

/**
 * 加载动画
 */
public class LoadingDialog extends Dialog {

    protected static final String TAG = "LoadingDialog";
    private static LoadingDialog instance;
    // 前景图片
    private ImageView img_front;
    // 定时器，用来不断的播放动画
    private Timer animationTimer;

    private AnimationDrawable animationDrawable;

    private Context context;

    public LoadingDialog(Context context) {
        super(context, R.style.load_dialog);
        this.context = context;
    }

    public static LoadingDialog getInstance(Context context) {
        if (instance == null) {
            instance = new LoadingDialog(context);
        }
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dg_loading);

        img_front = (ImageView) findViewById(R.id.img_front);
        animationTimer = new Timer();

        animationDrawable = (AnimationDrawable) img_front.getBackground();
        animationDrawable.setOneShot(false);

        animationDrawable.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        animationTimer.cancel();
        animationDrawable.stop();
    }
}
