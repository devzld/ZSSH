package com.zy.zssh.common.volleyutil;

import android.content.Context;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.orhanobut.logger.Logger;
import com.zy.zssh.MyApplication;

import java.util.Map;

/**
 * 请求网络时的请求队列以及请求方式
 * <p/>
 * 2015年10月16日
 * <p/>
 * VolleyClient.java
 *
 * @author hanj
 */
public class VolleyClient {

    //服务器地址
    public final static String SERVER_IP_ADDRESS = "http://app.shzywh.cn/app/";

    //服务器地址结尾
    public final static String SERVER_IP_FOOT = ".aspx";

    /**
     * 全局的toast
     */
    private static Toast mToast;

    /**
     * 上下文
     */
    public static Context context;
    /**
     * 请求队列
     */
    public static RequestQueue mRequestQueue = null;
    /**
     * 单例
     */
    public static VolleyClient instance = null;

    /**
     * 用户防止登陆超时，跳转登陆界面，退出界面重复进入
     */
    public static boolean isfirst = true;

    /**
     * 构造方法，初始化请求队列
     *
     * @param context
     */
    private VolleyClient(Context context) {
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    /**
     * 单例 返回类型:VolleyClient
     *
     * @param context
     * @return author:hanj
     */
    public static VolleyClient getInstance(Context context) {
        if (instance == null) {
            synchronized (VolleyClient.class) {
                if (instance == null) {
                    instance = new VolleyClient(context);
                }
            }
        }
        return instance;
    }

    /**
     * post请求方式：返回类型:void
     *
     * @param url      请求地址
     * @param params   参数map
     * @param callback author:hanj
     */
    public static void requsetOfPost(String url, Map<String, String> params, VolleyCallback callback) {
        requsetOfPost(null, url, params, callback);
    }

    /**
     * post请求方式：没有tag时传入null 返回类型:void
     *
     * @param tag      标记
     * @param url      请求地址
     * @param params   参数map
     * @param callback author:hanj
     */
    @SuppressWarnings("unchecked")
    public static void requsetOfPost(Object tag, String url, final Map<String, String> params, VolleyCallback callback) {

        try {

            StringRequest request = new StringRequest(Request.Method.POST, url,
                    responseSuccessListener(callback),
                    responseErrorListener(callback)) {

                @Override
                protected Map<String, String> getParams() {
                    //在这里设置需要post的参数

                    return params;
                }
            };

            addRequest(request, tag);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.e(e.toString());
        }


//        StringRequest request = new StringRequest(Request.Method.POST, url,
//                    responseSuccessListener(callback),
//                    responseErrorListener(callback)) {
//
//                @Override
//                protected Map<String, String> getParams() {
//                    //在这里设置需要post的参数
//
//                    return params;
//                }
//            };
//
//            addRequest(request, tag);


    }

    /**
     * 加入队列 返回类型:void
     *
     * @param request
     * @param tag     author:hanj
     */
    public static void addRequest(StringRequest request, Object tag) {
        if (tag != null) {
            request.setTag(tag);
        }
        mRequestQueue.add(request);
    }


    /**
     * 取消全部队列请求 返回类型:void
     *
     * @param tag author:hanj
     */
    public static void cancelAll(Object tag) {
        mRequestQueue.cancelAll(tag);
    }

    /**
     * 请求成功的监听
     * <p/>
     * 返回类型:Response.Listener
     *
     * @param callback
     * @return author:hanj
     */
    @SuppressWarnings("rawtypes")
    protected static Response.Listener responseSuccessListener(final VolleyCallback callback) {



        return new Response.Listener() {
            @Override
            public void onResponse(Object obj) {

                VolleyResponse volleyResponse = JSON.parseObject(
                        obj.toString(), VolleyResponse.class);

                callback.requestSuccess(volleyResponse);
            }
        };
    }

    /**
     * 请求失败的监听
     * <p/>
     * 返回类型:Response.ErrorListener
     *
     * @param callback
     * @return author:hanj
     */
    protected static Response.ErrorListener responseErrorListener(final VolleyCallback callback) {

        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                callback.requestError(volleyError);
            }
        };

    }


    public static void showToast(String text) {

        if (mToast == null) {
            mToast = Toast.makeText(MyApplication.getContext(), text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();

    }


}
