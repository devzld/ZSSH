package com.zy.zssh.common.volleyutil;

import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orhanobut.logger.Logger;
import com.zy.zssh.MyApplication;
import com.zy.zssh.common.utils.Tools;

/**
 * 请求网络的回调
 * <p>
 * 2015年10月16日
 * <p>
 * VolleyCallback.java
 *
 * @author hanj
 */
public abstract class VolleyCallback implements VolleyRequestListener {
    /**
     * 请求成功
     */
    @Override
    public void requestSuccess(VolleyResponse response) {


        if(!isSuccess(response)){

            Tools.Toast(MyApplication.getContext(),"服务器异常，请稍后再试");
            return;

        }


    }

    /**
     * 请求失败
     */
    @Override
    public void requestError(VolleyError error) {

        Logger.e(error.toString());
        Toast.makeText(MyApplication.getContext(), "网络不可用，请稍后重试", Toast.LENGTH_SHORT).show();
    }


    /**
     * 统一的请求数据返回，如果有错误，则会提示，如果没有错误，则会执行后面的数据
     *
     * @param response
     * @return
     */
    public boolean isSuccess(VolleyResponse response) {
        boolean status = response.getStatus() != 0;
        if (!status) {

            Toast.makeText(MyApplication.getContext(), "请求出错", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

}
