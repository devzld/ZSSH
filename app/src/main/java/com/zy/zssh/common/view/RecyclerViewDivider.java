package com.zy.zssh.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * desc:
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/19 0019
 **/
public class RecyclerViewDivider extends RecyclerView.ItemDecoration{


    private Paint mPaint;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mOrientation;
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};



    public RecyclerViewDivider(Context context , int orientation){




        if(orientation != LinearLayoutManager.VERTICAL && orientation != LinearLayoutManager.HORIZONTAL){


            throw new IllegalArgumentException("参数错误");


        }


        mOrientation = orientation;

        TypedArray a = context.obtainStyledAttributes(ATTRS);

        mDivider = a.getDrawable(0);

        a.recycle();


    }


    public RecyclerViewDivider(Context context , int orientation , int drawableId){


        this(context,orientation);
        mDivider = ContextCompat.getDrawable(context,drawableId);
        mDividerHeight = mDivider.getIntrinsicHeight();

    }

    public RecyclerViewDivider(Context context , int orientation , int dividerHeight , int dividerColor){


        this(context,orientation);
        mDividerHeight = dividerHeight;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(dividerColor);
        mPaint.setStyle(Paint.Style.FILL);


    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);

        if(mOrientation == LinearLayoutManager.VERTICAL){

            drawVertical(c,parent);

        }else{

            drawHorizontal(c,parent);


        }



    }

    private void drawHorizontal(Canvas c, RecyclerView parent) {

        final int left = parent.getPaddingLeft();
        final int right = parent.getMeasuredWidth() - parent.getPaddingRight();
        final int childSize = parent.getChildCount();
        for (int i = 0; i < childSize; i++) {
            final View child = parent.getChildAt(i);
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int top = child.getBottom() + layoutParams.bottomMargin;
            final int bottom = top + mDividerHeight;
            if (mDivider != null) {
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
            if (mPaint != null) {
                c.drawRect(left, top, right, bottom, mPaint);
            }
        }


    }

    private void drawVertical(Canvas c, RecyclerView parent) {


        int top = parent.getPaddingTop();
        int bottom = parent.getPaddingBottom();
        int childSize = parent.getChildCount();
        for(int i = 0 ; i<childSize ; i++){


            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int left = child.getRight() + params.rightMargin;
            int right = left + mDividerHeight;

            if(mDivider!=null){

                mDivider.setBounds(left,top,right,bottom);
                mDivider.draw(c);

            }

            if(mPaint!=null){

                c.drawRect(left,top,right,bottom,mPaint);

            }


        }


    }





}
