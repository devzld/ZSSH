package com.zy.zssh.common.volleyutil;

/**
 * 响应网络请求
 * <p/>
 * 2015年10月16日
 * <p/>
 * VolleyResponse.java
 *
 * @author hanj
 */
public class VolleyResponse {

    /**
     * 执行结果:待确定
     * <p/>
     * 1:成功
     * <p/>
     * 2：失败
     */
    private int status;

    /**
     * 响应主体,可以是JSONObject格式的字符串，也可以是JSONArray格式的字符串，客户端根据具体业务解析
     */
    private Object body;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "VolleyResponse{" +
                "status=" + status +
                ", body=" + body +
                '}';
    }
}
