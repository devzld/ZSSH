package com.zy.zssh.common.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.LogUtils;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.view.LoadingDialog;

import org.xutils.x;

/**
 * Created by hanji on 16/3/30.
 * <p/>
 * describe: 基本的fragment
 */

public class BaseFragment extends Fragment {

    protected BaseActivity mActivity;
    private boolean injected = false;
    private boolean isShowLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (BaseActivity) getActivity();
        if (savedInstanceState != null) {
            onParcelBundle(savedInstanceState);
        } else {
            Bundle bundle = getArguments();
            if (bundle == null) {
                bundle = new Bundle();
                Log.i("BaseFragment", "savedInstanceState bundle null");
            }
            onParcelBundle(bundle);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        injected = true;
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!injected) {
            x.view().inject(this, this.getView());
        }
    }

    /**
     * 解析bundle
     *
     * @param bundle
     */
    public void onParcelBundle(Bundle bundle) {

    }

    /**
     * show loading dialog
     */
    public void showLoading() {
        if (isShowLoading) {
            return;
        }
        isShowLoading = true;
        LoadingDialog.getInstance(mActivity).show();
        LoadingDialog.getInstance(mActivity).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (isShowLoading)
                    loadingIsCancel();
            }
        });
    }

    /**
     * loading被取消,主动调用取消不执行，之类覆盖
     */
    public void loadingIsCancel() {

    }

    /**
     * cancel loading dialog
     */
    public void cancelLoading() {
        if (!isShowLoading) {
            return;
        }

        isShowLoading = false;

        LoadingDialog.getInstance(mActivity).cancel();
    }

    /**
     * 启动 Activity *
     */
    protected void openActivity(Class<?> pClass) {
        openActivity(pClass, null, 0, 0);
    }

    /**
     * 启动 Activity 含Bundle *
     */
    protected void openActivity(Class<?> pClass, Bundle pBundle) {
        openActivity(pClass, pBundle, 0, 0);
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    /**
     * 启动 Activity 含Bundle *
     */
    protected void openActivity(Class<?> pClass, Bundle pBundle, int in, int out) {
        Intent intent = new Intent(mActivity, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
        if (in == 0 || out == 0) {
            mActivity.overridePendingTransition(in, out);
        }
    }


    /**
     * 启动 Activity 通过action  含Bundle *
     */
    protected void openActivity(String pAction, Bundle pBundle) {
        openActivity(pAction, pBundle, 0, 0);
    }

    protected void openActivity(String pAction, Bundle pBundle, int in, int out) {
        Intent intent = new Intent(pAction);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
        if (in == 0 || out == 0) {
            mActivity.overridePendingTransition(in, out);
        }
    }

    public void animationFinish() {
        animationFinish(0, 0);
    }

    public void animationFinish(int in, int out) {
        mActivity.finish();
        if (in == 0 || out == 0) {
            mActivity.overridePendingTransition(in, out);
        }
    }


    /**
     * @param title   对话框标题
     * @param content 内容
     * @return void 返回类型
     * @Title: showAlertDialog
     * @Description: 显示提示对话框，带一个确认按钮
     * @author yanzi
     */
    public void showAlertDialogSingle(String title, String content) {
        final Dialog dialog = new Dialog(getActivity(), R.style.dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setCancelable(false);
        View contentView = null;
        contentView = LayoutInflater.from(getActivity()).inflate(R.layout.dg_gray, null);
        // 初始化控件
        TextView tv_title = (TextView) contentView
                .findViewById(R.id.tv_dg_double_title);
        TextView tv_content = (TextView) contentView
                .findViewById(R.id.tv_dg_double_content);
        View v_dg_blue_divider_20 = contentView
                .findViewById(R.id.v_dg_blue_divider_20);

        // 设置标题
        if (Tools.isNull(title) && !Tools.isNull(content)) {
            tv_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv_content.setVisibility(View.GONE);
            v_dg_blue_divider_20.setVisibility(View.VISIBLE);
            tv_title.setText(content);
        } else {
            tv_title.setText(title);
        }

        // 设置内容
        if (Tools.isNull(content)) {
            tv_content.setVisibility(View.GONE);
            v_dg_blue_divider_20.setVisibility(View.VISIBLE);
        } else {
            if (!Tools.isNull(title)) {
                tv_content.setText(content);
            }
        }

        // 设置按钮
        TextView tv_dg_blue_singel = (TextView) contentView
                .findViewById(R.id.tv_dg_blue_singel);

        tv_dg_blue_singel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onAlertDialogSingleConfirm();
                dialog.cancel();
            }
        });
        dialog.setContentView(contentView);
        dialog.show();
    }

    /**
     * 弹窗的底部单个按钮--确定
     */
    public void onAlertDialogSingleConfirm() {
        LogUtils.e("onAlertDialogSingleConfirm");
    }

    /**
     * @param title   对话框标题
     * @param content 内容
     * @return void 返回类型
     * @Title: showAlertDialogDouble
     * @Description: 显示提示对话框，带"确认按钮" + “取消按钮”
     * @author lory
     */
    public void showAlertDialogDouble(String title, String content) {
        showAlertDialogDouble(title, content, getActivity().getResources().getString(R.string.cancel), getActivity().getResources().getString(R.string.ok));
    }

    /**
     * @param title     标题，字体大，为null时，title显示content内容且字体大
     * @param content   内容，字体小，为null时不显示内容
     * @param textLeft  左边按钮显示的文字
     * @param textRight 右边按钮显示的文字
     * @return void 返回类型
     * @Title: showAlertDialogDouble
     * @Description: 显示提示对话框，带"确认按钮" + “取消按钮”
     * @author lory
     */
    public void showAlertDialogDouble(String title, String content,
                                      String textLeft, String textRight) {

        final Dialog dialog = new Dialog(getActivity(), R.style.dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setCancelable(true);

        View contentView = null;
        contentView = LayoutInflater.from(mActivity).inflate(R.layout.dg_gray, null);

        // 初始化控件
        TextView tv_title = (TextView) contentView
                .findViewById(R.id.tv_dg_double_title);
        TextView tv_content = (TextView) contentView
                .findViewById(R.id.tv_dg_double_content);
        View v_dg_blue_divider_20 = contentView
                .findViewById(R.id.v_dg_blue_divider_20);

        // 设置标题
        if (Tools.isNull(title) && !Tools.isNull(content)) {
            tv_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv_content.setVisibility(View.GONE);
            v_dg_blue_divider_20.setVisibility(View.VISIBLE);
            tv_title.setText(content);
        } else {
            tv_title.setText(title);
        }

        // 设置内容
        if (Tools.isNull(content)) {
            tv_content.setVisibility(View.GONE);
            v_dg_blue_divider_20.setVisibility(View.VISIBLE);
        } else {
            if (!Tools.isNull(title)) {
                tv_content.setText(content);
            }
        }
        // 设置按钮
        LinearLayout layout = (LinearLayout) contentView
                .findViewById(R.id.ll_dg_blue_double);
        layout.setVisibility(View.VISIBLE);
        TextView tv_dg_blue_singel = (TextView) contentView
                .findViewById(R.id.tv_dg_blue_singel);
        tv_dg_blue_singel.setVisibility(View.GONE);
        TextView tv_dg_blue_cancel = (TextView) contentView
                .findViewById(R.id.tv_dg_blue_cancel);
        TextView tv_dg_blue_confirm = (TextView) contentView
                .findViewById(R.id.tv_dg_blue_confirm);
        tv_dg_blue_cancel.setText(textLeft);
        tv_dg_blue_confirm.setText(textRight);
        tv_dg_blue_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onAlertDialogDoubleCancel();
                dialog.cancel();
            }
        });
        tv_dg_blue_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onAlertDialogDoubleConfirm();
                dialog.cancel();
            }
        });
        dialog.setContentView(contentView);
        dialog.show();

    }

    /**
     * 弹窗底部两个按钮右---确定
     */
    protected void onAlertDialogDoubleConfirm() {
        LogUtils.e("onAlertDialogDoubleConfirm");
    }


    /**
     * 弹窗底部两个按钮左---取消
     */
    protected void onAlertDialogDoubleCancel() {
        LogUtils.e("onAlertDialogDoubleCancel");
    }

}