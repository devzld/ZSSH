package com.zy.zssh.common.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hanji on 16/3/29.
 * <p/>
 * describe:程序配置信息
 */
public class Config {

    private static Config instance = null;

    private static SharedPreferences sharedPreferences = null;

    private String APP_CONFIG_FILE_NAME = "zy_config";
    private String IS_FRIST = "is_first";
    private String PHONE_NUMBER = "phone_number";
    private String IS_SAVE_PASSWORD = "is_save_password";
    private String USER_PASSWORD = "user_password";
    private String USER_NAME = "user_name";
    private String USER_ID = "user_id";
    private String USER_URL = "user_url";
    private String USER_MAIL = "user_mail";
    private String USER_SEX = "user_sex";
    private String USER_SIGN = "user_sign";
    private String USER_SATE = "user_sate";

    private Config(Context context) {
        sharedPreferences = context.getSharedPreferences(APP_CONFIG_FILE_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * 带线程锁
     *
     * @param context
     * @return
     */
    public static synchronized Config ConfigSyn(Context context) {

        if (instance == null)
            instance = new Config(context);
        return instance;
    }

    /**
     * 不带线程锁
     *
     * @param context
     * @return
     */
    public static Config Config(Context context) {

        if (instance == null)
            instance = new Config(context);
        return instance;
    }

    private void setInt(String key, int value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private void setString(String key, String value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void setBoolean(String key, boolean value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    //是否为第一次进入软件

    /**
     * @param isFrist 0表示第一次
     */
    public void setIsFrist(boolean isFrist) {
        setBoolean(IS_FRIST, isFrist);
    }

    public boolean getIsFirst() {
        return sharedPreferences.getBoolean(IS_FRIST, true);
    }

    //存储手机号码
    public void setUserPhoneNumber(String phoneNumber) {
        setString(PHONE_NUMBER, phoneNumber);
    }

    public String getUserPhoneNumber() {
        return sharedPreferences.getString(PHONE_NUMBER, "");
    }

    //是否记住密码

    /**
     * @param isSavePassword 0表示不记住密码
     */
    public void setIsSavePassword(boolean isSavePassword) {
        setBoolean(IS_SAVE_PASSWORD, isSavePassword);
    }

    public boolean getIsSavePassword() {
        return sharedPreferences.getBoolean(IS_SAVE_PASSWORD, true);
    }

    //存储的密码

    public void setUserPassword(String userPassword) {
        setString(USER_PASSWORD, userPassword);
    }

    public String getUserPassword() {
        return sharedPreferences.getString(USER_PASSWORD, "");
    }

    //用户名
    public void setUserName(String userName) {
        setString(USER_NAME, userName);
    }

    public String getUserName() {
        return sharedPreferences.getString(USER_NAME, "");
    }

    //会员id
    public void setUserId(String userId) {
        setString(USER_ID, userId);
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "");
    }

    //用户头像地址
    public void setUserUrl(String userUrl) {
        setString(USER_URL, userUrl);
    }

    public String getUserUrl() {
        return sharedPreferences.getString(USER_URL, "");
    }

    //用户邮箱地址
    public void setUserMail(String userMail) {
        setString(USER_MAIL, userMail);
    }

    public String getUserMail() {
        return sharedPreferences.getString(USER_MAIL, "");
    }


    //用户性别
    public void setUserSex(String userSex) {
        setString(USER_SEX, userSex);
    }

    public String getUserSex() {
        return sharedPreferences.getString(USER_SEX, "3");
    }

    //用户签名
    public void setUserSign(String userSign) {
        setString(USER_SIGN, userSign);
    }

    public String getUserSign() {
        return sharedPreferences.getString(USER_SIGN, "");
    }

    //用户会员状态
    public void setUserSate(String userSate) {
        setString(USER_SATE, userSate);
    }

    public String getUserSate() {
        return sharedPreferences.getString(USER_SATE, "");
    }

}
