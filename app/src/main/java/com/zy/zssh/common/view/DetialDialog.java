package com.zy.zssh.common.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.zy.zssh.R;

/**
 * desc:提示对话框
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011
 **/
@SuppressLint("ValidFragment")
public class DetialDialog extends DialogFragment{


    private Activity activity;

    public DetialDialog(Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {





        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable());
        View view = inflater.inflate(R.layout.view_dialog_notific,null);
//        ImageView close = (ImageView) view.findViewById(R.id.close);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dismiss();
//
//
//            }
//        });


        return view;
    }


}
