package com.zy.zssh.common.dbBean;


import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

import java.io.Serializable;

/**
 * Created by hanji on 16/4/5.
 * <p/>
 * describe:频道列表对应可序化队列属性
 */
@Table(name = "ChannelItem")
public class ChannelItem implements Serializable {

    /**
     * 栏目对应ID
     */
    @Column(name = "id", isId = true)
    public Integer id;

    /**
     * 栏目对应NAME
     */
    @Column(name = "className")
    public String className;

    /**
     * 频道的id
     */
    @Column(name = "classId")
    public String classId;

    /**
     * 栏目是否选中
     */
    @Column(name = "sate")
    public String sate;

    public ChannelItem() {
    }

    public ChannelItem(String className, String classId, String sate) {
        this.className = className;
        this.classId = classId;
        this.sate = sate;
    }

    public int getId() {
        return this.id.intValue();
    }

    public void setId(int paramInt) {
        this.id = Integer.valueOf(paramInt);
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String paramString) {
        this.className = paramString;
    }

    public String getClassId() {
        return this.classId;
    }

    public void setClassId(String paramInt) {
        this.classId = paramInt;
    }

    public String getSate() {
        return this.sate;
    }

    public void setSate(String paramInteger) {
        this.sate = paramInteger;
    }

    public String toString() {
        return "ChannelItem [id=" + this.id + ", className=" + this.className
                + ", sate=" + this.sate + "]";
    }

}