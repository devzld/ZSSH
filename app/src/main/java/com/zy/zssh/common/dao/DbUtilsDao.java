package com.zy.zssh.common.dao;

import com.zy.zssh.common.dbBean.ChannelItem;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.x;

/**
 * Created by hanji on 16/4/5.
 * <p/>
 * describe:数据库访问工具类
 */
public class DbUtilsDao {

    private static DbManager.DaoConfig daoConfig = new DbManager.DaoConfig()
            .setDbName("futuremedia.db")
            // 不设置dbDir时, 默认存储在app的私有目录.
//            .setDbDir(new File("/sdcard")) // "sdcard"的写法并非最佳实践, 这里为了简单, 先这样写了.
            .setDbVersion(1)
            .setDbOpenListener(new DbManager.DbOpenListener() {
                @Override
                public void onDbOpened(DbManager db) {
                    // 开启WAL, 对写入加速提升巨大
                    db.getDatabase().enableWriteAheadLogging();
                }
            })
            .setDbUpgradeListener(new DbManager.DbUpgradeListener() {
                @Override
                public void onUpgrade(DbManager db, int oldVersion, int newVersion) {
                    // TODO: ...
                    // db.addColumn(...);
                    // db.dropTable(...);
                    // ...
                    // or
                    // db.dropDb();
                }
            });
    private static DbManager db = null;

    public static synchronized DbManager getDbManager() {

        if (db == null)
            db = x.getDb(daoConfig);

        return db;
    }

//    public static void initDb() {
//
//        for (int i = 0; i < 10; i++) {
//            try {
//                getDbManager().save(new ChannelItem("新闻" + i, i, 1));
//            } catch (DbException e) {
//                e.printStackTrace();
//            }
//        }
//
//        for (int i = 10; i < 20; i++) {
//            try {
//                getDbManager().save(new ChannelItem("新闻" + i, i - 10, 0));
//            } catch (DbException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     * 清空频道列表数据
     */
    public static void ClearChannelDb() {
        try {
            getDbManager().delete(getDbManager().selector(ChannelItem.class).findAll());

        } catch (DbException e) {
            e.printStackTrace();
        }
    }
}
