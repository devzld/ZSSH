package com.zy.zssh.common.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.zy.zssh.R;


public class SelectPicPopupWindow extends PopupWindow {
    private View rv_photo_paizhao, rv_photo_xcxz, rv_photo_quxiao;
    private View mMenuView;
    Activity mcontext;

    public SelectPicPopupWindow(Activity context, OnClickListener itemsOnClick) {
        super(context);
        this.mcontext=context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.upload_photo,null);
        rv_photo_paizhao = mMenuView.findViewById(R.id.rv_photo_paizhao);
        rv_photo_xcxz =   mMenuView.findViewById(R.id.rv_photo_xcxz);
        rv_photo_quxiao =   mMenuView.findViewById(R.id.rv_photo_quxiao);
        //取消按钮  
        rv_photo_quxiao.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                //销毁弹出框  
            	SelectPicPopupWindow.this.dismiss();
    	        //lp.alpha
    	        // =1f;
    	        //context.getWindow().setAttributes(lp);
            }
        });
        //rv_photo_paizhao.setOnClickListener();
        //设置按钮监听  
        rv_photo_paizhao.setOnClickListener(itemsOnClick);
        rv_photo_xcxz.setOnClickListener(itemsOnClick);
        //设置SelectPicPopupWindow的View  
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽  
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高  
        this.setHeight(LayoutParams.WRAP_CONTENT);
/*        ColorDrawable dw = new ColorDrawable(-00000);
        mMenuView.setBackgroundDrawable(dw);*/
        this.setOutsideTouchable(true);
    }


    public void setText(String tag1, String tag2){
        ((TextView)mMenuView.findViewById(R.id.textview_photo_paizhao)).setText(tag1);
        ((TextView)mMenuView.findViewById(R.id.textview_photo_xcxz)).setText(tag2);
    }

}
