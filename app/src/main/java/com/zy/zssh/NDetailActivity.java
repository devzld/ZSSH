package com.zy.zssh;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orhanobut.logger.Logger;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.news.NewsCommentAdapter;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;
import com.zy.zssh.m_news.bean.news.NewsDetailBean;
import com.zy.zssh.m_news.business.NewsBusiness;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NDetailActivity extends AppCompatActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private String newsId;
    private NewsBusiness business;
    private int comPage;
    private NewsCommentAdapter newsCommentAdapter;
    private NewsDetailBean newsDetailBean;
    /**
     * 新闻评论数据
     */
    private List<NewsCommentBean> newsCommentBeanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ndetail);
        ButterKnife.bind(this);

        initData();


    }

    private void initData() {


        newsCommentAdapter = new NewsCommentAdapter(NDetailActivity.this, newsCommentBeanList, newsDetailBean);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            newsId = bundle.getString("key", "");
        }

        business = NewsBusiness.getInstance();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(newsCommentAdapter);

        getNewsData();

    }


    /**
     * 获取新闻
     *
     */
    private void getNewsData() {
        if (!newsId.equals("")) {


            business.getNewsDetailData(newsId, this, new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);

                    if (isSuccess(response)) {

                        newsDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), NewsDetailBean.class);

                        newsCommentAdapter.setNewsDetailBean(newsDetailBean);

                        newsCommentAdapter.notifyDataSetChanged();

                        Logger.d("获取新闻成功");
                        getNewsCommentData();


                    }


                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);


                }
            });

        } else {

//            Toast.makeText(NewsDetailActivity.this, "很抱歉，加载出错", Toast.LENGTH_SHORT).show();
        }
    }

    private void getNewsCommentData() {


        business.getNewsCommentData(newsId, comPage + "", this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);


                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newscom"), NewsCommentBean.class);

                    if (aa != null) {



                        Logger.d("获取评论成功");


                        if (newsCommentBeanList.size() != 0) {

                            newsCommentBeanList.clear();
                        }


                        newsCommentBeanList.addAll(aa);

                        newsCommentAdapter.notifyDataSetChanged();


                    }




                } else {
                    Toast.makeText(NDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

            }
        });



    }


}
