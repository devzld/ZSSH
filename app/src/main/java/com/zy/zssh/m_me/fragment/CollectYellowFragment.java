package com.zy.zssh.m_me.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.business.MeBusiness;
import com.zy.zssh.m_news.activity.yellow.YellowPageDetailActivity;
import com.zy.zssh.m_news.adapter.yellow.YellowPageAdapter;
import com.zy.zssh.m_news.bean.yellow.YellowPageBean;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/21 0021.
 * 大黄页收藏页面
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.ac_yellow_page)
public class CollectYellowFragment extends BaseFragment {

    private Map<String, String> collectMap;

    @ViewInject(R.id.news_tab_swipeRefreshLayout)
    private SwipyRefreshLayout swipeRefreshLayout;

    @ViewInject(R.id.yellow_page_recycleview)
    private RecyclerView yellow_page_recycleview;

    private MeBusiness business;

    //企业列表
    private List<YellowPageBean> yellowPageBeanList = new ArrayList<>();
    private YellowPageAdapter yellowPageAdapter;

    private String mapID = "0";

    private int pageNum = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    public CollectYellowFragment(Map<String, String> collectMap) {
        this.collectMap = collectMap;
    }

    public CollectYellowFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        yellow_page_recycleview.setHasFixedSize(true);
        yellow_page_recycleview.setItemAnimator(new DefaultItemAnimator());

        yellow_page_recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        business = MeBusiness.getInstance();

        yellowPageAdapter = new YellowPageAdapter(getActivity(), yellowPageBeanList);
        yellow_page_recycleview.setAdapter(yellowPageAdapter);

        initBusinessData();

        setListener();
    }


    /**
     * 设置监听事件
     */
    private void setListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    initBusinessData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreBusinessData();
                }

            }
        });

        yellow_page_recycleview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == yellowPageAdapter.getItemCount()
                        && !isLoading && swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    swipeRefreshLayout.setRefreshing(true);
                    loadMoreBusinessData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        yellowPageAdapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {

                switch (view.getId()) {
                    case R.id.cv_item:

                        //传递id和name数据到详情页面
                        Bundle bundle = new Bundle();
                        bundle.putString("entId", yellowPageBeanList.get(position).getEntId());
                        bundle.putString("entName", yellowPageBeanList.get(position).getEntName());

                        openActivity(YellowPageDetailActivity.class, bundle);
                        break;

                    case R.id.yellow_page_item_like:

                        view.setSelected(!view.isSelected());
                        view.setEnabled(false);

                        business.setzanData(view.isSelected() ? "ent" : "clearent", yellowPageBeanList.get(position).getEntId(), getActivity(),
                                new VolleyCallback() {
                                    @Override
                                    public void requestSuccess(VolleyResponse response) {
                                        super.requestSuccess(response);

                                        if (isSuccess(response)) {

                                            if (view.isSelected()) {
                                                Toast.makeText(getActivity(), "点赞", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getActivity(), "取消赞", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        view.setEnabled(true);
                                    }

                                    @Override
                                    public void requestError(VolleyError error) {
                                        super.requestError(error);
                                        view.setEnabled(true);
                                    }
                                });

                        break;

                    case R.id.yellow_page_item_star_business:

                        view.setSelected(!view.isSelected());
                        view.setEnabled(false);

                        business.setzanData(view.isSelected() ? "enrtcol" : "clearenrtcol", yellowPageBeanList.get(position).getEntId(), getActivity(),
                                new VolleyCallback() {
                                    @Override
                                    public void requestSuccess(VolleyResponse response) {
                                        super.requestSuccess(response);

                                        if (isSuccess(response)) {

                                            if (view.isSelected()) {
                                                Toast.makeText(getActivity(), "收藏", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getActivity(), "取消收藏", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        view.setEnabled(true);
                                    }

                                    @Override
                                    public void requestError(VolleyError error) {
                                        super.requestError(error);
                                        view.setEnabled(true);
                                    }
                                });

                        break;
                }
            }
        });
    }

    /**
     * 获取当前城市的企业列表
     */
    private void initBusinessData() {

        business.getCollectData(pageNum + "", "2", getActivity(), new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<YellowPageBean> aa = FastJsonUtil.jsonString2Beans(response.getBody().toString(), YellowPageBean.class);

                    yellowPageBeanList.clear();
                    if (aa != null && aa.size() != 0) {
                        yellowPageBeanList.addAll(aa);
                    }
                    yellowPageAdapter.notifyDataSetChanged();

                    if (yellowPageBeanList.size() < 20) {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNum++;
                    }
                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 加载更多企业数据
     */
    private void loadMoreBusinessData() {

        business.getCollectData(pageNum + "", "2", getActivity(), new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<YellowPageBean> aa = FastJsonUtil.jsonString2Beans(response.getBody().toString(), YellowPageBean.class);

                    if (aa != null && aa.size() != 0) {
                        yellowPageBeanList.addAll(aa);
                        yellowPageAdapter.notifyDataSetChanged();
                    }

                    if (aa != null && aa.size() == 20) {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNum++;
                    } else {
                        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

}
