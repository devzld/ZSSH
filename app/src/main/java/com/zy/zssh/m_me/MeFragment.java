package com.zy.zssh.m_me;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.m_me.activity.FeedBackActivity;
import com.zy.zssh.m_me.activity.LoginActivity;
import com.zy.zssh.m_me.activity.MeCollectActivity;
import com.zy.zssh.m_me.activity.MessageActivity;
import com.zy.zssh.m_me.activity.SettingActivity;
import com.zy.zssh.m_me.activity.UserInfoActivity;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * Created by hanji on 16/3/29.
 * <p/>
 * describe:我的fragment页面
 */

@ContentView(R.layout.fg_me)
public class MeFragment extends BaseFragment {

    private String TAG = "MeFragment";

    @ViewInject(R.id.me_user_ico)
    private ImageView me_user_ico;

    @ViewInject(R.id.me_username)
    private TextView me_username;

    @ViewInject(R.id.me_sex)
    private ImageView me_sex;

    @ViewInject(R.id.me_signature)
    private TextView me_signature;

    @ViewInject(R.id.me_2_login)
    private TextView me_2_login;

    @ViewInject(R.id.me_jiantou)
    private ImageView me_jiantou;

    @ViewInject(R.id.isVip)
    private ImageView isVip;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    /**
     * 登录按钮的监听
     */
    @Event(R.id.me_2_login)
    private void OnClickLogin(View view) {

        if (Config.Config(getActivity()).getUserId().equals("")) {

            openActivity(LoginActivity.class);
        }
    }

    @Event(R.id.me_person_data)
    private void goPersonData(View view) {

        if (!Config.Config(getActivity()).getUserId().equals("")) {

            openActivity(UserInfoActivity.class);
        }
    }


    //数据加载
    @Override
    public void onResume() {
        super.onResume();

        if (!Config.Config(getActivity()).getUserId().equals("")) {

            me_sex.setVisibility(View.VISIBLE);
            me_signature.setVisibility(View.VISIBLE);
            me_jiantou.setVisibility(View.VISIBLE);
            me_2_login.setVisibility(View.GONE);

            me_username.setText(Config.Config(getActivity()).getUserName());
            ImageLoader.getInstance().displayImage(Config.Config(getActivity()).getUserUrl(), me_user_ico, MyApplication.imageOptionCircle);

            if (Config.Config(getActivity()).getUserSex().equals("1")) {
                me_sex.setImageResource(R.mipmap.personal_man);
                me_sex.setVisibility(View.VISIBLE);
            } else if (Config.Config(getActivity()).getUserSex().equals("2")) {
                me_sex.setImageResource(R.mipmap.personal_woman);
                me_sex.setVisibility(View.VISIBLE);
            } else {
                me_sex.setVisibility(View.GONE);
            }

            me_signature.setText(Config.Config(getActivity()).getUserSign());


            //Logger.d("当前用户的id：" + Config.Config(getActivity()).getUserId());
            //判断当前用户是否未vip
            String iv = Config.Config(getActivity()).getUserSate();
            if(TextUtils.isEmpty(iv) || iv.equals("0")){

                //非会员
                isVip.setImageResource(R.mipmap.feihuiyuan);


            }else{

                //会员
                isVip.setImageResource(R.mipmap.vip);

            }






        } else {

            me_sex.setVisibility(View.GONE);
            me_signature.setVisibility(View.GONE);
            me_2_login.setVisibility(View.VISIBLE);
            me_username.setText("您还没有登陆奥");
            ImageLoader.getInstance().displayImage(Config.Config(getActivity()).getUserUrl(), me_user_ico, MyApplication.imageOptionCircle);
        }

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    /**
     * 收藏
     *
     * @param view
     */
    @Event(R.id.me_collect)
    private void collectClick(View view) {

        if (!Config.Config(getActivity()).getUserId().equals("")) {

            openActivity(MeCollectActivity.class);
        } else {
            Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
            openActivity(LoginActivity.class);
        }

    }

    /**
     * 我的消息
     *
     * @param view
     */
    @Event(R.id.me_message)
    private void messageClick(View view) {

        if (!Config.Config(getActivity()).getUserId().equals("")) {

            openActivity(MessageActivity.class);
        } else {
            Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
            openActivity(LoginActivity.class);
        }
    }

    /**
     * 反馈
     *
     * @param view
     */
    @Event(R.id.me_feedback)
    private void feedbackClick(View view) {

        openActivity(FeedBackActivity.class);
    }

    /**
     * 设置
     *
     * @param view
     */
    @Event(R.id.me_setting)
    private void settingClick(View view) {


        openActivity(SettingActivity.class);
    }
}
