package com.zy.zssh.m_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.android.volley.VolleyError;
import com.orhanobut.logger.Logger;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.AppManager;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.bean.UserInfoBean;
import com.zy.zssh.m_me.business.MeBusiness;
import com.zy.zssh.main.MainActivity;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Map;

public class LoginActivity extends BaseActivity {


    /**
     * 手机号码框
     */
    @ViewInject(R.id.login_phone_number)
    private EditText login_phone_number;

    /**
     * 密码框
     */
    @ViewInject(R.id.login_password)
    private EditText login_password;

    /**
     * 是否记住密码
     */
    @ViewInject(R.id.login_is_password)
    private CheckBox login_is_password;

    @ViewInject(R.id.loginByQQ)
    private ImageView loginByQQ;
    @ViewInject(R.id.loginByWeiXin)
    private ImageView loginByWeiXin;
    @ViewInject(R.id.loginByWeiBo)
    private ImageView loginByWeiBo;


    private MeBusiness business;
    private UMShareAPI mShareAPI;
//    private SHARE_MEDIA platform;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_login);

        x.view().inject(this);
        AppManager.getAppManager().addLoginActivity(this);


        mShareAPI = UMShareAPI.get(this);


        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("登录");
        setRightTitle("注册");
        setLeftImageViewHide(true);

        login_phone_number.setText(Config.Config(this).getUserPhoneNumber());

        boolean isSave = Config.Config(this).getIsSavePassword();

        if (isSave) {
            login_password.setText(Config.Config(this).getUserPassword());
            login_is_password.setChecked(true);
        } else {
            login_is_password.setChecked(false);
        }

        login_is_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Config.Config(LoginActivity.this).setIsSavePassword(isChecked);
                if (!isChecked) {

                    Config.Config(LoginActivity.this).setUserPassword("");
                }
            }
        });


        //QQ登陆
        loginByQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                platform = SHARE_MEDIA.QQ;
//                mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);
                loginByPlatform(SHARE_MEDIA.QQ);


            }
        });

        //微信登陆
        loginByWeiXin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                platform = SHARE_MEDIA.WEIXIN;
//                mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);
                loginByPlatform(SHARE_MEDIA.WEIXIN);


            }
        });

        //微博登陆
        loginByWeiBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                platform = SHARE_MEDIA.SINA;
//                mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);

                loginByPlatform(SHARE_MEDIA.SINA);

            }
        });




    }


    //第三方登陆
    private void loginByPlatform(SHARE_MEDIA platform){

        //显示加载框


        mShareAPI.doOauthVerify(LoginActivity.this, platform, umAuthListener);

        showLoadingView();
    }



    //友盟第三方登陆回调接口
    private UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {

//            Toast.makeText( getApplicationContext(), "Authorize succeed", Toast.LENGTH_SHORT).show();

//            uid = data.get("uid");
            //登陆成功
//            Logger.d("第三方登陆：" + data.toString());

            mShareAPI.getPlatformInfo(LoginActivity.this, platform, new UMAuthListener() {
                @Override
                public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {

                    //获取用户信息成功
//                    Logger.d("获取用户信息成功：" + map.toString());

//                    String headurl = map.get("profile_image_url");
//                    String sex = map.get("gender");
//
//                    MeBusiness.getInstance().loginByThrid(uid, headurl, sex, LoginActivity.this, new VolleyCallback() {
//                        @Override
//                        public void requestSuccess(VolleyResponse response) {
//                            super.requestSuccess(response);
//                            saveUserInfo(response);
//
//                        }
//                    });

                    String uid = "";
                    String headurl = "";
                    String sex = "";


                    //根据登陆平台的不同分别处理   新浪登陆
                    if(share_media.equals(SHARE_MEDIA.SINA)){


                        String result = map.get("result");
//                        Logger.d("新浪返回的数据：" + result);

                        JSONObject jsonObject = (JSONObject) JSONObject.parse(result);
                        uid = jsonObject.getString("id");
                        headurl = jsonObject.getString("profile_image_url");
                        if(jsonObject.getString("gender").equals("m")){


                            sex = "2";

                        }else{

                            sex = "1";

                        }



                    }else if(share_media.equals(SHARE_MEDIA.QQ)){


                        uid = map.get("openid");
                        headurl = map.get("profile_image_url");
                        if(map.get("gender").equals("男")){


                            sex = "2";

                        }else{

                            sex = "1";

                        }



                    }else if(share_media.equals(SHARE_MEDIA.WEIXIN)){


                        uid = map.get("openid");
                        headurl = map.get("headimgurl");
                        if(map.get("sex").equals("1")){


                            sex = "2";

                        }else{

                            sex = "1";

                        }



                    }


//                    Logger.d("最终提交的数据：" + "uid:" + uid + " headurl:" + headurl + " sex:" + sex);

                    MeBusiness.getInstance().loginByThrid(uid, headurl, sex, LoginActivity.this, new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);
                            saveUserInfo(response);

                        }
                    });




                }

                @Override
                public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {


                    Logger.e(throwable.getMessage());
                    Toast.makeText( getApplicationContext(), "获取个人资料错误", Toast.LENGTH_SHORT).show();
                    endLoadingView();

                }

                @Override
                public void onCancel(SHARE_MEDIA share_media, int i) {


                    Toast.makeText( getApplicationContext(), "取消获取个人资料", Toast.LENGTH_SHORT).show();
                    endLoadingView();
                }
            });




        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText( getApplicationContext(), "认证失败", Toast.LENGTH_SHORT).show();
            endLoadingView();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Toast.makeText( getApplicationContext(), "取消认证", Toast.LENGTH_SHORT).show();
            endLoadingView();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mShareAPI.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void initData() {

        business = MeBusiness.getInstance();
    }

    @Override
    protected void initClickListener(View v) {

    }

    @Override
    public void rightTvClick() {
        super.rightTvClick();

        openActivity(RegisterActivity.class);
    }


    //登陆成功之后进行的操作
    public void saveUserInfo(VolleyResponse response){

        UserInfoBean userInfoBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), UserInfoBean.class);

        if (userInfoBean.getRester().equals("1")) {
            Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
            Config.ConfigSyn(LoginActivity.this).setIsFrist(true);
            Config.Config(LoginActivity.this).setUserPhoneNumber(login_phone_number.getText().toString());
            Config.Config(LoginActivity.this).setUserName(userInfoBean.getLoginName());
            Config.Config(LoginActivity.this).setUserId(userInfoBean.getUserId());
            Config.Config(LoginActivity.this).setUserUrl(userInfoBean.getUserUrl());
            Config.Config(LoginActivity.this).setUserMail(userInfoBean.getUserMail());
            Config.Config(LoginActivity.this).setUserSex(userInfoBean.getSex());
            Config.Config(LoginActivity.this).setUserSign(userInfoBean.getQian());
            Config.Config(LoginActivity.this).setUserSate(userInfoBean.getUserSate());

            //是否为记住密码，是保存密码
            if (login_is_password.isChecked()) {
                Config.Config(LoginActivity.this).setUserPassword(login_password.getText().toString());
            }


            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

//                                    AppManager.getAppManager().finishLogin();
            startActivity(intent);
            finish();

        } else if (userInfoBean.getRester().equals("2")) {

            Toast.makeText(LoginActivity.this, "该手机号码暂未注册", Toast.LENGTH_SHORT).show();
        } else if (userInfoBean.getRester().equals("3")) {

            Toast.makeText(LoginActivity.this, "密码错误", Toast.LENGTH_SHORT).show();
        }




    }



    /**
     * 登录按钮监听
     *
     * @param view
     */
    @Event(R.id.login)
    private void Login(View view) {

        Tools.hideKeyboard(this);

        if (isSure()) {

            showLoadingView();
            business.getUserLogin(login_phone_number.getText().toString(),
                    login_password.getText().toString(), LoginActivity.this, new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            //1登陆成功 2手机号未注册3密码错误
                            if (isSuccess(response)) {

                                UserInfoBean userInfoBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), UserInfoBean.class);

//                                if (userInfoBean.getRester().equals("1")) {
//                                    Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
//                                    Config.ConfigSyn(LoginActivity.this).setIsFrist(true);
//                                    Config.Config(LoginActivity.this).setUserPhoneNumber(login_phone_number.getText().toString());
//                                    Config.Config(LoginActivity.this).setUserName(userInfoBean.getLoginName());
//                                    Config.Config(LoginActivity.this).setUserId(userInfoBean.getUserId());
//                                    Config.Config(LoginActivity.this).setUserUrl(userInfoBean.getUserUrl());
//                                    Config.Config(LoginActivity.this).setUserMail(userInfoBean.getUserMail());
//                                    Config.Config(LoginActivity.this).setUserSex(userInfoBean.getSex());
//                                    Config.Config(LoginActivity.this).setUserSign(userInfoBean.getQian());
//                                    Config.Config(LoginActivity.this).setUserSate(userInfoBean.getUserSate());
//
//                                    //是否为记住密码，是保存密码
//                                    if (login_is_password.isChecked()) {
//                                        Config.Config(LoginActivity.this).setUserPassword(login_password.getText().toString());
//                                    }
//
//
//                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
////                                    AppManager.getAppManager().finishLogin();
//                                    startActivity(intent);
//                                    finish();
//
//                                } else if (userInfoBean.getRester().equals("2")) {
//
//                                    Toast.makeText(LoginActivity.this, "该手机号码暂未注册", Toast.LENGTH_SHORT).show();
//                                } else if (userInfoBean.getRester().equals("3")) {
//
//                                    Toast.makeText(LoginActivity.this, "密码错误", Toast.LENGTH_SHORT).show();
//                                }

                                saveUserInfo(response);


                            }

                            endLoadingView();
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);

                            endLoadingView();
                        }
                    });

        }
    }

//    /**
//     * 忘记密码按钮监听
//     *
//     * @param view
//     */
//    @Event(R.id.login_forget_password)
//    private void ForgetPassword(View view) {
//
//        openActivity(ForgetActivity.class);
//    }

//    /**
//     * 第三方QQ登录
//     *
//     * @param view
//     */
//    @Event(R.id.login_three_qq)
//    private void LoginThreeQQ(View view) {
//
////        business.ceshi("1", "2", this, new VolleyCallback() {
////            @Override
////            public void requestSuccess(VolleyResponse response) {
////                super.requestSuccess(response);
////                System.out.println("=========>>>" + response.getBody().toString());
////            }
////
////            @Override
////            public void requestError(VolleyError error) {
////                super.requestError(error);
////            }
////        });
//
//    }

//    /**
//     * 第三方微信登录
//     *
//     * @param view
//     */
//    @Event(R.id.login_three_weixin)
//    private void LoginThreeWeiXin(View view) {
//
//    }
//
//
//    /**
//     * 第三方微博登录
//     *
//     * @param view
//     */
//    @Event(R.id.login_three_weibo)
//    private void LoginThreeWeiBo(View view) {
//
//    }


    private boolean isSure() {

        if (Tools.isNull(login_phone_number)) {
            Toast.makeText(LoginActivity.this, "手机号码不能为空", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Tools.validatePhone(login_phone_number.getText().toString())) {
            Toast.makeText(LoginActivity.this, "请输入真确的手机号码", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Tools.isNull(login_password)) {
            Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
