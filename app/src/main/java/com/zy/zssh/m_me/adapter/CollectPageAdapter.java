package com.zy.zssh.m_me.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zy.zssh.m_me.fragment.CollectVideoFragment;
import com.zy.zssh.m_me.fragment.CollectYellowFragment;

import java.util.List;
import java.util.Map;

/**
 * 项目名称：ZSSH_Android
 * 类描述：
 * 创建人：HJ
 * 创建时间：2016/6/19 22:00
 */
public class CollectPageAdapter extends FragmentPagerAdapter {

    private List<Map<String, String>> collectList;

    public CollectPageAdapter(FragmentManager fm, List<Map<String, String>> collectList) {
        super(fm);
        this.collectList = collectList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return collectList.get(position).get("name");
    }

    @Override
    public Fragment getItem(int position) {

        if ("1".equals(collectList.get(position).get("id"))) {

            return new CollectVideoFragment(collectList.get(position));
        } else {

            return new CollectYellowFragment(collectList.get(position));
        }

    }

    @Override
    public int getCount() {
        return collectList.size();
    }
}
