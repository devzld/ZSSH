package com.zy.zssh.m_me.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.AppManager;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.bean.UserInfoBean;
import com.zy.zssh.m_me.business.MeBusiness;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class ForgetActivity extends BaseActivity {

    /**
     * 获取验证码
     */
    @ViewInject(R.id.register_get_code)
    private TextView register_get_code;

    /**
     * 用户输入的手机号码
     */
    @ViewInject(R.id.register_phone_number)
    private EditText register_phone_number;

    /**
     * 用户输入的密码
     */
    @ViewInject(R.id.register_password)
    private EditText register_password;

    /**
     * 用户输入的验证码
     */
    @ViewInject(R.id.register_code)
    private EditText register_code;

    private MeBusiness business;

    private String str_regist_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_forget);

        x.view().inject(this);
        AppManager.getAppManager().addLoginActivity(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("忘记密码");

        setRightTitle("登录");

        register_get_code.setOnClickListener(this);
    }

    @Override
    public void rightTvClick() {
        super.rightTvClick();

        openActivity(LoginActivity.class);
    }

    @Override
    protected void initData() {

        business = MeBusiness.getInstance();
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {
            case R.id.register_get_code:

                String phone_number = register_phone_number.getText().toString();

                if (!Tools.validatePhone(phone_number)) {

                    Toast.makeText(ForgetActivity.this, "请输入正确手机号码", Toast.LENGTH_SHORT).show();

                } else {

                    btnCancelClick();
                    getTimer();

                    Tools.hideKeyboard(ForgetActivity.this);

                    business.getRegisterCode(phone_number, ForgetActivity.this, new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {

                                str_regist_code = FastJsonUtil.getNoteJson(response.getBody().toString(), "number");

                                Toast.makeText(ForgetActivity.this, str_regist_code, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                        }
                    });
                }

                break;
        }

    }

    /**
     * 注册按钮的监听
     *
     * @param view
     */
    @Event(R.id.register)
    private void Register(View view) {

        if (isSure()) {

            showLoadingView();
            business.getForget(register_phone_number.getText().toString(),
                    register_password.getText().toString(), ForgetActivity.this, new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {

                                UserInfoBean userInfoBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), UserInfoBean.class);

                                //1成功
                                if (userInfoBean.getRester().equals("1")) {

                                    Toast.makeText(ForgetActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                                    Config.ConfigSyn(ForgetActivity.this).setIsFrist(true);
                                    Config.Config(ForgetActivity.this).setUserPhoneNumber(register_phone_number.getText().toString());
                                    Config.Config(ForgetActivity.this).setUserName(userInfoBean.getLoginName());
                                    Config.Config(ForgetActivity.this).setUserId(userInfoBean.getUserId());
                                    Config.Config(ForgetActivity.this).setUserUrl(userInfoBean.getUserUrl());
                                    Config.Config(ForgetActivity.this).setUserMail(userInfoBean.getUserMail());
                                    Config.Config(ForgetActivity.this).setUserSex(userInfoBean.getSex());
                                    Config.Config(ForgetActivity.this).setUserSign(userInfoBean.getQian());
                                    Config.Config(ForgetActivity.this).setUserSate(userInfoBean.getUserSate());

                                    AppManager.getAppManager().finishLogin();

                                } else {

                                    Toast.makeText(ForgetActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
                                }
                            }
                            endLoadingView();
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                            endLoadingView();
                        }
                    });
        }
    }

    /**
     * 验证码倒计时
     */
    private void getTimer() {
        CountDownTimer timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                register_get_code.setEnabled(false);
                register_get_code.setText(millisUntilFinished / 1000 + "秒后重发");
            }

            @Override
            public void onFinish() {
                register_get_code.setEnabled(true);
                register_get_code.setText("获取验证码");
            }

        };
        timer.start();
    }

    /**
     * 限制view的快速点击
     */
    private void btnCancelClick() {
        CountDownTimer timer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                register_get_code.setEnabled(false);
            }

            @Override
            public void onFinish() {
                register_get_code.setEnabled(true);
            }
        };
        timer.start();
    }

    /**
     * 验证号码密码验证码是否为就、空
     *
     * @return
     */
    private boolean isSure() {

        if (Tools.isNull(register_phone_number)) {
            Toast.makeText(ForgetActivity.this, "手机号码不能为空", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Tools.validatePhone(register_phone_number.getText().toString())) {
            Toast.makeText(ForgetActivity.this, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Tools.isNull(register_password)) {
            Toast.makeText(ForgetActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Tools.isNull(register_code)) {
            Toast.makeText(ForgetActivity.this, "验证码不能为空", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!str_regist_code.equals(register_code.getText().toString())) {
            Toast.makeText(ForgetActivity.this, "验证码错误", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
