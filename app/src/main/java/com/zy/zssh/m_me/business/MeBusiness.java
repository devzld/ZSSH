package com.zy.zssh.m_me.business;

import android.content.Context;

import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hanj .
 */
public class MeBusiness {
    private static MeBusiness instance = new MeBusiness();

    private MeBusiness() {

    }

    public static synchronized MeBusiness getInstance() {

        return instance;
    }

    /**
     * 获取验证码
     *
     * @param phone    手机号码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getRegisterCode(String phone,
                                Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "sms" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("phone", phone);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 用户注册
     *
     * @param useriphone 手机号码
     * @param loginPwd   密码
     * @param context    上下文环境
     * @param callback   回调callback
     */
    public void getRegister(String useriphone, String loginPwd,
                            Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "userzhuce" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("useriphone", useriphone);
        params.put("loginPwd", loginPwd);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 忘记密码
     *
     * @param iphone   手机号码
     * @param loginpwd 密码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getForget(String iphone, String loginpwd,
                          Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "forget" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("iphone", iphone);
        params.put("loginpwd", loginpwd);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 用户登录
     *
     * @param useriphone 手机号码
     * @param loginPwd   密码
     * @param context    上下文环境
     * @param callback   回调callback
     */
    public void getUserLogin(String useriphone, String loginPwd,
                             Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "userlogin" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("useriphone", useriphone);
        params.put("loginPwd", loginPwd);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取用户消息动态
     *
     * @param page     页码
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getUserMessage(String page, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "userdong" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 回复用户动态
     *
     * @param yuan     来源
     * @param leiId    原来动态的编号
     * @param mian     评论内容
     * @param comId    原评论编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void sendUserMessage(String yuan, String leiId, String mian, String comId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "hfdong" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("yuan", yuan);
        params.put("leiId", leiId);
        params.put("mian", mian);
        params.put("comId", comId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 我的收藏
     *
     * @param page     页码
     * @param type     1视频  2大黄页
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getCollectData(String page, String type, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "appusercol" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("type", type);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 上传用户数据
     *
     * @param loginName  用户名
     * @param userMail   电子邮箱
     * @param sex        性别
     * @param qian       个性签名
     * @param userIphone 手机号
     * @param context    上下文环境
     * @param callback   回调callback
     */

    public void updateUserInfo(String loginName, String userMail, String sex,
                               String qian, String userIphone, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "userupdate" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());
        params.put("loginName", loginName);
        params.put("userMail", userMail);
        params.put("sex", sex);
        params.put("qian", qian);
        params.put("userIphone", userIphone);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 用户反馈数据
     *
     * @param title    联系方式
     * @param mian     反馈内容
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void updateUserFeedBack(String title, String mian, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "appuserfk" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());
        params.put("title", title);
        params.put("mian", mian);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 上传用户头像
     *
     * @param basestring 图片的base64
     * @param context    上下文环境
     * @param callback   回调callback
     */
    public void updateUserIco(String basestring, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "updatehead" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());
        params.put("basestring", basestring);

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 设置赞数据
     *
     * @param type     类型，新闻视频等
     * @param id       id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void setzanData(String type, String id, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "zan" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("id", id);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 第三方登陆
     *
     * @param uid
     * @param headurl
     * @param sex
     * @param context
     * @param callback
     */
    public void loginByThrid(String uid , String headurl , String sex , Context context , VolleyCallback callback){


        String url = VolleyClient.SERVER_IP_ADDRESS + "Quick" + VolleyClient.SERVER_IP_FOOT;
        Map<String,String> params = new HashMap<>();
        params.put("uid",uid);
        params.put("headurl",headurl);
        if(sex.equals("男")){

            params.put("sex","2");

        }else{

            params.put("sex","1");

        }
        VolleyClient.getInstance(context).requsetOfPost(url,params,callback);





    }






}


