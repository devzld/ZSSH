package com.zy.zssh.m_me.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.business.MeBusiness;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class FeedBackActivity extends BaseActivity {

    @ViewInject(R.id.me_feedback_link)
    private EditText me_feedback_link;

    @ViewInject(R.id.me_feedback_contain)
    private EditText me_feedback_contain;

    private MeBusiness business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_feed_back);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("反馈");
        business = MeBusiness.getInstance();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initClickListener(View v) {

    }

    /**
     * 反馈内容的提交
     *
     * @param view
     */
    @Event(R.id.me_feedback_commit)
    private void commitFeedBack(View view) {

        if (me_feedback_contain.getText().toString().equals("")) {

            Toast.makeText(FeedBackActivity.this, "提交内容不能为空", Toast.LENGTH_SHORT).show();
        } else {
            business.updateUserFeedBack(me_feedback_link.getText().toString(), me_feedback_contain.getText().toString(), this, new VolleyCallback() {
                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);

                    if (isSuccess(response)) {
                        Toast.makeText(FeedBackActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(FeedBackActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);
                    Toast.makeText(FeedBackActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
