package com.zy.zssh.m_me.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_me.bean.MeMessageBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.List;

/**
 * Created by Administrator on 2016/6/20 0020.
 * 我的消息adapter
 */
public class MeMessageAdapter extends BaseRecycleAdapter {

    private List<MeMessageBean> meMessageBeanList;

    public MeMessageAdapter(Context mContext, List<MeMessageBean> meMessageBeanList) {
        super(mContext);
        this.meMessageBeanList = meMessageBeanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MeMessageViewHolder(mLayoutInflater.inflate(R.layout.me_message_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(getItem(position).getUrl(),
                ((MeMessageViewHolder) holder).me_message_item_ico, MyApplication.imageOptionCircle);

        ((MeMessageViewHolder) holder).me_message_item_name.setText(getItem(position).getLoginName());

        ((MeMessageViewHolder) holder).me_message_item_time.setText(getItem(position).getAddtime());

        if ("0".equals(getItem(position).getType())) {

            ((MeMessageViewHolder) holder).me_message_item_replay.setText(getItem(position).getMian());
        } else {

            ((MeMessageViewHolder) holder).me_message_item_replay.setText("攒了这条评论");
        }

        ((MeMessageViewHolder) holder).me_message_item_original.setText(getItem(position).getMian2());
    }

    private MeMessageBean getItem(int position) {

        return meMessageBeanList.get(position);
    }

    @Override
    public int getItemCount() {
        return meMessageBeanList.size();
    }

    public class MeMessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.cv_item)
        private View cv_item;

        @ViewInject(R.id.me_message_item_ico)
        private ImageView me_message_item_ico;

        @ViewInject(R.id.me_message_item_name)
        private TextView me_message_item_name;

        @ViewInject(R.id.me_message_item_time)
        private TextView me_message_item_time;

        @ViewInject(R.id.me_message_item_replay)
        private TextView me_message_item_replay;

        @ViewInject(R.id.me_message_item_original)
        private TextView me_message_item_original;

        public MeMessageViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getPosition());
            }

        }
    }
}
