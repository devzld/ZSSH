package com.zy.zssh.m_me.bean;

/**
 * Created by Administrator on 2016/6/20 0020.
 */
public class MeMessageBean {


    /**
     * userId : 10086
     * loginName : 小张
     * sate1 : 回复
     * mian : 哈哈
     * mian2 : hei
     * sate : 0
     * type : 1
     * yuan : 1
     * feiId : 1
     * comId : 1
     * addtime : 2016-6-20 9:55:58
     * url : http://app.shzywh.cn/app/images/head.png
     */

    private String userId;
    private String loginName;
    private String sate1;
    private String mian;
    private String mian2;
    private String sate;
    private String type;
    private String yuan;
    private String feiId;
    private String comId;
    private String addtime;
    private String url;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getSate1() {
        return sate1;
    }

    public void setSate1(String sate1) {
        this.sate1 = sate1;
    }

    public String getMian() {
        return mian;
    }

    public void setMian(String mian) {
        this.mian = mian;
    }

    public String getMian2() {
        return mian2;
    }

    public void setMian2(String mian2) {
        this.mian2 = mian2;
    }

    public String getSate() {
        return sate;
    }

    public void setSate(String sate) {
        this.sate = sate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYuan() {
        return yuan;
    }

    public void setYuan(String yuan) {
        this.yuan = yuan;
    }

    public String getFeiId() {
        return feiId;
    }

    public void setFeiId(String feiId) {
        this.feiId = feiId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
