package com.zy.zssh.m_me.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.m_me.adapter.MessagePageAdapter;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageActivity extends BaseActivity {

    @ViewInject(R.id.collect_tablayout)
    private SlidingTabLayout collect_tablayout;

    @ViewInject(R.id.collect_view_pager)
    private ViewPager collect_view_pager;

    private MessagePageAdapter messagePageAdapter;

    private List<Map<String, String>> collectList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_message);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setHidePublicTitle(true);
    }

    @Override
    protected void initData() {

        Map<String, String> map;
        map = new HashMap<>();
        map.put("id", "1");
        map.put("name", "动态");
        collectList.add(map);

        map = new HashMap<>();
        map.put("id", "2");
        map.put("name", "消息");
        collectList.add(map);

        messagePageAdapter = new MessagePageAdapter(getSupportFragmentManager(), collectList);

        collect_view_pager.setAdapter(messagePageAdapter);

        collect_tablayout.setViewPager(collect_view_pager);
    }

    @Event(R.id.collect_back)
    private void back(View view) {
        finish();
    }

    @Override
    protected void initClickListener(View v) {

    }
}
