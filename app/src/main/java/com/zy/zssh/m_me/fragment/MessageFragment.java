package com.zy.zssh.m_me.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.adapter.MeMessageAdapter;
import com.zy.zssh.m_me.bean.MeMessageBean;
import com.zy.zssh.m_me.business.MeBusiness;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/20 0020.
 * 我的消息fragment
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_me_message)
public class MessageFragment extends BaseFragment {

    private Map<String, String> collectMap;

    @ViewInject(R.id.root_view)
    private View root_view;

    @ViewInject(R.id.me_message_tab_swipeRefreshLayout)
    private SwipyRefreshLayout me_message_tab_swipeRefreshLayout;

    @ViewInject(R.id.me_message_tab_recycler_view)
    private RecyclerView me_message_tab_recycler_view;

    @ViewInject(R.id.me_message_item_com_layout)
    private View me_message_item_com_layout;

    @ViewInject(R.id.me_message_item_com_edit)
    private EditText me_message_item_com_edit;

    private List<MeMessageBean> meMessageBeanList = new ArrayList<>();

    private MeMessageAdapter meMessageAdapter;

    private MeBusiness business;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    private int position;

    public MessageFragment(Map<String, String> collectMap) {
        this.collectMap = collectMap;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();

        setListener();
    }

    private void init() {

        business = MeBusiness.getInstance();

        if (meMessageBeanList.size() == 0) {

            me_message_tab_swipeRefreshLayout.setRefreshing(true);
            initData();
        }

        me_message_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        me_message_tab_recycler_view.setHasFixedSize(true);
        me_message_tab_recycler_view.setItemAnimator(new DefaultItemAnimator());

        me_message_tab_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));

        meMessageAdapter = new MeMessageAdapter(getActivity(), meMessageBeanList);

        me_message_tab_recycler_view.setAdapter(meMessageAdapter);

        me_message_tab_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));
    }

    private void setListener() {
        me_message_tab_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    meMessageBeanList.clear();
                    initData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }

            }
        });

        me_message_tab_recycler_view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == meMessageAdapter.getItemCount()
                        && !isLoading && me_message_tab_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    me_message_tab_swipeRefreshLayout.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        meMessageAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if ("0".equals(meMessageBeanList.get(position).getType())) {

                    me_message_item_com_layout.setVisibility(View.VISIBLE);

                    me_message_item_com_edit.requestFocus();
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                    MessageFragment.this.position = position;

                } else {
                    me_message_item_com_layout.setVisibility(View.GONE);
                }
            }
        });

        me_message_item_com_edit.setOnKeyListener(backlistener);
        root_view.setOnKeyListener(backlistener);
        me_message_tab_recycler_view.setOnKeyListener(backlistener);
    }

    /**
     * 初始化消息数据
     */
    private void initData() {

        business.getUserMessage("1", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<MeMessageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "videocom"), MeMessageBean.class);

                    if (aa != null && aa.size() != 0) {

                        meMessageBeanList.clear();
                        meMessageBeanList.addAll(aa);
                        meMessageAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        me_message_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        me_message_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                me_message_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                me_message_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getUserMessage(pageNo + "", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<MeMessageBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "videocom"), MeMessageBean.class);

                    if (aa != null && aa.size() != 0) {

                        meMessageBeanList.addAll(aa);

                        meMessageAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        me_message_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        me_message_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                me_message_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                me_message_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 发送评论
     *
     * @param view
     */
    @Event(R.id.me_message_item_com_send)
    private void sendCom(View view) {

        if (!"".equals(me_message_item_com_edit.getText().toString())) {

            business.sendUserMessage(meMessageBeanList.get(position).getYuan(), meMessageBeanList.get(position).getFeiId(),
                    me_message_item_com_edit.getText().toString(), meMessageBeanList.get(position).getComId(), getActivity(), new VolleyCallback() {
                        @Override
                        public void requestSuccess(VolleyResponse response) {
                            super.requestSuccess(response);

                            if (isSuccess(response)) {
                                Tools.hideKeyboard(getActivity());
                                me_message_item_com_layout.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(getActivity(), "回复失败，请稍后重试！", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void requestError(VolleyError error) {
                            super.requestError(error);
                        }
                    });
        } else {

            Toast.makeText(getActivity(), "回复内容不能为空", Toast.LENGTH_SHORT).show();
        }
    }

    private View.OnKeyListener backlistener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (i == KeyEvent.KEYCODE_BACK) {  //表示按返回键 时的操作

                    if (me_message_item_com_layout.getVisibility() == View.VISIBLE) {
                        me_message_item_com_layout.setVisibility(View.GONE);

                        return true;
                    }
                }
            }
            return false;
        }
    };
}
