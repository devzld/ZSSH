package com.zy.zssh.m_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.dao.DbUtilsDao;
import com.zy.zssh.common.dbBean.ChannelItem;
import com.zy.zssh.common.utils.DataCleanManager;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.main.MainActivity;

import org.xutils.ex.DbException;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class SettingActivity extends BaseActivity {

    @ViewInject(R.id.setting_clear_cache_size)
    private TextView setting_clear_cache_size;

    @ViewInject(R.id.setting_version)
    private TextView setting_version;

    @ViewInject(R.id.setting_log_out)
    private View setting_log_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_setting);

        x.view().inject(this);
        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("设置");

        try {
            setting_clear_cache_size.setText(DataCleanManager.getCacheSize(getCacheDir(), getExternalCacheDir()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        setting_version.setText(Tools.getVerName(this));

        if (Config.Config(this).getUserId().equals("")) {
            setting_log_out.setVisibility(View.GONE);
        } else {
            setting_log_out.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initData() {

    }

    /**
     * 清楚缓存
     *
     * @param view
     */
    @Event(R.id.setting_clear_cache)
    private void clearCache(View view) {

        DataCleanManager.cleanInternalCache(this);
        DataCleanManager.cleanExternalCache(this);

        try {
            setting_clear_cache_size.setText(DataCleanManager.getCacheSize(getCacheDir(), getExternalCacheDir()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        toastMsg("清除成功");
    }

    /**
     * 退出登录
     *
     * @param view
     */
    @Event(R.id.setting_log_out)
    private void logOut(View view) {
        try {
            DbUtilsDao.getDbManager().dropTable(ChannelItem.class);
        } catch (DbException e) {
            e.printStackTrace();
        }

        //清楚个人信息
        Config.Config(this).setIsFrist(true);
        Config.Config(this).setUserPhoneNumber("");
        Config.Config(this).setIsSavePassword(true);
        Config.Config(this).setUserPassword("");
        Config.Config(this).setUserName("");
        Config.Config(this).setUserId("");
        Config.Config(this).setUserUrl("");
        Config.Config(this).setUserMail("");
        Config.Config(this).setUserSex("");
        Config.Config(this).setUserSign("");
        Config.Config(this).setUserSate("");

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    @Override
    protected void initClickListener(View v) {

    }
}
