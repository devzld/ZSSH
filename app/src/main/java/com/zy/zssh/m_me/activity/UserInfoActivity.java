package com.zy.zssh.m_me.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.CommonTool;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.PictureUtil;
import com.zy.zssh.common.view.SelectPicPopupWindow;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.business.MeBusiness;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class UserInfoActivity extends BaseActivity {

    private PopupWindow popupWindow;

    @ViewInject(R.id.user_info_ico_bg)
    private ImageView user_info_ico_bg;

    @ViewInject(R.id.user_info_name)
    private EditText user_info_name;

    @ViewInject(R.id.user_info_sex)
    private TextView user_info_sex;

    @ViewInject(R.id.user_info_phone)
    private EditText user_info_phone;

    @ViewInject(R.id.user_info_mail)
    private EditText user_info_mail;

    @ViewInject(R.id.user_info_sign)
    private EditText user_info_sign;

    private MeBusiness business;

    private SelectPicPopupWindow menuWindow;

    // 头像上传
    public static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 裁剪

    private String str_image;
    private File tempFile;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.ac_user_info);

        x.view().inject(this);

        initView();

        initData();
    }

    @Override
    protected void initView() {

        setMiddleTitle("个人信息");
        setRightTitle("保存");
        setRightTitleColorSize(getResources().getColor(R.color.right_title_color));

        menuWindow = new SelectPicPopupWindow(this, this);
        menuWindow.setOnDismissListener(new PersonInformaton());

        business = MeBusiness.getInstance();
    }

    @Override
    protected void initData() {

        ImageLoader.getInstance().displayImage(Config.Config(this).getUserUrl(), user_info_ico_bg, MyApplication.imageOptionCircle);
        user_info_name.setText(Config.Config(this).getUserName());

        if (Config.Config(this).getUserSex().equals("1")) {
            user_info_sex.setText("男");
        } else if (Config.Config(this).getUserSex().equals("2")) {
            user_info_sex.setText("女");
        } else {
            user_info_sex.setText("暂未设置性别");
        }

        user_info_phone.setText(Config.Config(this).getUserPhoneNumber());
        user_info_mail.setText(Config.Config(this).getUserMail());
        user_info_sign.setText(Config.Config(this).getUserSign());
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {
            //相机拍照或男
            case R.id.rv_photo_paizhao:
                camera();
                if (menuWindow != null) {
                    menuWindow.dismiss();
                }
                break;
            //相册选择或女
            case R.id.rv_photo_xcxz:
                gallery();
                if (menuWindow != null) {
                    menuWindow.dismiss();
                }
                break;
        }

    }

    /**
     * 右侧文字的点击事件
     */
    @Override
    public void rightTvClick() {
        super.rightTvClick();

        business.updateUserInfo(user_info_name.getText().toString(), user_info_mail.getText().toString(),
                user_info_sex.getText().toString().equals("男") ? "1" : "2", user_info_sign.getText().toString(),
                user_info_phone.getText().toString(), this, new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);

                        if (isSuccess(response)) {

                            Toast.makeText(UserInfoActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                            Config.Config(UserInfoActivity.this).setUserName(user_info_name.getText().toString());
                            Config.Config(UserInfoActivity.this).setUserSex(user_info_sex.getText().toString().equals("男") ? "1" : "2");
                            Config.Config(UserInfoActivity.this).setUserPhoneNumber(user_info_phone.getText().toString());
                            Config.Config(UserInfoActivity.this).setUserMail(user_info_mail.getText().toString());
                            Config.Config(UserInfoActivity.this).setUserSign(user_info_sign.getText().toString());

                        } else {

                            Toast.makeText(UserInfoActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                    }
                });
    }

    @Event(R.id.user_info_sex_lay)
    private void sexLay(View view) {

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
            popupWindow = null;
        } else {
            getPopupWindow();
            popupWindow.showAsDropDown(findViewById(R.id.iamgeview));
        }
    }

    /**
     * 获取PopupWindow实例
     */
    private void getPopupWindow() {
        if (null != popupWindow) {
            popupWindow.dismiss();
            return;
        } else {
            initPopuptWindow();
        }
    }

    /**
     * 创建PopupWindow
     */
    protected void initPopuptWindow() {
        // TODO Auto-generated method stub
        // 获取自定义布局文件activity_popupwindow_left.xml的视图
        View popupWindow_view = getLayoutInflater().inflate(R.layout.sex_popwindow, null, false);
        // 创建PopupWindow实例,200,LayoutParams.MATCH_PARENT分别是宽度和高度
        popupWindow = new PopupWindow(popupWindow_view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);

        // 背景不能为空，为空下面代码无效
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //设置焦点
        popupWindow.setFocusable(true);
        //设置点击其他地方 就消失
        popupWindow.setOutsideTouchable(true);

        RadioGroup radioGroup = (RadioGroup) popupWindow_view.findViewById(R.id.radio_group);
        final RadioButton radioButton1 = (RadioButton) popupWindow_view.findViewById(R.id.radiobutton1);
        final RadioButton radioButton2 = (RadioButton) popupWindow_view.findViewById(R.id.radiobutton2);

        if (user_info_sex.getText().toString().equals("女")) {
            radioGroup.check(radioButton2.getId());
        } else if (user_info_sex.getText().toString().equals("男")) {
            radioGroup.check(radioButton1.getId());
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == radioButton1.getId()) {
                    popupWindow.dismiss();
                    user_info_sex.setText("男");

                } else if (checkedId == radioButton2.getId()) {
                    popupWindow.dismiss();
                    user_info_sex.setText("女");
                }

            }
        });

    }

    class PersonInformaton implements PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            backgroundAlpha(1f);
        }
    }

    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; // 0.0-1.0
        getWindow().setAttributes(lp);
    }

    @Event(R.id.user_info_ico)
    private void setUpLoadIco(View view) {

        menuWindow.setBackgroundDrawable(new BitmapDrawable());
        menuWindow.setText("拍照", "从相册中选取");
        menuWindow.update();
        menuWindow.showAtLocation(this.findViewById(R.id.root_view),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        backgroundAlpha(0.5f);

    }

    /**
     * 从相机获取
     */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }


    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * 从相册获取
	 */
    public void gallery() {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }

    /**
     * 剪切图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */

    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    public void saveFile(Bitmap bm, String fileName) throws IOException {
        File dirFile = new File(CommonTool.PROJECT_IMAGE_PATH);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        File myCaptureFile = new File(CommonTool.PROJECT_IMAGE_PATH + fileName);

        if (myCaptureFile.exists()) {
            myCaptureFile.delete();
        }

        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
        bos.flush();
        bos.close();
    }


    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PHOTO_REQUEST_GALLERY) {
            if (data != null) {
                // 得到图片的全路径
                Uri uri = data.getData();
                crop(uri);
                str_image = uri.toString();
            }

        } else if (requestCode == PHOTO_REQUEST_CAMERA && resultCode != 0) {
            if (hasSdcard()) {
                tempFile = new File(Environment.getExternalStorageDirectory(),
                        PHOTO_FILE_NAME);
                str_image = Uri.fromFile(tempFile).toString();
                crop(Uri.fromFile(tempFile));
            } else {
                toastMsg("未找到存储卡，无法存储照片！");
            }

        } else if (requestCode == PHOTO_REQUEST_CUT && resultCode != 0) {
            try {
                WindowManager.LayoutParams lp = this.getWindow()
                        .getAttributes();
                lp.alpha = 1f;
                this.getWindow().setAttributes(lp);

                bitmap = data.getParcelableExtra("data");
                saveFile(bitmap, "user_ico.jpg");

                ImageLoader.getInstance().displayImage("file://" + CommonTool.PROJECT_IMAGE_PATH + "user_ico.jpg", user_info_ico_bg, MyApplication.imageOptionCircleLocal);

                String imageBase64 = PictureUtil.bitmapToString(PictureUtil.getBitmap(CommonTool.PROJECT_IMAGE_PATH + "user_ico.jpg"));

                business.updateUserIco(imageBase64, UserInfoActivity.this, new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);
                        if (isSuccess(response)) {
                            String imgUrl = FastJsonUtil.getNoteJson(response.getBody().toString(), "userUrl");
                            Config.Config(UserInfoActivity.this).setUserUrl(imgUrl);
                            Toast.makeText(UserInfoActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UserInfoActivity.this, "很抱歉，出错啦", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                        Toast.makeText(UserInfoActivity.this, "很抱歉，出错啦", Toast.LENGTH_SHORT).show();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
