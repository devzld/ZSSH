package com.zy.zssh.m_me.bean;

/**
 * Created by Administrator on 2016/5/30 0030.
 */
public class UserInfoBean {


    /**
     * rester : 1
     * userId : 10135
     * userUrl : http://app.shzywh.cn/images/head.jpg
     * loginName : 11558349
     * userMail :
     * sex : 0
     * qian : 无
     * userSate : 0
     */

    private String rester;
    private String userId;
    private String userUrl;
    private String loginName;
    private String userMail;
    private String sex;
    private String qian;
    private String userSate;

    public String getRester() {
        return rester;
    }

    public void setRester(String rester) {
        this.rester = rester;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getQian() {
        return qian;
    }

    public void setQian(String qian) {
        this.qian = qian;
    }

    public String getUserSate() {
        return userSate;
    }

    public void setUserSate(String userSate) {
        this.userSate = userSate;
    }
}
