package com.zy.zssh.m_me.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.business.MeBusiness;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;
import com.zy.zssh.m_video.activity.VideoDetailActivity;
import com.zy.zssh.m_video.adapter.VideoTabAdapter;
import com.zy.zssh.m_video.bean.VideoListBean;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/21 0021.
 * 视频收藏页面
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_video_tab)
public class CollectVideoFragment extends BaseFragment {

    private Map<String, String> collectMap;

    @ViewInject(R.id.video_tab_swipeRefreshLayout)
    private SwipyRefreshLayout video_tab_swipeRefreshLayout;

    @ViewInject(R.id.video_tab_recycler_view)
    private RecyclerView video_tab_recycler_view;

    private VideoTabAdapter videoTabAdapter;

    private MeBusiness business;

    private List<VideoListBean> videoListBeanList = new ArrayList<>();



    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    public CollectVideoFragment(Map<String, String> collectMap) {
        this.collectMap = collectMap;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        business = MeBusiness.getInstance();

        if (videoListBeanList.size() == 0) {

            video_tab_swipeRefreshLayout.setRefreshing(true);
            initData();
        }

        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        video_tab_recycler_view.setHasFixedSize(true);
        video_tab_recycler_view.setItemAnimator(new DefaultItemAnimator());

        video_tab_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));

        videoTabAdapter = new VideoTabAdapter(getActivity(), videoListBeanList);

        video_tab_recycler_view.setAdapter(videoTabAdapter);

        video_tab_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        video_tab_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    videoListBeanList.clear();
                    initData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }

            }
        });

        video_tab_recycler_view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == videoTabAdapter.getItemCount()
                        && !isLoading && video_tab_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    video_tab_swipeRefreshLayout.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        videoTabAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();
                bundle.putString("vidId", videoListBeanList.get(position).getVidId());

                openActivity(VideoDetailActivity.class, bundle);
            }
        });

    }

    /**
     * 初始化list数据
     */
    private void initData() {

        business.getCollectData("1", "1", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(response.getBody().toString(), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.clear();
                        videoListBeanList.addAll(aa);
                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                video_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getCollectData(pageNo + "", "1", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(response.getBody().toString(), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.addAll(aa);

                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
