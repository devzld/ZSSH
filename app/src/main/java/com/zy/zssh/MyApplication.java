package com.zy.zssh;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.baidu.mapapi.SDKInitializer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.umeng.message.PushAgent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.media.WBShareCallBackActivity;

import org.xutils.x;


/**
 * Created by hanji on 16/3/29.
 * <p>
 * describe:
 */
public class MyApplication extends Application {

    //设置频道前几位不可移动
    public static int index_no_move = 2;
    private static Context context;

    /**
     * 圆角头像的配置
     */
    public static DisplayImageOptions imageOptionCircle;

    /**
     * 圆角头像的配置，不带缓存
     */
    public static DisplayImageOptions imageOptionCircleLocal;

    /**
     * 普通图片加载的配置
     */
    public static DisplayImageOptions imageOptionNormal;
    public static DisplayImageOptions imageOptionNormal15;
    public static DisplayImageOptions imageOptionNormal5;
    public static DisplayImageOptions imageOptionNormalLocal;
    public static DisplayImageOptions imageOptionLoadingNetFile;


    /**
     * 获取上下文
     *
     * @return context
     */
    public static Context getContext() {
        if (context != null) {
            return context;
        }
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        x.Ext.init(this);

        setUniversalImage();

        //百度地图的初始化
        SDKInitializer.initialize(getApplicationContext());

//        Vitamio.isInitialized(getApplicationContext());



        //初始化友盟
        initPlatform();




    }

    //初始化友盟第三方服务
    private void initPlatform() {


        /**
         * debug配置信息：第三方登陆
         *
         */
        //第三方登陆
        PlatformConfig.setWeixin("wx944311d1154867c4", "21d8738d9b29d7d92a5068176de4bb6d");
        //微信 appid appsecret
//        PlatformConfig.setSinaWeibo("904778553","650b510ac237ca88197493a74c85c5c6");
//        //新浪微博 appkey appsecret

        PlatformConfig.setSinaWeibo("2572718122","982b88c9e4c6e7ffd95ca64158ee0d15");

        PlatformConfig.setQQZone("1105502879", "atYwi3xPMCDnvWvb");
        // QQ和Qzone appid appkey

        //推送服务
        PushAgent mPushAgent = PushAgent.getInstance(context);
        mPushAgent.enable();
        PushAgent.getInstance(context).onAppStart();
        //设置新浪微博的回调页
        Config.REDIRECT_URL = "http://sns.whalecloud.com/sina2/callback";




    }


    /**
     * 配置UniversalImageLoader
     */
    private void setUniversalImage() {


        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));



        imageOptionLoadingNetFile = new DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.ic_error)
                .showImageOnFail(R.mipmap.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(100))
                .build();
        imageOptionCircle = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.personal_touxiang) //设置图片在下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.personal_touxiang)//设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.personal_touxiang)  //设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
                .displayer(new RoundedBitmapDisplayer(180))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成

        imageOptionCircleLocal = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.personal_touxiang) //设置图片在下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.personal_touxiang)//设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.personal_touxiang)  //设置图片加载/解码过程中错误时候显示的图片
//                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
                .displayer(new RoundedBitmapDisplayer(180))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成

        imageOptionNormal = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.default_image) //设置图片在下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)//设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)  //设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
//                .displayer(new RoundedBitmapDisplayer(180))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成
        imageOptionNormal5= new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.default_image) //设置图片在下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)//设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)  //设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
                .displayer(new RoundedBitmapDisplayer(5))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成


        imageOptionNormal15 = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.default_image) //设置图片在下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)//设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)  //设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
                .displayer(new RoundedBitmapDisplayer(15))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成

        imageOptionNormalLocal = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.default_image) //设置图片在下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)//设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)  //设置图片加载/解码过程中错误时候显示的图片
//                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
//				.decodingOptions(android.graphics.BitmapFactory.Options decodingOptions)//设置图片的解码配置
//.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
//设置图片加入缓存前，对bitmap进行设置
//.preProcessor(BitmapProcessor preProcessor)
                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
//                .displayer(new RoundedBitmapDisplayer(180))//是否设置为圆角，弧度为多少
//				.displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
                .build();//构建完成







    }

    /**
     * Created by wangfei on 15/12/3.
     */
    public static class WBShareActivity extends WBShareCallBackActivity {
    }
}
