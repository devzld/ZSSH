package com.zy.zssh.m_video.bean;

/**
 * Created by Administrator on 2016/6/14 0014.
 * 视频列表数据bean
 */
public class VideoListBean {


    /**
     * vidId : 1
     * vidName : 小张测试视频1
     * vidUrl : app.shzywh.cn/app/video/123.mp4
     * SunCom : 2
     * addTime : 2016-6-2 8:39:27
     * vidWriter : 小张在线
     * vidZan : 0
     * vidMain : 视频简介
     * vidUpzan : 0
     * zansate : 0
     * colsate : 0
     * url : http://app.shzywh.cn/app/images/videourl.png
     * time : 05:47
     */

    private String vidId;
    private String vidName;
    private String vidUrl;
    private String SunCom;
    private String addTime;
    private String vidWriter;
    private String vidZan;
    private String vidMain;
    private String vidUpzan;
    private String zansate;
    private String colsate;
    private String url;
    private String time;

    public String getVidId() {
        return vidId;
    }

    public void setVidId(String vidId) {
        this.vidId = vidId;
    }

    public String getVidName() {
        return vidName;
    }

    public void setVidName(String vidName) {
        this.vidName = vidName;
    }

    public String getVidUrl() {
        return vidUrl;
    }

    public void setVidUrl(String vidUrl) {
        this.vidUrl = vidUrl;
    }

    public String getSunCom() {
        return SunCom;
    }

    public void setSunCom(String SunCom) {
        this.SunCom = SunCom;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getVidWriter() {
        return vidWriter;
    }

    public void setVidWriter(String vidWriter) {
        this.vidWriter = vidWriter;
    }

    public String getVidZan() {
        return vidZan;
    }

    public void setVidZan(String vidZan) {
        this.vidZan = vidZan;
    }

    public String getVidMain() {
        return vidMain;
    }

    public void setVidMain(String vidMain) {
        this.vidMain = vidMain;
    }

    public String getVidUpzan() {
        return vidUpzan;
    }

    public void setVidUpzan(String vidUpzan) {
        this.vidUpzan = vidUpzan;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }

    public String getColsate() {
        return colsate;
    }

    public void setColsate(String colsate) {
        this.colsate = colsate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
