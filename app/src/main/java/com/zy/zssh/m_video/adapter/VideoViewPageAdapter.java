package com.zy.zssh.m_video.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.zy.zssh.m_video.bean.VideoClassifyBean;
import com.zy.zssh.m_video.fragment.VideoTabFragment;

import java.util.List;

/**
 * Created by Administrator on 2016/6/14 0014.
 * 视频页面viewpage的adapter
 */
public class VideoViewPageAdapter extends FragmentPagerAdapter {

    private List<VideoClassifyBean> videoClassifyBeanList;

    private String TAG = "VideoViewPageAdapter";

    public VideoViewPageAdapter(FragmentManager fm, List<VideoClassifyBean> videoClassifyBeanList) {
        super(fm);
        this.videoClassifyBeanList = videoClassifyBeanList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return videoClassifyBeanList.get(position).getClassName();
    }

    @Override
    public int getCount() {
        return videoClassifyBeanList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public Fragment getItem(int position) {

        return new VideoTabFragment(videoClassifyBeanList.get(position));
    }

    //设置每个页签唯一id防止不调用getitem
    @Override
    public long getItemId(int position) {
        // 获取当前数据的hashCode
        return videoClassifyBeanList.get(position).getClassName().hashCode();
    }
}