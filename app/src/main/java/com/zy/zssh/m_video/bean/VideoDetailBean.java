package com.zy.zssh.m_video.bean;

/**
 * Created by Administrator on 2016/6/14 0014.
 * 视频详情bean
 */
public class VideoDetailBean {


    /**
     * vidName : 小张测试视频1
     * vidUrl : app.shzywh.cn/app/video/123.mp4
     * vidUpzan : 0
     * vidZan : 0
     * vidHost : 3
     * vidMain : 视频简介
     * zansate : 0
     * colsate : 0
     */

    private String vidName;
    private String vidUrl;
    private String vidUpzan;
    private String vidZan;
    private String vidHost;
    private String vidMain;
    private String zansate;
    private String colsate;

    public String getVidName() {
        return vidName;
    }

    public void setVidName(String vidName) {
        this.vidName = vidName;
    }

    public String getVidUrl() {
        return vidUrl;
    }

    public void setVidUrl(String vidUrl) {
        this.vidUrl = vidUrl;
    }

    public String getVidUpzan() {
        return vidUpzan;
    }

    public void setVidUpzan(String vidUpzan) {
        this.vidUpzan = vidUpzan;
    }

    public String getVidZan() {
        return vidZan;
    }

    public void setVidZan(String vidZan) {
        this.vidZan = vidZan;
    }

    public String getVidHost() {
        return vidHost;
    }

    public void setVidHost(String vidHost) {
        this.vidHost = vidHost;
    }

    public String getVidMain() {
        return vidMain;
    }

    public void setVidMain(String vidMain) {
        this.vidMain = vidMain;
    }

    public String getZansate() {
        return zansate;
    }

    public void setZansate(String zansate) {
        this.zansate = zansate;
    }

    public String getColsate() {
        return colsate;
    }

    public void setColsate(String colsate) {
        this.colsate = colsate;
    }
}
