package com.zy.zssh.m_video;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.volley.VolleyError;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_video.activity.VideoSearchActivity;
import com.zy.zssh.m_video.adapter.VideoViewPageAdapter;
import com.zy.zssh.m_video.bean.VideoClassifyBean;
import com.zy.zssh.m_video.business.VideoBusiness;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanji on 16/3/29.
 * <p/>
 * describe:视频fragment页面
 */
@ContentView(R.layout.fg_video)
public class VideoFragment extends BaseFragment {

    private String TAG = "VideoFragment";

    @ViewInject(R.id.video_tablayout)
    private TabLayout video_tablayout;

    @ViewInject(R.id.video_view_pager)
    private ViewPager video_view_pager;

    private VideoViewPageAdapter videoViewPageAdapter;

    private List<VideoClassifyBean> videoClassifyBeanList = new ArrayList<>();

    private VideoBusiness business;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        business = VideoBusiness.getInstance();

        videoViewPageAdapter = new VideoViewPageAdapter(getActivity().getSupportFragmentManager(), videoClassifyBeanList);

        video_view_pager.setAdapter(videoViewPageAdapter);

        video_tablayout.setupWithViewPager(video_view_pager);
        video_tablayout.setTabTextColors(getResources().getColor(R.color.select_text), Color.WHITE);

        getVideoClassifyData();
    }

    private void getVideoClassifyData() {

        business.getVideoClassifyData(getActivity(), new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    videoClassifyBeanList.addAll(FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "newslist"), VideoClassifyBean.class));

                    videoViewPageAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });
    }

    /**
     * 视频页面搜索框的响应
     *
     * @param view
     */
    @Event(R.id.video_search_channel)
    private void VideoSearchChannel(View view) {

        openActivity(VideoSearchActivity.class);
    }
}
