package com.zy.zssh.m_video.business;

import android.content.Context;

import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hanj
 */
public class VideoBusiness {
    private static VideoBusiness instance = new VideoBusiness();

    private VideoBusiness() {
    }

    public static synchronized VideoBusiness getInstance() {

        return instance;
    }

    /**
     * 获取视频分类
     *
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getVideoClassifyData(Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "videoclass" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取视频列表数据
     *
     * @param page     页数 第一页传递1
     * @param classId  分类编号 无为“”
     * @param key      搜索关键词 无为“”
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getVideoListData(String page, String classId, String key, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "videolist" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("classId", classId);
        params.put("key", key);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }


    /**
     * 视频模块详情数据接口
     *
     * @param vidId    视频编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getVideoDetailData(String vidId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "videode" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("vidId", vidId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 视频评论展示数据接口
     *
     * @param vidId    视频编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void getVideoCommentData(String page, String vidId, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "videocom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("vidId", vidId);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 视频添加评论
     * <p/>
     * =新闻编号&userId=评论者用户编号&=评论内容&=被评论的评论ID
     *
     * @param vidId    视频编号
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void addVideoCommentData(String vidId, String comMain, String comId2, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "addvideoCom" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("vidId", vidId);
        params.put("comMain", comMain);
        params.put("comId2", comId2);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

    /**
     * 获取轮播图数据
     *
     * @param type     类型，新闻视频等
     * @param id       id
     * @param context  上下文环境
     * @param callback 回调callback
     */
    public void setzanData(String type, String id, Context context, VolleyCallback callback) {

        String url = VolleyClient.SERVER_IP_ADDRESS + "zan" + VolleyClient.SERVER_IP_FOOT;

        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("id", id);
        params.put("userId", Config.Config(context).getUserId());

        VolleyClient.getInstance(context).requsetOfPost(url, params, callback);
    }

}


