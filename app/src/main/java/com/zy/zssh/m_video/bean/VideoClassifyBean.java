package com.zy.zssh.m_video.bean;

/**
 * Created by Administrator on 2016/6/14 0014.
 * 视频分类数据bean
 */
public class VideoClassifyBean {

    /**
     * classId : 1
     * className : 红木TV
     */

    private String classId;
    private String className;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
