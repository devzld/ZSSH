package com.zy.zssh.m_video.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_video.bean.VideoDetailBean;

/**
 * Created by devin on 16/4/27.
 */
public abstract class VideoCommentBaseRecycleAdapter extends BaseRecycleAdapter {

    protected enum ITEM_TYPE {
        HEAD, CONTENT
    }

    protected VideoDetailBean videoDetailBean;

    public VideoCommentBaseRecycleAdapter(Context mContext, VideoDetailBean videoDetailBean) {
        super(mContext);
        this.videoDetailBean = videoDetailBean;
    }

    public void setNewsDetailBean(VideoDetailBean videoDetailBean) {
        this.videoDetailBean = videoDetailBean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE.HEAD.ordinal() && videoDetailBean != null) {

            return onCreateHeadViewHolder(parent);
        } else if (viewType == ITEM_TYPE.CONTENT.ordinal()) {

            return onCreateContentViewHolder(parent);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 && videoDetailBean != null) {

            return ITEM_TYPE.HEAD.ordinal();
        } else {

            return ITEM_TYPE.CONTENT.ordinal();
        }
    }

    public abstract RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent);

    public abstract RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent);
}
