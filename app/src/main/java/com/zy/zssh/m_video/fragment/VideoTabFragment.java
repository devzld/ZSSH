package com.zy.zssh.m_video.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.view.RecyclerViewDivider;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;
import com.zy.zssh.m_video.activity.VideoDetailActivity;
import com.zy.zssh.m_video.adapter.VideoTabAdapter;
import com.zy.zssh.m_video.bean.VideoClassifyBean;
import com.zy.zssh.m_video.bean.VideoListBean;
import com.zy.zssh.m_video.business.VideoBusiness;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/14 0014.
 * 分类视频页面
 */
@SuppressLint("ValidFragment")
@ContentView(R.layout.fg_video_tab)
public class VideoTabFragment extends BaseFragment {

    private VideoClassifyBean videoClassifyBean;

    @ViewInject(R.id.video_tab_swipeRefreshLayout)
    private SwipyRefreshLayout video_tab_swipeRefreshLayout;

    @ViewInject(R.id.video_tab_recycler_view)
    private RecyclerView video_tab_recycler_view;

    private VideoTabAdapter videoTabAdapter;

    private VideoBusiness business;

    private List<VideoListBean> videoListBeanList = new ArrayList<>();

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    public VideoTabFragment(VideoClassifyBean videoClassifyBean) {
        super();
        this.videoClassifyBean = videoClassifyBean;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        business = VideoBusiness.getInstance();

        if (videoListBeanList.size() == 0) {

            video_tab_swipeRefreshLayout.setRefreshing(true);
            initData();
        }

        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        video_tab_recycler_view.setHasFixedSize(true);
        video_tab_recycler_view.setItemAnimator(new DefaultItemAnimator());

        video_tab_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));

        //设置分割线
        video_tab_recycler_view.addItemDecoration(new RecyclerViewDivider(getContext(),LinearLayoutManager.VERTICAL));



        videoTabAdapter = new VideoTabAdapter(getActivity(), videoListBeanList);

        video_tab_recycler_view.setAdapter(videoTabAdapter);

        video_tab_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        video_tab_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(getActivity(), "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    videoListBeanList.clear();
                    initData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }

            }
        });

        video_tab_recycler_view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == videoTabAdapter.getItemCount()
                        && !isLoading && video_tab_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    video_tab_swipeRefreshLayout.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        videoTabAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();
                bundle.putString("vidId", videoListBeanList.get(position).getVidId());
                bundle.putString("vidUrl", videoListBeanList.get(position).getUrl());

                openActivity(VideoDetailActivity.class, bundle);
            }
        });

    }

    /**
     * 初始化list数据
     */
    private void initData() {

        business.getVideoListData("1", videoClassifyBean.getClassId(), "", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.clear();
                        videoListBeanList.addAll(aa);
                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                video_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getVideoListData(pageNo + "", videoClassifyBean.getClassId(), "", mActivity, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.addAll(aa);

                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        video_tab_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                video_tab_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
