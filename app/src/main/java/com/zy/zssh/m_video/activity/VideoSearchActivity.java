package com.zy.zssh.m_video.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_news.adapter.news.NewsBaseRecycleAdapter;
import com.zy.zssh.m_video.adapter.VideoTabAdapter;
import com.zy.zssh.m_video.bean.VideoListBean;
import com.zy.zssh.m_video.business.VideoBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

public class VideoSearchActivity extends BaseActivity {

    @ViewInject(R.id.video_search_edit)
    private EditText video_search_edit;

    @ViewInject(R.id.video_search_recycleview)
    private RecyclerView video_search_recycleview;

    @ViewInject(R.id.video_search_swipeRefreshLayout)
    private SwipyRefreshLayout video_search_swipeRefreshLayout;

    @ViewInject(R.id.leftIcon)
    private ImageView leftIcon;


    private VideoTabAdapter videoTabAdapter;

    private VideoBusiness business;

    private List<VideoListBean> videoListBeanList = new ArrayList<>();

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    private int pageNo = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    private String searchStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentLayout(R.layout.activity_video_search);

        x.view().inject(this);

        initView();
    }

    @Override
    protected void initView() {



        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //视频输入框监听事件
        video_search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }

            @Override
            public void afterTextChanged(Editable s) {

                String input = s.toString().trim();
                if(input.length()>0){

                    leftIcon.setImageResource(R.mipmap.fanhui);
                    leftIcon.setClickable(true);

                }else{

                    //输入内容为空设置为搜索
                    leftIcon.setImageResource(R.mipmap.sousuo);
                    leftIcon.setClickable(false);

                }

            }
        });

        //键盘搜索监听
        video_search_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                searchVideo();

                return true;


            }
        });



        setHidePublicTitle(true);

        business = VideoBusiness.getInstance();

//        if (videoListBeanList.size() == 0) {
//
//            video_search_swipeRefreshLayout.setRefreshing(true);
//            initData();
//        }

        video_search_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        video_search_recycleview.setHasFixedSize(true);
        video_search_recycleview.setItemAnimator(new DefaultItemAnimator());

        video_search_recycleview.setLayoutManager(new LinearLayoutManager(this));

        videoTabAdapter = new VideoTabAdapter(this, videoListBeanList);

        video_search_recycleview.setAdapter(videoTabAdapter);

        video_search_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        video_search_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Toast.makeText(VideoSearchActivity.this, "刷新成功" +
                        (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"), Toast.LENGTH_SHORT).show();

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    videoListBeanList.clear();
                    initData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreData();
                }

            }
        });

        video_search_recycleview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == videoTabAdapter.getItemCount()
                        && !isLoading && video_search_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    video_search_swipeRefreshLayout.setRefreshing(true);
                    loadMoreData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });

        videoTabAdapter.setOnItemClickListener(new NewsBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();
                bundle.putString("vidId", videoListBeanList.get(position).getVidId());

                openActivity(VideoDetailActivity.class, bundle);
            }
        });









    }

    @Override
    protected void initData() {


        business.getVideoListData("1", "", searchStr, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    pageNo = 1;

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.clear();
                        videoListBeanList.addAll(aa);
                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_search_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        video_search_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        pageNo++;
                    }

                }

                video_search_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                video_search_swipeRefreshLayout.setRefreshing(false);
            }
        });


    }


    /**
     * 加载更多list数据
     */
    private void loadMoreData() {

        business.getVideoListData(pageNo + "", "", searchStr, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<VideoListBean> aa = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(
                            response.getBody().toString(), "newslist"), VideoListBean.class);

                    if (aa != null && aa.size() != 0) {

                        videoListBeanList.addAll(aa);

                        videoTabAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 20) {
                        video_search_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        pageNo++;
                        video_search_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }

                isLoading = false;
                video_search_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                isLoading = false;
                video_search_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }



    public void searchVideo(){


        if (video_search_edit.getText().toString().equals("")) {
            Toast.makeText(VideoSearchActivity.this, "搜索内容不能为空", Toast.LENGTH_SHORT).show();
        } else {
            searchStr = video_search_edit.getText().toString();
            initData();

            Tools.hideKeyboard(this);
        }


    }




    @Override
    protected void initClickListener(View v) {

    }
}
