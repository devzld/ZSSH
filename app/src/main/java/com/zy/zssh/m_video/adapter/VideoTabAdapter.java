package com.zy.zssh.m_video.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.m_video.bean.VideoListBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/15 0015.
 * 视频Tab界面的adapter
 */
public class VideoTabAdapter extends BaseRecycleAdapter {

    private List<VideoListBean> videoListBeanList;

    public VideoTabAdapter(Context mContext, List<VideoListBean> videoListBeanList) {
        super(mContext);

        if (videoListBeanList == null) {
            videoListBeanList = new ArrayList<>();
        }

        this.videoListBeanList = videoListBeanList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new VideoTabViewHolder(mLayoutInflater.inflate(R.layout.fg_video_tab_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        VideoListBean videoListBean = getItem(position);

        if (videoListBean != null) {

            ImageLoader.getInstance().displayImage(videoListBean.getUrl(), ((VideoTabViewHolder) holder).video_tab_imageview, MyApplication.imageOptionNormal);

            ((VideoTabViewHolder) holder).video_tab_title.setText(videoListBean.getVidName());

            ((VideoTabViewHolder) holder).video_tab_duration_time.setText(videoListBean.getTime());

            ((VideoTabViewHolder) holder).video_tab_writer.setText(videoListBean.getVidWriter());

            ((VideoTabViewHolder) holder).video_tab_time.setText(videoListBean.getAddTime());

            ((VideoTabViewHolder) holder).video_tab_view_num.setText(videoListBean.getVidUpzan());

            ((VideoTabViewHolder) holder).video_tab_comment_num.setText(videoListBean.getSunCom());
        }

    }

    private VideoListBean getItem(int position) {
        if (videoListBeanList != null) {
            return videoListBeanList.get(position);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return videoListBeanList.size();
    }

    public class VideoTabViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.cv_item)
        private FrameLayout cv_item;

        @ViewInject(R.id.video_tab_imageview)
        private ImageView video_tab_imageview;

        @ViewInject(R.id.video_tab_title)
        private TextView video_tab_title;

        @ViewInject(R.id.video_tab_duration_time)
        private TextView video_tab_duration_time;

        @ViewInject(R.id.video_tab_writer)
        private TextView video_tab_writer;

        @ViewInject(R.id.video_tab_time)
        private TextView video_tab_time;

        @ViewInject(R.id.video_tab_view_num)
        private TextView video_tab_view_num;

        @ViewInject(R.id.video_tab_comment_num)
        private TextView video_tab_comment_num;

        public VideoTabViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null) {

                itemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
