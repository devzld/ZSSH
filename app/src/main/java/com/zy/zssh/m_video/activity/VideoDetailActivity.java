package com.zy.zssh.m_video.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_me.activity.LoginActivity;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;
import com.zy.zssh.m_video.adapter.VideoCommentAdapter;
import com.zy.zssh.m_video.adapter.VideoCommentBaseRecycleAdapter;
import com.zy.zssh.m_video.bean.VideoDetailBean;
import com.zy.zssh.m_video.business.VideoBusiness;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class VideoDetailActivity extends Activity implements View.OnClickListener {

//    @ViewInject(R.id.video_detail_parent)
//    private FrameLayout video_detail_parent;

//    @ViewInject(R.id.video_detail_videoView)
//    private VideoView video_detail_videoView;

    @ViewInject(R.id.video_detail_videoView)
    private JCVideoPlayerStandard video_detail_videoView;

    @ViewInject(R.id.video_detail_swipeRefreshLayout)
    private SwipyRefreshLayout video_detail_swipeRefreshLayout;

    @ViewInject(R.id.video_detail_recycleView)
    private RecyclerView video_detail_recycleView;

    @ViewInject(R.id.video_detail_comment_edit)
    private EditText video_detail_comment_edit;

    @ViewInject(R.id.video_detail_comment_send)
    private ImageView video_detail_comment_send;

//    @ViewInject(R.id.video_detail_comment_num)
//    private ImageView video_detail_comment_num;

    private VideoBusiness business;

    //视频的id
    private String vidId = "";
    //视频的封面
    private String vidUrl = "";

    private VideoDetailBean videoDetailBean;

    private VideoCommentAdapter videoCommentAdapter;

    private List<NewsCommentBean> newsCommentBeanList = new ArrayList<>();

    private int comPage = 1;

    /**
     * 是否正在加载
     */
    private boolean isLoading = false;

    /**
     * 最后一个item项
     */
    private int lastPosition = -1;

    /**
     * 当前缩放模式
     */
//    private int mLayout = VideoView.VIDEO_LAYOUT_ORIGIN;
    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_video_detail);

        x.view().inject(this);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Tools.ScreenSize(this)[1] / 3);

//        video_detail_parent.setLayoutParams(params);

//        mGestureDetector = new GestureDetector(this, new MyGestureListener());

        initData();
    }


//    //视屏播放手势操作
//    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
//
//        /**
//         * 双击
//         */
//        @Override
//        public boolean onDoubleTap(MotionEvent e) {
//            if (mLayout == VideoView.VIDEO_LAYOUT_ORIGIN) {
//
//                mLayout = VideoView.VIDEO_LAYOUT_SCALE;
//
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
////                video_detail_parent.setLayoutParams(params);
//
//                WindowManager.LayoutParams attrs = getWindow().getAttributes();
//                VideoDetailActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
//                getWindow().setAttributes(attrs);
//                //设置全屏
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//
//            } else {
//
//                mLayout = VideoView.VIDEO_LAYOUT_ORIGIN;
//
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Tools.ScreenSize(VideoDetailActivity.this)[0] / 3);
//
////                video_detail_parent.setLayoutParams(params);
//
//                WindowManager.LayoutParams attrs = getWindow().getAttributes();
//                VideoDetailActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                attrs.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                getWindow().setAttributes(attrs);
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//
//            }
//
////            if (video_detail_videoView != null)
////                video_detail_videoView.setVideoLayout(mLayout, 0);
//            return true;
//        }
//    }


    protected void initView() {

        if (videoDetailBean != null) {

            videoCommentAdapter = new VideoCommentAdapter(VideoDetailActivity.this, newsCommentBeanList, videoDetailBean);
            video_detail_recycleView.setAdapter(videoCommentAdapter);
            setListener();
            displayVideo(videoDetailBean.getVidUrl());

            getCommentData();
        }

    }

    protected void initData() {

        vidId = getIntent().getExtras().getString("vidId", "");
        vidUrl = getIntent().getExtras().getString("vidUrl", "");

        business = VideoBusiness.getInstance();

        video_detail_comment_edit.setTag("0");
        //设置隐藏标题栏

        //设置默认颜色灰色
        video_detail_videoView.setBackgroundColor(getResources().getColor(R.color.huise));

        video_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        video_detail_swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.refresh_color));

        video_detail_recycleView.setHasFixedSize(true);
        video_detail_recycleView.setItemAnimator(new DefaultItemAnimator());

        video_detail_recycleView.setLayoutManager(new LinearLayoutManager(this));

        business.getVideoDetailData(vidId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {
                    videoDetailBean = FastJsonUtil.jsonString2Bean(response.getBody().toString(), VideoDetailBean.class);

                    initView();
                }

            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    private void setListener() {


        //滑动刷新事件
        video_detail_swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    getCommentData();

                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

                    loadMoreCommentData();
                }

            }
        });


        //滚动事件
        video_detail_recycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //当前为非滚动RecyclerView.SCROLL_STATE_IDLE
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition + 1 == videoCommentAdapter.getItemCount()
                        && !isLoading && video_detail_swipeRefreshLayout.getDirection() == SwipyRefreshLayoutDirection.BOTH) {

                    isLoading = true;
                    video_detail_swipeRefreshLayout.setRefreshing(true);
                    loadMoreCommentData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            }
        });


        //item中view的点击事件
        videoCommentAdapter.setOnItemClickListener(new VideoCommentBaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {

                switch (view.getId()) {
                    case R.id.cv_item:

                        //弹出输入框
                        video_detail_comment_edit.setText("@" + newsCommentBeanList.get(position).getLoginName() + ":");
                        video_detail_comment_edit.setTag(newsCommentBeanList.get(position).getComId());
                        video_detail_comment_edit.requestFocus();

                        video_detail_comment_edit.setSelection(video_detail_comment_edit.length());

                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                        break;

                    //横排的喜欢按钮
                    case R.id.video_detail_like:

                        view.setSelected(!view.isSelected());
                        view.setEnabled(false);

                        business.setzanData(view.isSelected() ? "video" : "clearvideo", vidId, VideoDetailActivity.this, new VolleyCallback() {
                            @Override
                            public void requestSuccess(VolleyResponse response) {
                                super.requestSuccess(response);

                                if (isSuccess(response)) {
                                    if (view.isSelected()) {
                                        Toast.makeText(VideoDetailActivity.this, "点赞", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(VideoDetailActivity.this, "取消赞", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                view.setEnabled(true);
                            }

                            @Override
                            public void requestError(VolleyError error) {
                                super.requestError(error);

                                view.setEnabled(true);
                            }
                        });

                        break;

                    //横排的搜藏按钮
                    case R.id.video_detail_collect:

                        view.setSelected(!view.isSelected());
                        view.setEnabled(false);

                        business.setzanData(view.isSelected() ? "videocol" : "clearvideocol", vidId, VideoDetailActivity.this, new VolleyCallback() {
                            @Override
                            public void requestSuccess(VolleyResponse response) {
                                super.requestSuccess(response);

                                if (isSuccess(response)) {
                                    if (view.isSelected()) {
                                        Toast.makeText(VideoDetailActivity.this, "收藏", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(VideoDetailActivity.this, "取消收藏", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                view.setEnabled(true);
                            }

                            @Override
                            public void requestError(VolleyError error) {
                                super.requestError(error);

                                view.setEnabled(true);
                            }
                        });
                        break;
                    //评论的喜欢按钮
                    case R.id.news_comment_like:

                        view.setSelected(!view.isSelected());
                        view.setEnabled(false);

                        business.setzanData(view.isSelected() ? "videocom" : "clearhuacom", newsCommentBeanList.get(position).getComId(), VideoDetailActivity.this, new VolleyCallback() {
                            @Override
                            public void requestSuccess(VolleyResponse response) {
                                super.requestSuccess(response);

                                if (isSuccess(response)) {
                                    if (view.isSelected()) {
                                        Toast.makeText(VideoDetailActivity.this, "收藏", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(VideoDetailActivity.this, "取消收藏", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                view.setEnabled(true);
                            }

                            @Override
                            public void requestError(VolleyError error) {
                                super.requestError(error);

                                view.setEnabled(true);
                            }
                        });

                        break;
                }
            }
        });


        video_detail_comment_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                //输入内容为空时隐藏发送按钮显示评论数目按钮
                if (start == 0 && after == 0) {

                    video_detail_comment_send.setVisibility(View.GONE);
//                    video_detail_comment_num.setVisibility(View.VISIBLE);

                    video_detail_comment_edit.setTag("0");
                } else {

                    video_detail_comment_send.setVisibility(View.VISIBLE);
//                    video_detail_comment_num.setVisibility(View.GONE);
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        video_detail_comment_send.setOnClickListener(this);
    }


    /**
     * 添加评论
     */
    private void sendComment() {

        //newsId=新闻编号&userId=评论者用户编号&comMain=评论内容&comId2=被评论的评论ID
        business.addVideoCommentData(vidId,
                video_detail_comment_edit.getText().toString(), video_detail_comment_edit.getTag().toString(), this,
                new VolleyCallback() {
                    @Override
                    public void requestSuccess(VolleyResponse response) {
                        super.requestSuccess(response);

                        if (isSuccess(response)) {
                            Toast.makeText(VideoDetailActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            video_detail_comment_edit.setText("");
                            Tools.hideKeyboard(VideoDetailActivity.this);
                        } else {
                            Toast.makeText(VideoDetailActivity.this, "发布失败", Toast.LENGTH_SHORT).show();
                            Tools.hideKeyboard(VideoDetailActivity.this);
                        }

                        video_detail_comment_edit.setTag("0");
                    }

                    @Override
                    public void requestError(VolleyError error) {
                        super.requestError(error);
                        video_detail_comment_edit.setTag("0");
                    }
                });
    }

    /**
     * 获取视频评论数据
     */
    private void getCommentData() {

        business.getVideoCommentData(comPage + "", vidId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "videocom"), NewsCommentBean.class);

                    if (aa != null) {

                        if (newsCommentBeanList.size() != 0) {

                            newsCommentBeanList.clear();
                        }

                        newsCommentBeanList.addAll(aa);

                        videoCommentAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 10) {
                        video_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        video_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                        comPage++;
                    }

                } else {
                    Toast.makeText(VideoDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }

                video_detail_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                video_detail_swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 加载更多视频评论数据
     */
    private void loadMoreCommentData() {

        business.getVideoCommentData(comPage + "", vidId, this, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                if (isSuccess(response)) {

                    List<NewsCommentBean> aa = FastJsonUtil.jsonString2Beans(
                            FastJsonUtil.getNoteJson(response.getBody().toString(), "videocom"), NewsCommentBean.class);

                    if (aa != null) {

                        newsCommentBeanList.addAll(aa);

                        videoCommentAdapter.notifyDataSetChanged();
                    }

                    //不足一页禁止下拉加载，满足一页开启加载更多
                    if (aa != null && aa.size() != 10) {
                        video_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    } else {
                        comPage++;
                        video_detail_swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }

                } else {
                    Toast.makeText(VideoDetailActivity.this, "评论列表加载错误", Toast.LENGTH_SHORT).show();
                }

                isLoading = false;
                video_detail_swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);

                isLoading = false;
                video_detail_swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    //设置视频初始化数据
    private void displayVideo(final String videoUri) {



        video_detail_videoView.setUp(videoUri,"");
        video_detail_videoView.thumbImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        ImageLoader.getInstance().displayImage(vidUrl,video_detail_videoView.thumbImageView, MyApplication.imageOptionNormal);


//        if (Vitamio.isInitialized(this)) {
//
//            video_detail_videoView.setVideoURI(Uri.parse(videoUri));
//            MediaController controller = new MediaController(this, true, video_detail_parent);
//            controller.setVisibility(View.INVISIBLE);
//            video_detail_videoView.setMediaController(controller);
//
//            ImageLoader.getInstance().loadImage(vidUrl, new ImageLoadingListener() {
//                @Override
//                public void onLoadingStarted(String imageUri, View view) {
//
//                }
//
//                @Override
//                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//
//                }
//
//                @Override
//                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//
//                    video_detail_videoView.setBackgroundDrawable(PictureUtil.bitmap2Drawable(loadedImage));
//                }
//
//                @Override
//                public void onLoadingCancelled(String imageUri, View view) {
//
//                }
//            });
//
//            video_detail_videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                @Override
//                public boolean onInfo(MediaPlayer mp, int what, int extra) {
//
//                    switch (what) {
//                        //开始缓冲
//                        case MediaPlayer.MEDIA_INFO_BUFFERING_START:
//                            System.out.println("开始缓冲" + video_detail_videoView.isPlaying());
//                            break;
//                        //缓冲结束
//                        case MediaPlayer.MEDIA_INFO_BUFFERING_END:
//                            System.out.println("缓冲结束");
//                            if (video_detail_videoView.isPlaying()) {
//                                video_detail_videoView.setBackgroundColor(getResources().getColor(R.color.cancel));
//                            }
//                            break;
//                        //正在缓冲
//                        case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
//                            System.out.println("当前网速:" + extra + "kb/s");
//                            break;
//                    }
//                    return true;
//                }
//            });
//
//            video_detail_videoView.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
//                @Override
//                public void onBufferingUpdate(MediaPlayer mp, int percent) {
//                    System.out.println("缓冲：" + percent + "%");
//                }
//            });
//        }





    }

//    /**
//     * 开始
//     *
//     * @param view
//     */
//    @Event(R.id.video_detail_start)
//    private void start(View view) {
//
////        video_detail_videoView.start();
//        video_detail_videoView.setBackgroundColor(getResources().getColor(R.color.cancel));
//        view.setVisibility(View.GONE);
//    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        if (mGestureDetector.onTouchEvent(event))
//            return true;
//        return super.onTouchEvent(event);
//
//
//    }

    //发送评论
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.video_detail_comment_send:

                if (Config.Config(VideoDetailActivity.this).getUserId().equals("")) {

                    Toast.makeText(VideoDetailActivity.this, "登录后才能评论", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(VideoDetailActivity.this, LoginActivity.class));
                } else {

                    sendComment();
                }

                break;
        }
    }
}
