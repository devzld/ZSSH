package com.zy.zssh.m_video.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.m_news.bean.news.NewsCommentBean;
import com.zy.zssh.m_video.bean.VideoDetailBean;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/3 0003.
 * 新闻评论列表的adapter
 */
public class VideoCommentAdapter extends VideoCommentBaseRecycleAdapter {

    private List<NewsCommentBean> newsCommentBeanList;

    private VideoDetailBean videoDetailBean;

    public VideoCommentAdapter(Context mContext, List<NewsCommentBean> newsCommentBeanList, VideoDetailBean videoDetailBean) {
        super(mContext, videoDetailBean);

        this.newsCommentBeanList = newsCommentBeanList;
        this.videoDetailBean = videoDetailBean;

        if (newsCommentBeanList == null) {
            this.newsCommentBeanList = new ArrayList<>();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //头部事件的处理
        if (holder instanceof VideoCommentHeadViewHolder && videoDetailBean != null) {

            ((VideoCommentHeadViewHolder) holder).video_detail_detail.setText(videoDetailBean.getVidMain());

            ((VideoCommentHeadViewHolder) holder).video_detail_play_num.setText(videoDetailBean.getVidHost());

            ((VideoCommentHeadViewHolder) holder).video_detail_like_num.setText(videoDetailBean.getVidZan());

            ((VideoCommentHeadViewHolder) holder).video_detail_dispraise_num.setText(videoDetailBean.getVidUpzan());
            ((VideoCommentHeadViewHolder) holder).video_detail_like.setSelected(videoDetailBean.getZansate().equals("1"));
            ((VideoCommentHeadViewHolder) holder).video_detail_collect.setSelected(videoDetailBean.getColsate().equals("1"));

        } else if (holder instanceof VideoCommentViewHolder) {

            ImageLoader.getInstance().displayImage(getItem(position).getUserUrl(),
                    ((VideoCommentViewHolder) holder).news_comment_user_ico, MyApplication.imageOptionCircle);

            ((VideoCommentViewHolder) holder).news_comment_user_name.setText(getItem(position).getLoginName());

            ((VideoCommentViewHolder) holder).news_comment_user_time.setText(getItem(position).getAddTime());

            ((VideoCommentViewHolder) holder).news_comment_content.setText(getItem(position).getComMain());

            ((VideoCommentViewHolder) holder).news_comment_like.setSelected(getItem(position).getZansate().equals("1"));

            if ("0".equals(getItem(position).getUserID2())) {
                ((VideoCommentViewHolder) holder).news_comment_reply.setVisibility(View.GONE);
            } else {

                try {
                    ((VideoCommentViewHolder) holder).news_comment_reply.setVisibility(View.VISIBLE);

                    ImageLoader.getInstance().displayImage(getItem(position).getUserUrL2(),
                            ((VideoCommentViewHolder) holder).news_comment_user_ico1, MyApplication.imageOptionCircle);

                    ((VideoCommentViewHolder) holder).news_comment_user_name1.setText(getItem(position).getLoginName2());

                    ((VideoCommentViewHolder) holder).news_comment_user_time1.setText(getItem(position).getAddTime());

                    ((VideoCommentViewHolder) holder).news_comment_content1.setText(getItem(position).getComMain2());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public NewsCommentBean getItem(int position) {
        return newsCommentBeanList.get(videoDetailBean != null ? position - 1 : position);
    }

    @Override
    public int getItemCount() {
        return videoDetailBean != null ? newsCommentBeanList.size() + 1 : newsCommentBeanList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeadViewHolder(ViewGroup parent) {
        return new VideoCommentHeadViewHolder(mLayoutInflater.inflate(R.layout.video_detail_comment_item_head, parent, false));
    }

    @Override
    public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
        return new VideoCommentViewHolder(mLayoutInflater.inflate(R.layout.fg_news_comment_item, parent, false));
    }

    public class VideoCommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.news_comment_user_ico)
        private ImageView news_comment_user_ico;

        @ViewInject(R.id.news_comment_user_name)
        private TextView news_comment_user_name;

        @ViewInject(R.id.news_comment_user_time)
        private TextView news_comment_user_time;

        @ViewInject(R.id.news_comment_content)
        private TextView news_comment_content;

        @ViewInject(R.id.news_comment_like)
        private ImageView news_comment_like;

        @ViewInject(R.id.news_comment_reply)
        private View news_comment_reply;

        @ViewInject(R.id.news_comment_user_ico1)
        private ImageView news_comment_user_ico1;

        @ViewInject(R.id.news_comment_user_name1)
        private TextView news_comment_user_name1;

        @ViewInject(R.id.news_comment_user_time1)
        private TextView news_comment_user_time1;

        @ViewInject(R.id.news_comment_content1)
        private TextView news_comment_content1;

        @ViewInject(R.id.cv_item)
        private LinearLayout cv_item;

        public VideoCommentViewHolder(View itemView) {
            super(itemView);
            x.view().inject(this, itemView);

            cv_item.setOnClickListener(this);
            news_comment_like.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null)
                itemClickListener.onItemClick(v, videoDetailBean != null ? getPosition() - 1 : getPosition());
        }
    }

    public class VideoCommentHeadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @ViewInject(R.id.video_detail_detail)
        private TextView video_detail_detail;

        @ViewInject(R.id.video_detail_play_num)
        private TextView video_detail_play_num;

        @ViewInject(R.id.video_detail_collect)
        private ImageView video_detail_collect;

        @ViewInject(R.id.video_detail_collect_num)
        private TextView video_detail_collect_num;

        @ViewInject(R.id.video_detail_like)
        private ImageView video_detail_like;

        @ViewInject(R.id.video_detail_like_num)
        private TextView video_detail_like_num;

        @ViewInject(R.id.video_detail_dispraise_num)
        private TextView video_detail_dispraise_num;

        public VideoCommentHeadViewHolder(View itemView) {
            super(itemView);

            x.view().inject(this, itemView);

            video_detail_like.setOnClickListener(this);
            video_detail_collect.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if (itemClickListener != null)
                itemClickListener.onItemClick(v, videoDetailBean != null ? getPosition() - 1 : getPosition());
        }
    }
}
