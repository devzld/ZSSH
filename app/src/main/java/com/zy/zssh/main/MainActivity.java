package com.zy.zssh.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RadioButton;

import com.zy.zssh.R;
import com.zy.zssh.common.activity.BaseActivity;
import com.zy.zssh.m_book.BookFragment;
import com.zy.zssh.m_gambit.GambitFragment;
import com.zy.zssh.m_me.MeFragment;
import com.zy.zssh.m_news.NewsFragment;
import com.zy.zssh.m_video.VideoFragment;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * desc:首页的activity
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011 9:19
**/
public class MainActivity extends BaseActivity {


    // tab
    public static final int TAB_NEWS = 1;
    public static final int TAB_VIDEO = 2;
    public static final int TAB_ATTENTION = 3;
    public static final int TAB_ME = 4;
    public static final int TAB_BOOK = 5;
    //tab对应的tag
    private static final String FRAGMENT_TAG_NEWS = "news";
    private static final String FRAGMENT_TAG_VIDEO = "video";
    private static final String FRAGMENT_TAG_ATTENTION = "attention";
    private static final String FRAGMENT_TAG_ME = "me";
    private static final String FRAGMENT_TAG_BOOK = "book";

    /**
     * 记录当前所在Tab页的索引(默认为新闻)
     */
    public int mCurrentTab = TAB_NEWS;

    @ViewInject(R.id.rbn_main_bottom_news)
    private RadioButton rbn_main_bottom_news;

    @ViewInject(R.id.rbn_main_bottom_video)
    private RadioButton rbn_main_bottom_video;

    @ViewInject(R.id.rbn_main_bottom_attention)
    private RadioButton rbn_main_bottom_attention;

    @ViewInject(R.id.rbn_main_bottom_me)
    private RadioButton rbn_main_bottom_me;

    @ViewInject(R.id.rbn_main_bottom_book)
    private RadioButton rbn_main_bottom_book;
    /**
     * fragment事务对象
     */
    private FragmentTransaction mFragTransaction;

    //fragment的所有实例
    private NewsFragment newsFragment;
    private VideoFragment videoFragment;
    private GambitFragment gambitFragment;
    private MeFragment meFragment;
    private BookFragment bookFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);
        x.view().inject(this);

        initView();

        initData();

    }

    @Override
    protected void onResume() {
        super.onResume();

        //初始化进入新闻页面
        handleFragment();
    }

    @Override
    protected void initView() {

        rbn_main_bottom_news.setOnClickListener(this);
        rbn_main_bottom_video.setOnClickListener(this);
        rbn_main_bottom_attention.setOnClickListener(this);
        rbn_main_bottom_me.setOnClickListener(this);
        rbn_main_bottom_book.setOnClickListener(this);
    }

    @Override
    protected void initData() {

//        //暂时作为初始化频道数据
//        if (Config.ConfigSyn(this).getIsFirst()) {
//            Config.ConfigSyn(this).setIsFrist(false);
//            DbUtilsDao.initDb();
//        }
    }

    @Override
    protected void initClickListener(View v) {

        switch (v.getId()) {

            case R.id.rbn_main_bottom_news:

                changeTab(TAB_NEWS);
                break;

            case R.id.rbn_main_bottom_video:

                changeTab(TAB_VIDEO);
                break;

            case R.id.rbn_main_bottom_attention:

                changeTab(TAB_ATTENTION);
                break;

            case R.id.rbn_main_bottom_me:

                changeTab(TAB_ME);
                break;

            case R.id.rbn_main_bottom_book:

                changeTab(TAB_BOOK);


        }
    }

    /**
     * @param currentTab 要切换到的tab
     */
    private void changeTab(int currentTab) {
        if (currentTab != mCurrentTab) {
            mCurrentTab = currentTab;
            handleFragment();
        }
    }

    /**
     * 处理绑定或解绑Fragment
     */
    private void handleFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        findFragment(fragmentManager);
        mFragTransaction = fragmentManager.beginTransaction();
        ArrayList<Fragment> fragList = new ArrayList<Fragment>(Arrays.asList(newsFragment, videoFragment, gambitFragment, meFragment,bookFragment));

        switch (mCurrentTab) {
            case TAB_NEWS:

                rbn_main_bottom_news.setChecked(true);
                ensureTabNews();
                fragList.remove(newsFragment);
                break;

            case TAB_VIDEO:
                rbn_main_bottom_video.setChecked(true);
                fragList.remove(videoFragment);
                ensureTabVideo();
                break;

            case TAB_ATTENTION:

                rbn_main_bottom_attention.setChecked(true);
                ensureTabLife();
                fragList.remove(gambitFragment);

                break;
            case TAB_ME:

                rbn_main_bottom_me.setChecked(true);
                ensureTabMe();
                fragList.remove(meFragment);

                break;


            case TAB_BOOK:

                rbn_main_bottom_book.setChecked(true);
                ensureTabBook();
                fragList.remove(bookFragment);



        }

        for (Fragment frag : fragList) {
            if (frag != null && frag.isVisible()) {
//                mFragTransaction.detach(frag);
                mFragTransaction.hide(frag);
            }
        }
        mFragTransaction.commitAllowingStateLoss();

    }

    /**
     * @param fragmentManager 通过标签定位Fragment
     */
    private void findFragment(FragmentManager fragmentManager) {
        newsFragment = (NewsFragment) fragmentManager
                .findFragmentByTag(FRAGMENT_TAG_NEWS);
        videoFragment = (VideoFragment) fragmentManager
                .findFragmentByTag(FRAGMENT_TAG_VIDEO);
        gambitFragment = (GambitFragment) fragmentManager
                .findFragmentByTag(FRAGMENT_TAG_ATTENTION);
        meFragment = (MeFragment) fragmentManager
                .findFragmentByTag(FRAGMENT_TAG_ME);
        bookFragment = (BookFragment) fragmentManager.findFragmentByTag(FRAGMENT_TAG_BOOK);

    }


    /**
     * 显示新闻
     */
    private void ensureTabNews() {
        if (newsFragment == null) {
            newsFragment = new NewsFragment();
            mFragTransaction.add(R.id.rl_main_body, newsFragment,
                    FRAGMENT_TAG_NEWS);
        } else {
            if (newsFragment.isDetached()) {
                mFragTransaction.attach(newsFragment);
            } else {
                mFragTransaction.show(newsFragment);
            }
        }
    }


    /**
     * 显示视屏
     */
    private void ensureTabVideo() {
        if (videoFragment == null) {
            videoFragment = new VideoFragment();
            mFragTransaction.add(R.id.rl_main_body, videoFragment,
                    FRAGMENT_TAG_VIDEO);
        } else {
            if (videoFragment.isDetached()) {
                mFragTransaction.attach(videoFragment);
            } else {
                mFragTransaction.show(videoFragment);
            }
        }
    }


    /**
     * 显示消息
     */
    private void ensureTabLife() {
        if (gambitFragment == null) {
            gambitFragment = new GambitFragment();
            mFragTransaction.add(R.id.rl_main_body, gambitFragment,
                    FRAGMENT_TAG_ATTENTION);
        } else {
            if (gambitFragment.isDetached()) {
                mFragTransaction.attach(gambitFragment);
            } else {
                mFragTransaction.show(gambitFragment);
            }
        }

    }

    /**
     * 显示我的
     */
    private void ensureTabMe() {
        if (meFragment == null) {
            meFragment = new MeFragment();
            mFragTransaction.add(R.id.rl_main_body, meFragment,
                    FRAGMENT_TAG_ME);
        } else {
            if (meFragment.isDetached()) {
                mFragTransaction.attach(meFragment);
            } else {
                mFragTransaction.show(meFragment);
            }
        }
    }


    private void ensureTabBook(){


        if(bookFragment == null){


            bookFragment = new BookFragment();
            mFragTransaction.add(R.id.rl_main_body,bookFragment,FRAGMENT_TAG_BOOK);

        }else{


            if(bookFragment.isDetached()){


                mFragTransaction.attach(bookFragment);

            }else{

                mFragTransaction.show(bookFragment);

            }


        }




    }




}
