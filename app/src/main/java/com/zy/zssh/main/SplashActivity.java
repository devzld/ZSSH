package com.zy.zssh.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.SharedListString;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;
import com.zy.zssh.common.volleyutil.VolleyResponse;

import org.xutils.view.annotation.ContentView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * desc:首页广告的加载
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007 11:40
 **/
@ContentView(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {



    private static final String AD_CACHE = "ad_cache";

    private final static int COUNT_DOWN_TIME = 5;
    @BindView(R.id.countDownText)
    TextView countDownText;
    @BindView(R.id.ads)
    ImageView ads;

    private CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        initData();


    }

    protected void initData() {


        countDownTimer = new CountDownTimer(COUNT_DOWN_TIME*1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {


                countDownText.setText(millisUntilFinished/1000 + "s");


            }

            @Override
            public void onFinish() {

                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();


            }
        };


        String url = VolleyClient.SERVER_IP_ADDRESS + "advlft" + VolleyClient.SERVER_IP_FOOT;
        Map<String, String> params = new HashMap<>();
        VolleyClient.getInstance(SplashActivity.this).requsetOfPost(url, params, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                List<AdBean> list = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), AdBean.class);
                if(list==null||list.size() == 0){

                    getAdsFail();


                }else{


                    //缓存广告，显示广告，倒计时
                    countDownTimer.start();
                    SharedListString.saveSpString(SplashActivity.this,AD_CACHE,FastJsonUtil.toJsonString(list));
                    showAd(getAdBean(list));



                }

            }

            @Override
            public void requestError(VolleyError error) {
                super.requestError(error);
                getAdsFail();

            }
        });

    }

    //获取广告失败
    public void getAdsFail(){

        String cache = SharedListString.getSpString(SplashActivity.this,AD_CACHE);
        if(TextUtils.isEmpty(cache)){

            //获取失败跳转主页面
            startActivity(new Intent(SplashActivity.this,MainActivity.class));
            finish();


        }else{


            List<AdBean> list = FastJsonUtil.jsonString2Beans(cache,AdBean.class);
            AdBean adBean = getAdBean(list);
            //显示广告并倒计时
            showAd(adBean);
            countDownTimer.start();


        }



    }


    /**
     * 显示广告
     *
     * @param adBean
     */
    public void showAd(AdBean adBean){

        if(adBean!=null){


            ImageLoader.getInstance().displayImage(adBean.getUrl(),ads,MyApplication.imageOptionNormal);

        }


    }


    /**
     * 随机获取一个广告
     *
     * @param list
     * @return
     */
    public AdBean getAdBean(List<AdBean> list){


        if(list == null || list.size() == 0){


            return null;

        }else{


            int size = list.size();
            Random random = new Random(System.currentTimeMillis());
            int num = random.nextInt(size);
            return list.get(num);

        }

    }



}
