package com.zy.zssh.main;

/**
 * desc:首页的广告
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/22 0022
 **/
public class AdBean {

    private String id;
    private String httputrl;
    private String Url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHttputrl() {
        return httputrl;
    }

    public void setHttputrl(String httputrl) {
        this.httputrl = httputrl;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
