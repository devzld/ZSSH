package com.zy.zssh.m_book.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_book.BookReadActivity;
import com.zy.zssh.m_book.bean.Book;
import com.zy.zssh.m_book.business.BookBusiness;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * desc:已购图书
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007 13:09
 **/
public class BoughtBookFragment extends Fragment {

    @BindView(R.id.boughtBookList)
    RecyclerView boughtBookList;
    @BindView(R.id.info)
    TextView info;

    private ArrayList<Book> boughtBook = new ArrayList<>();

    private int currentPage = 1;

    private BoughtBookAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        //Logger.d("加载数据：" + System.currentTimeMillis());

        View v = inflater.inflate(R.layout.fragment_bought_book, null);
        ButterKnife.bind(this, v);

        initVariables();


        return v;
    }



    //初始化书城资料
    private void initVariables() {


        adapter = new BoughtBookAdapter(getActivity());

        //已购买图书的点击事件
        adapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                //跳转到图片浏览界面  传入当前书籍的id
                Intent intent = new Intent(getContext(), BookReadActivity.class);
                Book book = boughtBook.get(position);
                intent.putExtra("bookId",book.getId());
                startActivity(intent);


            }
        });

        boughtBookList.setLayoutManager(new GridLayoutManager(getContext(),3));
        boughtBookList.setAdapter(adapter);




        //首先判断当前用户是否已登录
        String userId = Config.Config(getContext()).getUserId();
        if (TextUtils.isEmpty(userId)) {


            //未登录
            info.setVisibility(View.VISIBLE);
            boughtBookList.setVisibility(View.INVISIBLE);

        } else {


            BookBusiness.getBoughtBook(getContext(),currentPage,userId,new VolleyCallback(){


                @Override
                public void requestSuccess(VolleyResponse response) {

                    if(isSuccess(response)){

                        //成功，加载数据
//                        info.setVisibility(View.INVISIBLE);
//                        boughtBookList.setVisibility(View.VISIBLE);
                        List<Book> list = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(),"comList"),Book.class);

                        if(list == null || list.size() == 0){

                            boughtBookList.setVisibility(View.INVISIBLE);
                            info.setVisibility(View.VISIBLE);
                            info.setText("您还未购买过书籍");

                        }else{

                            boughtBookList.setVisibility(View.VISIBLE);
                            info.setVisibility(View.INVISIBLE);

                            boughtBook.clear();
                            boughtBook.addAll(list);
                            adapter.notifyDataSetChanged();


                        }



                    }else{

                        //失败，提示用户
                        info.setText("您当前还未购买过图书");
                        info.setVisibility(View.VISIBLE);
                        boughtBookList.setVisibility(View.INVISIBLE);


                    }


                }


                @Override
                public void requestError(VolleyError error) {
                    //super.requestError(error);
                    //失败，提示用户
                    info.setText("您当前还未购买过图书");
                    info.setVisibility(View.VISIBLE);
                    boughtBookList.setVisibility(View.INVISIBLE);
                }
            });








        }

    }

    /**
     * 筛选用户已购买的书籍
     *
     * @param ls
     * @return
     */
    public List<Book> chooseBook(List<Book> ls){

        List<Book> list = new ArrayList<>();

        for(Book book:ls){

            if(book.getBaysate().equals("1")){

                list.add(book);

            }
        }


        return list;


    }





    private class BoughtBookAdapter extends BaseRecycleAdapter {


        public BoughtBookAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = mLayoutInflater.inflate(R.layout.view_item_boughtbok, null);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


            Book book = boughtBook.get(position);
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.bookTitle.setText(book.getName());
            ImageLoader.getInstance().displayImage(book.getUrl(),viewHolder.bookPage);
            viewHolder.bookPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v,position);
                }
            });

        }

        @Override
        public int getItemCount() {
            return boughtBook.size();
        }



    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.bookPage)
        ImageView bookPage;
        @BindView(R.id.bookTitle)
        TextView bookTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
