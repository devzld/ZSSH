package com.zy.zssh.m_book.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.R;
import com.zy.zssh.common.adapter.BaseRecycleAdapter;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_book.BookDetailActivity;
import com.zy.zssh.m_book.BookReadActivity;
import com.zy.zssh.m_book.bean.Book;
import com.zy.zssh.m_book.business.BookBusiness;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * desc:所有图书
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007 13:09
 **/
@ContentView(R.layout.fragment_all_book)
public class AllBookFragment extends BaseFragment {


    @ViewInject(R.id.allBookList)
    private RecyclerView allBookList;
    private AllBookAdapter adapter;
    private ArrayList<Book> alBook = new ArrayList<>();
    private int currentPage = 1;
    private int lastPosition = 0;


//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//
//
//        Logger.d("onCreateView");
//        View v = inflater.inflate(R.layout.fragment_all_book, null);
//        allBookList = (RecyclerView) v.findViewById(R.id.allBookList);
//
//        TextView textView=new TextView(container.getContext());
//
//        textView.setText("hsjhajs");
//        textView.setTextColor(R.color.refresh_color);
//
//        initVariables();
//        return v;
//    }


//    public AllBookFragment() {
//        super();
//
//        System.out.println("");
//    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initVariables();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Logger.d("onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Logger.d("onDestory");
    }

    //初始化界面的数据
    private void initVariables() {

        //判断当前
        adapter = new AllBookAdapter(getActivity());
        allBookList.setAdapter(adapter);
        allBookList.setLayoutManager(new LinearLayoutManager(getActivity()));

        //滑动加载更多数据
        allBookList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


//                Logger.d("调试");

                if(newState == RecyclerView.SCROLL_STATE_IDLE && lastPosition+1 == adapter.getItemCount()){


                    //如果滑到底部加载更多
                    loadMoreData();


                }

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


               // Logger.d("调试");

                LinearLayoutManager manager = (LinearLayoutManager) allBookList.getLayoutManager();
                lastPosition = manager.findLastVisibleItemPosition();

            }
        });


        BookBusiness.getBookInfoByPage(getActivity(), currentPage, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                List<Book> list = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), Book.class);


                if (list == null || list.size() == 0) {


                    Tools.ToastServerError(getContext());


                } else {


                    alBook.clear();
                    alBook.addAll(list);
                    adapter.notifyDataSetChanged();
                    //下一次请求的页数加一
                    currentPage++;

                }


            }
        });




        adapter.setOnItemClickListener(new BaseRecycleAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {



                Book book = alBook.get(position);
                String bookId = book.getId();
                if(TextUtils.isEmpty(book.getBaysate())||book.getBaysate().equals("0")){

//                    Tools.Toast(getContext(),"您还未购买，不能查看");

                    Intent intent = new Intent(getContext(), BookDetailActivity.class);
                    intent.putExtra("bookId",bookId);
                    startActivity(intent);
                }else{


//                    跳转到图片浏览界面  传入当前书籍的id
                    Intent intent = new Intent(getContext(), BookReadActivity.class);
                    intent.putExtra("bookId",bookId);
                    startActivity(intent);

                }





//                Logger.d("点击：" + position);

//                对用户是否未会员进行判断
//                if(Config.Config(getContext()).getUserSate().equals("1")){
//
//                    //跳转到图片浏览界面  传入当前书籍的id
//                    Intent intent = new Intent(getContext(), BookReadActivity.class);
//                    Book book = alBook.get(position);
//                    intent.putExtra("bookId",book.getId());
//                    startActivity(intent);
//
//                }else{
//
//                    Tools.Toast(getContext(),"您还未购买，不能查看");
//
//                }



//            //跳转到图片浏览界面  传入当前书籍的id
//            Intent intent = new Intent(getContext(), BookReadActivity.class);
//            Book book = alBook.get(position);
//            intent.putExtra("bookId",book.getId());
//            startActivity(intent);



            }
        });





    }

    private void loadMoreData() {


        BookBusiness.getBookInfoByPage(getActivity(), currentPage, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);

                List<Book> list = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(), "comList"), Book.class);


                if (list == null || list.size() == 0) {


                    Tools.ToastServerError(getContext());


                } else {


                    alBook.addAll(list);
                    adapter.notifyDataSetChanged();
                    //下一次请求的页数加一
                    currentPage++;

                }


            }
        });





    }

    private class AllBookAdapter extends BaseRecycleAdapter {


        public AllBookAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = mLayoutInflater.inflate(R.layout.item_book, null);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


            MyViewHolder myViewHolder = (MyViewHolder) holder;
            Book book = alBook.get(position);
            ImageLoader.getInstance().displayImage(book.getUrl(),myViewHolder.bookPage);
            myViewHolder.bookTitle.setText(book.getName());
            myViewHolder.bookPrice.setText("￥" + book.getMomey());
            myViewHolder.bookDes.setText(book.getMian());
            myViewHolder.bookAuthor.setText(book.getWriter());
            myViewHolder.bookPageNum.setText(book.getPage() + "页");

            myViewHolder.bookItemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemClickListener.onItemClick(v,position);

                }
            });

            //显示天数
            int day = Tools.stimeToNow(book.getAddtime());
            if(day != -1){

                if(day == 0){
                    myViewHolder.bookPubTime.setText("今日");

                }else{

                    myViewHolder.bookPubTime.setText(day + "天前");
                }



            }

            //显示是否已购买
            if(book.getBaysate().equals("1")){

                myViewHolder.isBuy.setVisibility(View.VISIBLE);

            }else{

                myViewHolder.isBuy.setVisibility(View.INVISIBLE);

            }






        }

        @Override
        public int getItemCount() {

            return alBook.size();

        }




    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.isBuy)
        ImageView isBuy;
        @BindView(R.id.bookPage)
        ImageView bookPage;
        @BindView(R.id.bookTitle)
        TextView bookTitle;
        @BindView(R.id.bookPrice)
        TextView bookPrice;
        @BindView(R.id.bookDes)
        TextView bookDes;
        @BindView(R.id.bookAuthor)
        TextView bookAuthor;
        @BindView(R.id.bookPageNum)
        TextView bookPageNum;
        @BindView(R.id.bookPubTime)
        TextView bookPubTime;
        @BindView(R.id.bookItemContainer)
        LinearLayout bookItemContainer;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }


}
