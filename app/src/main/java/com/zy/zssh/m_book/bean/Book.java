package com.zy.zssh.m_book.bean;

/**
 * desc:
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007
 **/
public class Book {


    /**
     * Id : 1
     * name : 哈哈历险记
     * writer : 小张
     * addtime : 2016-6-28 13:47:28
     * Momey : 300
     * Page : 5
     * Host : 0
     * url : 123
     * Mian : 123
     * Baysate : 0
     */

    private String Id;
    private String name;
    private String writer;
    private String addtime;
    private String Momey;
    private String Page;
    private String Host;
    private String url;
    private String Mian;
    private String Baysate;

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getMomey() {
        return Momey;
    }

    public void setMomey(String Momey) {
        this.Momey = Momey;
    }

    public String getPage() {
        return Page;
    }

    public void setPage(String Page) {
        this.Page = Page;
    }

    public String getHost() {
        return Host;
    }

    public void setHost(String Host) {
        this.Host = Host;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMian() {
        return Mian;
    }

    public void setMian(String Mian) {
        this.Mian = Mian;
    }

    public String getBaysate() {
        return Baysate;
    }

    public void setBaysate(String Baysate) {
        this.Baysate = Baysate;
    }
}
