package com.zy.zssh.m_book.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orhanobut.logger.Logger;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.view.ZoomImageView;
import com.zy.zssh.m_book.bean.BookContent;

import java.util.List;

/**
 * desc:书籍内页的
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/11 0011
 **/
public class BookImageAdapter extends BaseAdapter{



    private List<BookContent> books;
    private Context context;
    private int repeatCount = 1;
    private ImageView nextImageView;




    public List<BookContent> getBooks() {
        return books;
    }

    public void setBooks(List<BookContent> books) {
        this.books = books;
    }

    public BookImageAdapter(Context context, List<BookContent> imageUrls) {
        this.context = context;
        this.books = imageUrls;
        nextImageView = new ImageView(context);

    }

    @Override
    public int getCount() {
        return books == null ? 0:books.size();
    }

    @Override
    public Object getItem(int position) {
        return books.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


//        ImageView imageView = new ImageView(context);

        //GestureImageView imageView = new GestureImageView(context);


        View view = LayoutInflater.from(context).inflate(R.layout.item_pager_image,null);
        ZoomImageView imageView = (ZoomImageView) view.findViewById(R.id.image);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.loading);
        String url = books.get(position).getUrl();

        ImageLoader.getInstance().displayImage(url, imageView, MyApplication.imageOptionLoadingNetFile, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

                progressBar.setVisibility(View.VISIBLE);
                Logger.d("画册图片正在加载");

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                progressBar.setVisibility(View.GONE);
                Logger.d("画册图片加载失败");

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                progressBar.setVisibility(View.GONE);
                Logger.d("画册图片加载完成");

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

                progressBar.setVisibility(View.GONE);
                Logger.d("画册图片取消加载");

            }
        });



//        ZoomImageView imageView = new ZoomImageView(context);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        imageView.setLayoutParams(params);
//        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

//        String cachePath = CommonTool.getCachePath(context);
//        String path = "file://" + cachePath + "/" + MD5.md5(url) + ".jpg";
//        ImageLoader.getInstance().displayImage(path,imageView);

        return view;
    }
}
