package com.zy.zssh.m_book.bean;

/**
 * desc:书籍的内容
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/8 0008
 **/
public class BookContent {

    private String page;
    private String url;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
