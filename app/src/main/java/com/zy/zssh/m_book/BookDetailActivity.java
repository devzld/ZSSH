package com.zy.zssh.m_book;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zy.zssh.MyApplication;
import com.zy.zssh.R;
import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_book.bean.BookDes;
import com.zy.zssh.m_book.business.BookBusiness;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookDetailActivity extends AppCompatActivity {


    @BindView(R.id.bookPager)
    ImageView bookPager;
    @BindView(R.id.bookTitle)
    TextView bookTitle;
    @BindView(R.id.bookAuthor)
    TextView bookAuthor;
    @BindView(R.id.bookPrice)
    TextView bookPrice;
    @BindView(R.id.bookDescrible)
    TextView bookDescrible;
    @BindView(R.id.bookPage1)
    ImageView bookPage1;
    @BindView(R.id.bookPage2)
    ImageView bookPage2;
    @BindView(R.id.bookPage3)
    ImageView bookPage3;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.buy)
    TextView buy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        ButterKnife.bind(this);
        loadData();

    }

    private void loadData() {

        String bookId = getIntent().getStringExtra("bookId");
        String userId = Config.Config(this).getUserId();
        BookBusiness.getBookDes(this, userId, bookId, new VolleyCallback() {
            @Override
            public void requestSuccess(VolleyResponse response) {
                super.requestSuccess(response);


                String body = response.getBody().toString();

//                Logger.d("书籍详情：" + body);
                BookDes bookDes = FastJsonUtil.jsonString2Bean(body,BookDes.class);

                loadDataToView(bookDes);


            }
        });


    }

    //将数据加载到view中
    private void loadDataToView(BookDes bookDes) {


        if(bookDes!=null){

            ImageLoader.getInstance().displayImage(bookDes.getUrl(),bookPager, MyApplication.imageOptionNormal);
            ImageLoader.getInstance().displayImage(bookDes.getJj1(),bookPage1, MyApplication.imageOptionNormal);
            ImageLoader.getInstance().displayImage(bookDes.getJj2(),bookPage2, MyApplication.imageOptionNormal);
            ImageLoader.getInstance().displayImage(bookDes.getJj3(),bookPage3, MyApplication.imageOptionNormal);

            bookTitle.setText(bookDes.getName());
            bookAuthor.setText("作者：" + bookDes.getWriter());
            bookPrice.setText("￥" + bookDes.getMomey());
            bookDescrible.setText(bookDes.getMian());


        }else{

            Tools.Toast(this,"服务器异常");


        }



    }


    @OnClick({R.id.back, R.id.buy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.buy:
                break;
        }
    }
}
