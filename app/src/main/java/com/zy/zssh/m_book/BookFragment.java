package com.zy.zssh.m_book;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.zy.zssh.R;
import com.zy.zssh.common.fragment.BaseFragment;
import com.zy.zssh.m_book.adapter.BookFragAdapter;
import com.zy.zssh.m_book.fragment.AllBookFragment;
import com.zy.zssh.m_book.fragment.BoughtBookFragment;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * desc:主页的图书界面
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007 13:08
 **/
@ContentView(R.layout.fg_book)
public class BookFragment extends BaseFragment {


    @ViewInject(R.id.bookTabLayout)
    TabLayout bookTabLayout;
    @ViewInject(R.id.bookViewPager)
    ViewPager bookViewPager;
    private String[] titles = {"全部图书", "已购图书"};
    private ArrayList<Fragment> alFragments = new ArrayList<>();


//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fg_book, null);
//        ButterKnife.bind(this, view);
//        initVariables();
//        return view;
//    }




    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initVariables();
    }

    private void initVariables() {



        alFragments.add(new AllBookFragment());
        alFragments.add(new BoughtBookFragment());

        bookViewPager.setAdapter(new BookFragAdapter(getActivity().getSupportFragmentManager(), titles, alFragments));

        bookTabLayout.setupWithViewPager(bookViewPager);




    }


}
