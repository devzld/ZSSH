package com.zy.zssh.m_book;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.android.volley.VolleyError;
import com.orhanobut.logger.Logger;
import com.zy.zssh.R;
import com.zy.zssh.common.utils.CommonTool;
import com.zy.zssh.common.utils.DownloadService;
import com.zy.zssh.common.utils.FastJsonUtil;
import com.zy.zssh.common.utils.Tools;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyResponse;
import com.zy.zssh.m_book.adapter.BookImageAdapter;
import com.zy.zssh.m_book.bean.BookContent;
import com.zy.zssh.m_book.business.BookBusiness;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.flipview.FlipView;

/**
 * desc:查看书籍的activity
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/8 0008 14:15
**/
public class BookReadActivity extends AppCompatActivity {



    private ArrayList<BookContent> al = new ArrayList<>();
    private int currentRequest = 1;
    private BookImageAdapter adapter;
    private String bookId;
//    private FlipViewController controller;

    private FlipView flipView;

    private ProgressDialog dialog;
    private int num = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_read);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setTitle("正在加载，请稍等");
        flipView = (FlipView) findViewById(R.id.flip_view);

        initVariables();


    }




    private void initVariables() {


//        showLoadingView();

        //取得当前书籍的id
        bookId = getIntent().getStringExtra("bookId");
//        controller = new FlipViewController(this,FlipViewController.HORIZONTAL);
//        controller.setOverFlipEnabled(true);
//        controller.setFlipByTouchEnabled(false);


        adapter = new BookImageAdapter(this,al);

//        controller.setAdapter(adapter);


        flipView.setAdapter(adapter);


        if(TextUtils.isEmpty(bookId)){

            Tools.ToastServerError(this);

        }else{

            //获取书籍的图片
            BookBusiness.getBookContentByQuest(this,currentRequest,bookId,new VolleyCallback(){


                @Override
                public void requestSuccess(VolleyResponse response) {
                    super.requestSuccess(response);


                    List<BookContent> list = FastJsonUtil.jsonString2Beans(FastJsonUtil.getNoteJson(response.getBody().toString(),"comList"), BookContent.class);
                    //获取当前书籍的图片并缓存



                    adapter.getBooks().clear();
                    adapter.getBooks().
                            addAll(list);

//                    downloadImages(list);

                    adapter.notifyDataSetChanged();




                }

                @Override
                public void requestError(VolleyError error) {
                    super.requestError(error);



                }
            });


        }

//        setContentView(controller);


    }

    private void downloadImages(final List<BookContent> list) {



        if(list!=null && list.size()>0){



            List<String> downPath = new ArrayList<>();
            for (BookContent bookContent : list) {

                String url = bookContent.getUrl();
                if(!TextUtils.isEmpty(url)){

                    downPath.add(url);
                }

            }


            dialog.setMax(downPath.size());
            new DownloadService(CommonTool.getCachePath(this), downPath, new DownloadService.DownloadStateListener() {
                @Override
                public void onStart() {

                    Logger.d("开始下载");
                    dialog.show();

                }

                @Override
                public void onFinish() {

                    Logger.d("下载完成");
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();

                }

                @Override
                public void onProgress(int size) {

                    Logger.d("下载进度：" + size);
                    dialog.setProgress(size);

                }


                @Override
                public void onFailed() {

                    Logger.d("下载失败");
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();

                }
            }).startDownload();




        }


//            for (BookContent bookContent : list) {
//
//
//
//                CommonTool.cacheNetFile(BookReadActivity.this, bookContent.getUrl(), new Callback.CommonCallback<File>() {
//                    @Override
//                    public void onSuccess(File result) {
//
//                        synchronized (this){
//
//                            num++;
//                            Logger.d("下载成功：" + num);
//
//                        }
//
//
//
//
//
//                    }
//
//                    @Override
//                    public void onError(Throwable ex, boolean isOnCallback) {
//
//                        synchronized (this){
//
//                            //错误，数字加一
//                            num++;
//                            Logger.d("下载失败：" + num);
//
//                        }
//
//
//                    }
//
//                    @Override
//                    public void onCancelled(CancelledException cex) {
//
//
//                    }
//
//                    @Override
//                    public void onFinished() {
//
//                        if(num == list.size()){
//
//                            dialog.dismiss();
//                            adapter.notifyDataSetChanged();
//
//                        }
//
//                    }
//                });
//
//
//
//            }
//
//        }



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog!=null){

            dialog.dismiss();

        }

    }
}
