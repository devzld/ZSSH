package com.zy.zssh.m_book.bean;

/**
 * Created by Administrator on 2016/8/1 0001.
 */
public class BookDes {


    /**
     * Id : 1
     * name : 哈哈历险记
     * writer : 小张
     * addtime : 2016/6/28 13:47:28
     * Momey : 300
     * Page : 5
     * Host : 0
     * url : http://app.shzywh.cn/app/book/fenmian.jpg
     * Mian : 介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍
     * Baysate : 1
     * jj1 : http://app.shzywh.cn/app/book/page10.jpg
     * jj2 : http://app.shzywh.cn/app/book/page10.jpg
     * jj3 : http://app.shzywh.cn/app/book/page10.jpg
     * jj4 : http://app.shzywh.cn/app/book/page10.jpg
     * jj5 : http://app.shzywh.cn/app/book/page10.jpg
     * jj6 : http://app.shzywh.cn/app/book/page10.jpg
     */

    private String Id;
    private String name;
    private String writer;
    private String addtime;
    private String Momey;
    private String Page;
    private String Host;
    private String url;
    private String Mian;
    private String Baysate;
    private String jj1;
    private String jj2;
    private String jj3;
    private String jj4;
    private String jj5;
    private String jj6;

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getMomey() {
        return Momey;
    }

    public void setMomey(String Momey) {
        this.Momey = Momey;
    }

    public String getPage() {
        return Page;
    }

    public void setPage(String Page) {
        this.Page = Page;
    }

    public String getHost() {
        return Host;
    }

    public void setHost(String Host) {
        this.Host = Host;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMian() {
        return Mian;
    }

    public void setMian(String Mian) {
        this.Mian = Mian;
    }

    public String getBaysate() {
        return Baysate;
    }

    public void setBaysate(String Baysate) {
        this.Baysate = Baysate;
    }

    public String getJj1() {
        return jj1;
    }

    public void setJj1(String jj1) {
        this.jj1 = jj1;
    }

    public String getJj2() {
        return jj2;
    }

    public void setJj2(String jj2) {
        this.jj2 = jj2;
    }

    public String getJj3() {
        return jj3;
    }

    public void setJj3(String jj3) {
        this.jj3 = jj3;
    }

    public String getJj4() {
        return jj4;
    }

    public void setJj4(String jj4) {
        this.jj4 = jj4;
    }

    public String getJj5() {
        return jj5;
    }

    public void setJj5(String jj5) {
        this.jj5 = jj5;
    }

    public String getJj6() {
        return jj6;
    }

    public void setJj6(String jj6) {
        this.jj6 = jj6;
    }
}
