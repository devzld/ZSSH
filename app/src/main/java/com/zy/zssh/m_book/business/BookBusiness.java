package com.zy.zssh.m_book.business;

import android.content.Context;

import com.zy.zssh.common.config.Config;
import com.zy.zssh.common.volleyutil.VolleyCallback;
import com.zy.zssh.common.volleyutil.VolleyClient;

import java.util.HashMap;
import java.util.Map;

/**
 * desc:书城的业务逻辑
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007
 **/
public class BookBusiness {




    private static final String URL_ALL_BOOK = "http://app.shzywh.cn/app/booklist.aspx";
    private static final String URL_BOOK_CONTENT = "http://app.shzywh.cn/app/book.aspx";
    private static final String URL_BOUGHT_BOOK = "http://app.shzywh.cn/app/booklist1.aspx";


    /**
     * 请求书城的列表
     *
     * @param context
     * @param page
     * @param callback
     */
    public static void getBookInfoByPage(final Context context, int page,VolleyCallback callback){



        String userId = Config.Config(context).getUserId();

        Map<String , String> params = new HashMap<>();
        params.put("userId",userId);
        params.put("page",page+"");


        VolleyClient.getInstance(context).requsetOfPost(URL_ALL_BOOK, params, callback);


    }


    /**
     * 根据书籍的id得到书籍的内容
     *
     * @param context
     * @param quest
     * @param bookId
     * @param callback
     */
    public static void getBookContentByQuest(Context context , int quest , String bookId,VolleyCallback callback){


        Map<String , String> params = new HashMap<>();
        params.put("quest",quest + "");
        params.put("bookId",bookId);

        VolleyClient.getInstance(context).requsetOfPost(URL_BOOK_CONTENT,params,callback);


    }


    public static void getBoughtBook(Context context , int page,String userId , VolleyCallback callback){



        Map<String , String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("userId",userId);

        VolleyClient.getInstance(context).requsetOfPost(URL_BOUGHT_BOOK,params,callback);


    }


    public static void getBookDes(Context context , String userId,String bookId,VolleyCallback callback){

        String url = VolleyClient.SERVER_IP_ADDRESS + "bookdec" + VolleyClient.SERVER_IP_FOOT;
        Map<String,String> params = new HashMap<>();
        params.put("userId",userId);
        params.put("bookId",bookId);
        VolleyClient.getInstance(context).requsetOfPost(url,params,callback);



    }




}
