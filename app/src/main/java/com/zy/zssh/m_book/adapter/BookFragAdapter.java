package com.zy.zssh.m_book.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * desc:
 * author:陈祥龙
 * email:shannonc@163.com
 * created at:2016/7/7 0007
 **/
public class BookFragAdapter extends FragmentPagerAdapter{

    private String[] titles;
    private ArrayList<Fragment> alFragmetns;

    public BookFragAdapter(FragmentManager fm,String[] titles , ArrayList<Fragment> alFragmetns) {

        super(fm);
        this.titles = titles;
        this.alFragmetns = alFragmetns;
    }




    @Override
    public int getCount() {
        return titles == null? 0 : titles.length;
    }

    @Override
    public Fragment getItem(int position) {

        //Logger.d("调用getItem:" + position);

        return alFragmetns.get(position);
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return super.getItemPosition(object);
//    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return alFragmetns.get(position).getClass().hashCode();
    }

//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        return super.instantiateItem(container, position);
//    }


}
